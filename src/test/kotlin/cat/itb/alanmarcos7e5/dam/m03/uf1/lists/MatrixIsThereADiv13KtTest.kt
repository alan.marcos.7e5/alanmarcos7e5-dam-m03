package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class MatrixIsThereADiv13KtTest {

    @Test
    fun `matrixDivisibleBy13 true`() {
        val matrix = listOf(listOf(2,5,1,6),listOf(23,52,14,36))
        val result = matrixDivisibleBy13(matrix)
        assertTrue(result)
    }

    @Test
    fun `matrixDivisibleBy13 false`() {
        val matrix = listOf(listOf(2,5,1,6),listOf(23,14))
        val result = matrixDivisibleBy13(matrix)
        assertFalse(result)
    }
}