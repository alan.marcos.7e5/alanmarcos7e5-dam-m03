package cat.itb.alanmarcos7e5.dam.m03.uf2.generalexam

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class LogicOilTest {
    val spill = Spill("name", "company", 123456, 0.256)
    val spill2 = Spill("name2", "company2", 654321, 0.08)
    val spill3 = Spill("name3", "company", 654320, 0.16)
    val logicOil = LogicOil()

    @Test
    fun addSpillTest() {
        val expected = listOf<Spill>(spill,spill2)
        logicOil.addSpill(spill)
        logicOil.addSpill(spill2)
        assertEquals(expected, listOf(spill,spill2))
    }
    @Test
    fun listTest(){
        logicOil.addSpill(spill)
        logicOil.addSpill(spill2)
        val result = logicOil.listOfSpills
        assertEquals(listOf(spill,spill2),result)
    }
    @Test
    fun `searchWorstTest not empty`(){
        logicOil.addSpill(spill)
        logicOil.addSpill(spill2)
        val result = logicOil.searchWorst()
        assertEquals(spill2,result)
    }
    @Test
    fun `searchWorstTest empty`(){
        val result = logicOil.searchWorst()
        assertEquals(null,result)
    }
    @Test
    fun `searchSpillTest exists`(){
        logicOil.addSpill(spill)
        logicOil.addSpill(spill2)
        val result = logicOil.searchSpill("name")
        assertEquals(spill,result)
    }
    @Test
    fun `searchSpillTest not exists`(){
        val result = logicOil.searchSpill("fakeName")
        assertEquals(null,result)
    }
    @Test
    fun `searchSpillLitersTest exists`(){
        logicOil.addSpill(spill)
        logicOil.addSpill(spill2)
        val result = logicOil.spillLitres("name")
        assertEquals(123456,result)
    }
    @Test
    fun `searchSpillLitersTest not  exists`(){
        logicOil.addSpill(spill)
        logicOil.addSpill(spill2)
        val result = logicOil.spillLitres("fakename")
        assertEquals(null,result)
    }
    @Test
    fun `searchSpillGravityTest exists`(){
        logicOil.addSpill(spill)
        logicOil.addSpill(spill2)
        val result = logicOil.spillGravity("name")
        assertEquals(31604.73,result!!,0.02)
    }
    @Test
    fun `searchSpillGravityTest not  exists`(){
        val result = logicOil.spillLitres("name")
        assertEquals(null,result)
    }
    @Test
    fun `searchSpillByCompanyTest exists`(){
        logicOil.addSpill(spill)
        logicOil.addSpill(spill2)
        logicOil.addSpill(spill3)
        val result = logicOil.spillsCompany("company")
        assertEquals(listOf(spill,spill3),result)
    }
    @Test
    fun `searchSpillByCompanyTest not exists`(){
        logicOil.addSpill(spill)
        logicOil.addSpill(spill2)
        logicOil.addSpill(spill3)
        val result = logicOil.spillsCompany("fakecompany")
        assertEquals(listOf<Spill>(),result)
    }
}
//Spill(name=Bahamas2011, company=OilCompany, litres=22547, toxicity=0.5)
//Spill(name=DeepwaterHorizon, company=PetroCo, litres=24547, toxicity=0.5)