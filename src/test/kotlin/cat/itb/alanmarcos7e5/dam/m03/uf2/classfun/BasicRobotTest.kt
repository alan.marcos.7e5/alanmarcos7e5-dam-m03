package cat.itb.alanmarcos7e5.dam.m03.uf2.classfun

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class BasicRobotTest {

    @Test
    fun baixTest() {
        val robot = BasicRobot()
        robot.baix()
        val result = robot.position[1]
        assertEquals(-1.0,result,0.01)
    }

    @Test
    fun daltTest() {
        val robot = BasicRobot()
        robot.dalt()
        robot.dalt()
        val result = robot.position[1]
        assertEquals(2.0,result,0.01)
    }

    @Test
    fun leftTest() {
        val robot = BasicRobot()
        robot.left()
        val result = robot.position[0]
        assertEquals(-1.0,result,0.01)
    }

    @Test
    fun rigthTest() {
        val robot = BasicRobot()
        robot.rigth()
        val result = robot.position[0]
        assertEquals(1.0,result,0.01)
    }

    @Test
    fun accelerateTest() {
        val robot = BasicRobot()
        robot.accelerate()
        val result = robot.velocity
        assertEquals(1.5,result,0.01)
    }

    @Test
    fun decreaseTest() {
        val robot = BasicRobot()
        repeat(9) {robot.decrease()}
        val result = robot.velocity
        assertEquals(-3.5,result,0.01)
    }

    @Test
    fun toOutputTestPos() {
        val robot = BasicRobot()
        val result = robot.toOutput("POSICIO")
        assertEquals("La posición es (0.0,0.0)\n",result)
    }
    @Test
    fun toOutputTestVel() {
        val robot = BasicRobot()
        val result = robot.toOutput("VELOCITAT")
        assertEquals("La velocidad es ${robot.velocity}\n",result)
    }
}