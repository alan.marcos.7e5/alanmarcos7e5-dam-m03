package cat.itb.alanmarcos7e5.dam.m03.uf1.introfunciones

import isATeenager
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class IsTeenagerFuncKtTest {

    @Test
    fun isATeenagerTrueTest() {
        val result1 = isATeenager(20)
        val result2 = isATeenager(10)
        assertTrue(result1)
        assertTrue(result2)
    }

    @Test
    fun isATeenagerFalseTest() {
        val result = isATeenager(9)
        val result2 = isATeenager(21)
        assertFalse(result)
        assertFalse(result2)
    }
}