package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class TestIsPair {

    @Test
    fun testIsPairTrue() {
        val result = isPair(32)
        assertTrue(result)
    }

    @Test
    fun testIsPairFalse() {
        val result = isPair(69)
        assertFalse(result)
    }
}