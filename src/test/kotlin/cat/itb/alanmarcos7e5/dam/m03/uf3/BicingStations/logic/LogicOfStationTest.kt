package cat.itb.alanmarcos7e5.dam.m03.uf3.BicingStations.logic

import cat.itb.alanmarcos7e5.dam.m03.uf3.BicingStations.ui.readBicingFile
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import kotlin.io.path.Path
import kotlin.math.log

internal class LogicOfStationTest {
    val homePath = Path(System.getProperty("user.home"))
    val filePath = homePath.resolve("Documents/DAMr1A/m03/JSON/station_status.json")
    val bcnStations = readBicingFile(filePath)
    val logicOfStations = LogicOfStation(bcnStations)
    val station1 = Station(id=1, availables=18, docks=24, status="IN_SERVICE", availablesTypes=BikeType(mechanical=18, ebike=0))
    val station1TypesAvailables = station1.availablesTypes
    @Test
    fun findStationTest() {
        val station = logicOfStations.findStation(1)
        assertEquals(station1,station)
    }

    @Test
    fun findAvailablesByStationTest() {
        val availables = logicOfStations.findAvailablesByStation(1)
        assertEquals(station1TypesAvailables,availables)
    }

    @Test
    fun availablesByStationTest() {
        val availablesType = logicOfStations.availablesByStation(1)
        assertEquals(18,availablesType)
    }

}