package cat.itb.alanmarcos7e5.dam.m03.uf1.introfunciones

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class IntRectangleAreaKtTest {

    @Test
    fun testAreaHabitacion() {
        val width = 3.0
        val heigth = 9.0
        val result = areaHabitacion(width, heigth)
        assertEquals(27.0, result)
    }
}