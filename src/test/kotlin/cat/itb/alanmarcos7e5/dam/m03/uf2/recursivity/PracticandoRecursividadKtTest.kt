package cat.itb.alanmarcos7e5.dam.m03.uf2.recursivity

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class PracticandoRecursividadKtTest {

    @Test
    fun getFiguresRecursiveTest() {
        val result = getFiguresRecursive(12345)
        assertEquals(5,result)
    }

    @Test
    fun multRecursiveTest() {
        val result = multRecursive(5,8)
        assertEquals(40,result)
    }

    @Test
    fun powRecursiveTest() {
        val result = powRecursive(5,3)
        assertEquals(125,result)
    }

    @Test
    fun factRecursiveTest() {
        val result = factRecursive(10)
        assertEquals(3628800,result)
    }

    @Test
    fun dotRecursiveTest() {
        val result = dotRecursive(4)
        assertEquals("....",result)
    }

    @Test
    fun searchMaxValueRecursiveTest() {
        val result = searchMaxValueRecursive(listOf(11,23,3,1994,53))
        assertEquals(1994,result)
    }
}