package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.intro

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class MechanicalArmTest {
    val mechanicalArm = MechanicalArm()
    @Test
    fun updateAngleTest() {
        mechanicalArm.updateAngle(180)
        val expected = mechanicalArm.openAngle
        assertEquals(expected,180.0)
    }
    @Test
    fun updateAngleMore360Test() {
        mechanicalArm.updateAngle(180)
        mechanicalArm.updateAngle(180)
        mechanicalArm.updateAngle(180)
        val expected = mechanicalArm.openAngle
        assertEquals(expected,360.0)
    }

    @Test
    fun updateAltitudeTest() {
        mechanicalArm.updateAltitude(3)
        val expected = mechanicalArm.altitude
        assertEquals(expected,3.0)
    }

    @Test
    fun setStatusTest() {
        mechanicalArm.setStatus(true)
        val expected = mechanicalArm.turnedOn
        assertEquals(expected,true)
    }
}