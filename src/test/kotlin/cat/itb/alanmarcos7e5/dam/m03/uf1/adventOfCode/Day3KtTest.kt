package cat.itb.alanmarcos7e5.dam.m03.uf1.adventOfCode

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class Day3KtTest {

    @Test
    fun `1011 should be 11`() {
        val list = listOf<Int>(1,0,1,1)
        val toDec = binaryToDecimal(list)
        assertEquals(11,toDec)
    }
    @Test
    fun `1001 should be 9`() {
        val list = listOf<Int>(1,0,0,1)
        val toDec = binaryToDecimal(list)
        assertEquals(9,toDec)
    }
    @Test
    fun `10110 should be 22`() {
        val list = listOf<Int>(1,0,1,1,0)
        val toDec = binaryToDecimal(list)
        assertEquals(22,toDec)
    }
    @Test
    fun `011000110010 should be 1586`() {
        val list = listOf<Int>(0,1,1,0,0,0,1,1,0,0,1,0)
        val toDec = binaryToDecimal(list)
        assertEquals(1586,toDec)
    }
    @Test
    fun `111111111111 should be 4095`() {
        val list = List(12){1}
        val toDec = binaryToDecimal(list)
        assertEquals(4095,toDec)
    }
}