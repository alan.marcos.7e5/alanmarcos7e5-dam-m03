package cat.itb.alanmarcos7e5.dam.m03.uf1.introfunciones

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class HowBigIsMyPizzaKtTest {

    @Test
    fun testCalculateCircleArea() {
        val radius = 10.0
        val result = calculateCircleArea(radius)
        assertEquals(314.159,result,0.001)
    }
}