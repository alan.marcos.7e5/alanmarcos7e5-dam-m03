package cat.itb.alanmarcos7e5.dam.m03.uf2.functions

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class IntListsKtTest {

    @Test
    fun minTest() {
        val result = min(listOf(5,3,8,9,2))
        assertEquals(2,result)
    }

    @Test
    fun maxTest() {
        val result = max(listOf(5,3,8,9,2))
        assertEquals(9,result)
    }

    @Test
    fun avgTest() {
        val result = avg(listOf(5,3,8,8))
        assertEquals(6.0,result,0.01)
    }
    @Test
    fun sumTest() {
        val result = sum(listOf(5,3,8,8))
        assertEquals(24,result)
    }
}