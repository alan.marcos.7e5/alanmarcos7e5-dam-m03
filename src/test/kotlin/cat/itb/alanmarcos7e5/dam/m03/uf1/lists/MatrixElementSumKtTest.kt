package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class MatrixElementSumKtTest {

    @Test
    fun matrixElementSumInt() {
        val matrix = listOf(listOf(2,-1),listOf(3,1,1),listOf(2,-2))
        val result = matrixElementSumInt(matrix)
        assertEquals(6,result)
    }
}