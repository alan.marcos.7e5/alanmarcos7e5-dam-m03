package cat.itb.alanmarcos7e5.dam.m03.uf2.classfun

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class BoardTest {

    @Test
    fun isPositionEmptyTestBusy() {
        val board = Board(
            mutableListOf<MutableList<String>>(mutableListOf("x","x","x"),mutableListOf("-","-","-"),
            mutableListOf("-","-","-")))
        val position = listOf<Int>(0,2)
        val result = board.isPositionEmpty(position)
        assertFalse(result)
    }
    @Test
    fun isPositionEmptyTestFree() {
        val board = Board(
            mutableListOf<MutableList<String>>(mutableListOf("x","x","x"),mutableListOf("-","-","-"),
                mutableListOf("-","-","-")))
        val position = listOf<Int>(1,2)
        val result = board.isPositionEmpty(position)
        assertTrue(result)
    }

    @Test
    fun addPieceTest() {
        val board = Board(
            mutableListOf<MutableList<String>>(mutableListOf("x","x","x"),mutableListOf("-","-","-"),
            mutableListOf("-","-","-")),false)
        val position = listOf<Int>(1,2)
        board.addPiece(position)
        val result = board.listOfPieces
        assertEquals(mutableListOf<MutableList<String>>(mutableListOf("x","x","x"),mutableListOf("-","-","o"),
            mutableListOf("-","-","-")),result)
    }

    @Test
    fun findWinnerXHorTest() {
        val board = Board(
            mutableListOf<MutableList<String>>(mutableListOf("x","x","x"),mutableListOf("-","-","-"),
            mutableListOf("-","-","-")))
        val result = board.findWinner()
        assertEquals("x",result)
    }
    @Test
    fun findWinnerODiagTest() {
        val board = Board(
            mutableListOf<MutableList<String>>(mutableListOf("o","x","x"),mutableListOf("-","o","-"),
                mutableListOf("-","-","o")))
        val result = board.findWinner()
        assertEquals("o",result)
    }

    @Test
    fun boardToStringTest() {
        val board = Board(
            mutableListOf<MutableList<String>>(mutableListOf("x","-","-"),mutableListOf("-","-","-"),
            mutableListOf("-","-","-")))
        val result = board.boardToString()
        assertEquals("x - - \n" +
                "- - - \n" +
                "- - - \n",result)
    }

    @Test
    fun allFullFalseTest() {
        val board = Board(
            mutableListOf<MutableList<String>>(mutableListOf("x","x","x"),mutableListOf("-","-","-"),
            mutableListOf("-","-","-")))
        val result = board.allFull()
        assertFalse(result)
    }
    @Test
    fun allFullTrueTest() {
        val board = Board(
            mutableListOf<MutableList<String>>(mutableListOf("x","x","x"),mutableListOf("o","o","x"),
                mutableListOf("x","o","o")))
        val result = board.allFull()
        assertTrue(result)
    }
    @Test
    fun gameFinishedTestTrue() {
        val board = Board(
            mutableListOf<MutableList<String>>(mutableListOf("x","x","x"),mutableListOf("o","o","x"),
                mutableListOf("x","o","o")))
        val result = board.gameFinished()
        assertTrue(result)
    }
    @Test
    fun gameFinishedFalseTest() {
        val board = Board(
            mutableListOf<MutableList<String>>(mutableListOf("o","x","x"),mutableListOf("o","o","x"),
                mutableListOf("x","o","-")))
        val result = board.gameFinished()
        assertFalse(result)
    }
    @Test
    fun printWinnerOrBoardFullTest(){
        val board = Board(
            mutableListOf<MutableList<String>>(mutableListOf("x","x","o"),mutableListOf("o","o","x"),
                mutableListOf("o","o","-")))
        val result = printWinnerOrBoardFull(board)
        assertEquals("Ganador o",result)
    }
}