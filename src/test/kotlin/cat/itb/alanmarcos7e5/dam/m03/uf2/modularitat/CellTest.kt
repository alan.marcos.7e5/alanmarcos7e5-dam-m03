package cat.itb.alanmarcos7e5.dam.m03.uf2.modularitat

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class CellTest {

    @Test
    fun showInformationTestX() {
        val cell = Cell(4,5)
        val result = cell.showInformation()
        assertEquals("X",result)
    }
    @Test
    fun showInformationTestB() {
        val cell = Cell(4,5,true,true)
        val result = cell.showInformation()
        assertEquals("B",result)
    }
    @Test
    fun showInformationTestAdj() {
        val cell = Cell(4,5,false,true)
        val result = cell.showInformation()
        assertEquals("-1",result)
    }
    @Test
    fun showInformationTestBombNoDiscover() {
        val cell = Cell(4,5,true,false)
        val result = cell.showInformation()
        assertEquals("X",result)
    }
}