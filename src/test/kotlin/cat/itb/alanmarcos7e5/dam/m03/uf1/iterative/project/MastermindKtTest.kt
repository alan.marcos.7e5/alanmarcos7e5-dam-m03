package cat.itb.alanmarcos7e5.dam.m03.uf1.iterative.project

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class MastermindKtTest {

    @Test
    fun `evaluateWord equals`() {
        val secret = "ABCD"
        val guess = "ABCD"
        val result = evaluateWord(secret, guess)
        assertEquals(Evaluation(4,0), result)
    }
    @Test
    fun `evaluateWord with one difference`(){
        val secret = "ABCD"
        val guess = "ABCE"
        val result = evaluateWord(secret, guess)
        assertEquals(Evaluation(3,0), result)
    }

    @Test
    fun `evaluateWord with one in wrong position`(){
        val secret = "ABCD"
        val guess = "ABCA"
        val result = evaluateWord(secret, guess)
        assertEquals(Evaluation(3,1), result)
    }
    /*
    TESTING RANDOMCHAR
     */
    /*@Test
    fun `randomChar `(){
        val result = randomChar()
        val allowedChars = 'A'..'F'
        assertTrue(allowedChars in result)
    }*/
}