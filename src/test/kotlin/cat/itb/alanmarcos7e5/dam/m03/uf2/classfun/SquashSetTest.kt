package cat.itb.alanmarcos7e5.dam.m03.uf2.classfun

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class SquashSetTest {

    @Test
    fun `countPointsTestB service A`() {
        val squashSet = SquashSet("BBBBBBBBBBF")
        val result = squashSet.countPoints()
        assertEquals(listOf(0,9),result)
    }
    @Test
    fun `countPointsTestA service A`() {
        val squashSet = SquashSet("AAAAAAAAAF")
        val result = squashSet.countPoints()
        assertEquals(listOf(9,0),result)
    }
    @Test
    fun `countPointsTest A service B`() {
        val squashSet = SquashSet("AAAAAAAABBBBBBBBBBAABBBABBF","B")
        val result = squashSet.countPoints()
        assertEquals(listOf(8,12),result)
    }

    @Test
    fun findSetWinnerTestA() {
        val squashSet = SquashSet("AAAAAAAAAF")
        val list = squashSet.countPoints()
        val result = squashSet.findSetWinner(list)
        assertEquals("A",result)
    }
    @Test
    fun findSetWinnerTestB() {
        val squashSet = SquashSet("AAAAAAAABBBBBBBBBBAABBBABBF")
        val list = squashSet.countPoints()
        val result = squashSet.findSetWinner(list)
        assertEquals("B",result)
    }
}