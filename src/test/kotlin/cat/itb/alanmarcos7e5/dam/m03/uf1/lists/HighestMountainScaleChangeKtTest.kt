package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class HighestMountainScaleChangeKtTest {

    @Test
    fun convertToPeus() {
        val valueMts = 2.0
        val result = convertToPeus(valueMts)
        assertEquals(6.56,result,0.01)
    }
}