package cat.itb.alanmarcos7e5.dam.m03.uf3.BicingStations.logic

data class LogicOfStation(val bcnStations: BicingStationsBcn){
    val stations get() = bcnStations.data.listOfStations

    /**
     * @return the specified station by id
     */
    fun findStation(id: Int): Station?{
        for (station in stations){
            if (station.id == id) return station
        }
        return null
    }

    /**
     * @param id of the wanted station
     * @return the availables bikes of each station, specifing the type of bike
     */
    fun findAvailablesByStation(id: Int): BikeType?{
        val station = findStation(id)
        return if (station != null) station.availablesTypes else null
    }

    /**
     * @param id of the wanted station
     * @return the availables bikes from an specified station
     */
    fun availablesByStation(id: Int): Int?{
        val station = findStation(id)
        return station?.availables
    }

    /**
     * @return all the availables bikes in Bcn
     */
    fun totalAvailables(): Int{
        var count = 0
        for (station in stations) count += station.availables
        return count
    }

    /**
     * @return all the docks in Bcn
     */
    fun totalDocks(): Int {
        var count = 0
        for (station in stations) count += station.docks
        return count
    }

    /**
     * @return A list of stations that their status are OUT_SERVICE
     */
    fun outOfService(): List<Station>{
        val listOfStation = mutableListOf<Station>()
        for (station in stations) {
            if (station.status != "IN_SERVICE") listOfStation += station
        }
        return listOfStation
    }

}