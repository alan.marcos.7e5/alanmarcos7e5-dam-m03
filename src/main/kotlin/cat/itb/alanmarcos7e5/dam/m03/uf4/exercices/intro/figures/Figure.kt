package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.intro.figures

abstract class Figure(val color: String) { //si es abstracta también será open
    fun paint(){
        prepareColor()
        paintText()
        clearColor()
    }
    abstract fun paintText()
    fun prepareColor() = color
    fun clearColor() = RESET
}