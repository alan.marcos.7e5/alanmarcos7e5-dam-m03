package cat.itb.alanmarcos7e5.dam.m03.uf1.seleccio.exam

import java.util.*

/**
 * La Masovera, de la cançó la masovera, s'ha fet gran i necessita un programa informàtic
 * que li digui què ha de comprar segons els dia de la setmana.
 *   L'usuari ha d'escriure el dia de la setmana en lletres (dilluns, dimarts, etc)
 *   Imprimeix per pantalla què ha de comprar.
 *   dilluns: Compra llums
 *   dimarts: Compra naps
 *   dimecres: Compra nespres
 *   dijous: Compra nous
 *   divendres: Faves tendres
 *   dissabte: Tot s'ho gasta
 *   diumenge: Tot s'ho menja
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val indicator = when (scanner.nextLine()){
        "dilluns" -> "Compra llums"
        "dimarts" -> "Compra naps"
        "dimecres" -> "Compra nespres"
        "dijous" -> "Compra nous"
        "divendres" -> "Faves tendres"
        "dissabte" -> "Tot s'ho gasta"
        "diumenge" -> "Tot s'ho menja"
        else -> "Día inexistente"
    }
    println(indicator)
}