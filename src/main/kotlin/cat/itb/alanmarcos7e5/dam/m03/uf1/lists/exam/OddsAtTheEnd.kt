package cat.itb.alanmarcos7e5.dam.m03.uf1.lists.exam

import cat.itb.alanmarcos7e5.dam.m03.uf1.lists.isPair
import java.util.*

/**
 * L'usuari introduirà enters. Afegeix els enters al inici de la llista si són senars, i al final si són parells.
 * Quan entri el -1 s'acaba el programa i s'imprimeix la llista.
 * input:
 * 1 5 8 2 9 10 77 4 8 -1
 * output:
 * [8, 4, 10, 2, 8, 1, 5, 9, 77]
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val intList = mutableListOf<Int>()
    var inputInt = scanner.nextInt()
    while (inputInt != -1){
        if (inputInt % 2 == 0) intList.add(0, inputInt)
        else intList.add(inputInt)
        inputInt = scanner.nextInt()
    }

    println(intList)
}