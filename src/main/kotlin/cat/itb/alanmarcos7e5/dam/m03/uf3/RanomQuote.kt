package cat.itb.alanmarcos7e5.dam.m03.uf3

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * La següent API ens retorna frases famoses de diferents personatjes històrics: https://api.quotable.io/random
 * Fes un programa que cada cop que s'executi ens mostri una frase diferetn
 * Output
 * Albert Einstein:
 * Learn from yesterday, live for today, hope for tomorrow.
 */
@Serializable
data class Quote(val content: String, @SerialName ("authorSlug") val author: String)

fun createJsonHttpClient(ignore: Boolean) = HttpClient (CIO) {
    install(JsonFeature) {
        serializer = KotlinxSerializer(kotlinx.serialization.json.Json { ignoreUnknownKeys = ignore}) }
}

suspend fun main() {
    val client = createJsonHttpClient(true)
    val quote : Quote = client.get("https://api.quotable.io/random")
    println("${quote.author}: ${quote.content}")
}