package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.exam.gameenemies

import kotlin.math.max

abstract class Enemy(val name: String, var lives: Int) {

    abstract fun attack(intensity: Int)

    open fun printAttacked(intensity: Int){
        if (lives <= 0) println("L'enemic $name està mort")
        else{
            println("L'enemic $name té $lives punts de vida després d'un atac de força $intensity")
        }
    }

    fun decrementLives(quantity: Int){
        lives -= quantity
        lives = max(lives, 0)
    }

    override fun toString(): String {
        return "name: $name - lives: $lives"
    }

}

class Zombie(name: String, lives: Int, private val sound: String): Enemy(name, lives){

    override fun attack(intensity: Int) {
        //lives -= intensity
        decrementLives(intensity)
        //lives = max(lives,0)
        printAttacked(intensity)
    }

    override fun printAttacked(intensity: Int) {
        if (lives <= 0) println("L'enemic $name està mort")
        else{
            println(sound)
            println("L'enemic $name té $lives punts de vida després d'un atac de força $intensity")
        }
    }


}

class Goblin(name: String,lives: Int): Enemy(name, lives){
    override fun attack(intensity: Int) {
        if (intensity > 4) {
            //lives -= 5
            decrementLives(5)
        }
        else {
            //lives--
            decrementLives(1)
        }
        //lives = max(lives,0)
        printAttacked(intensity)
    }

}

class Troll(name: String, lives: Int, private val endurance: Int): Enemy(name, lives){
    override fun attack(intensity: Int){
        if (endurance < intensity){
            //lives -= (intensity - endurance)
            decrementLives(intensity-endurance)
        }
        //lives = max(lives,0)
        printAttacked(intensity)
    }
}