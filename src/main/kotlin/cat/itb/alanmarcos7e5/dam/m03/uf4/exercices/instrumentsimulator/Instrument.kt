package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.instrumentsimulator

abstract class Instrument {
    fun makeSounds(times: Int){
        repeat(times){sound()}
    }
    abstract fun sound()
}