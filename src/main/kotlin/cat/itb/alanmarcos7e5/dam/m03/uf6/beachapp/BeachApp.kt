package cat.itb.alanmarcos7e5.dam.m03.uf6.beachapp

import cat.itb.alanmarcos7e5.dam.m03.uf6.beachapp.data.BeachDao
import cat.itb.alanmarcos7e5.dam.m03.uf6.beachapp.data.BeachDatabase
import cat.itb.alanmarcos7e5.dam.m03.uf6.beachapp.models.Beach
import java.util.*

fun main() {
    BeachApp().start()
}

/**
 * This class has all the Ui operations
 */
class BeachApp(val scanner: Scanner = Scanner(System.`in`), val database: BeachDatabase = BeachDatabase()){
    val beachDao = BeachDao(database)
    fun start() {
        database.connect().use {
            beachDao.createTableIfNotExists()
            executeOperations()
        }
    }

    fun executeOperations() {
        showOptions()
        var inputUser = scanner.nextLine().toInt()
        while (inputUser != -1) {
            when (inputUser){
                1 -> insertBeach()
                2 -> updateBeach()
                3 -> listBeaches()
                4 -> avgWaterQualityByCity()
                else -> println("Wrong input")
            }
            showOptions()
            inputUser = scanner.nextLine().toInt()
        }
    }

    private fun avgWaterQualityByCity() {
        val waterByCity = beachDao.listByWater()
        var i = 1
        /*waterByCity.forEach {
            println("$i.${it.key}: ${it.value}")
            i++
        }*/
        waterByCity.forEach { println("$i.${it.city}: ${it.avgOfWaterQty}") }

    }

    private fun updateBeach() {
        println("Insert id of beach to modify:")
        val id = scanner.nextLine().toInt()
        println("Insert new water qty:")
        val newWaterQuality = scanner.nextLine().toInt()
        beachDao.update(id,newWaterQuality)
    }

    private fun insertBeach() {
        println("Insert id:")
        val id = scanner.nextLine().toInt()
        println("Insert name:")
        val name = scanner.nextLine()
        println("Insert city:")
        val city = scanner.nextLine()
        println("Insert water qty:")
        val waterQuality = scanner.nextLine().toInt()
        val newBeach = Beach(id,name, city, waterQuality)
        beachDao.insert(newBeach)
    }

    private fun listBeaches() {
        beachDao.list().forEach { println(it) }
    }

    private fun showOptions() {
        println("Vols (1) afegir, (2) modificar qualitat, (3) llistar, (4) resum?\n")
    }

}
