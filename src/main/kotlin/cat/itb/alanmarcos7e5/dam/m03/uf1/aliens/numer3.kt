package cat.itb.alanmarcos7e5.dam.m03.uf1.aliens

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val input = scanner.nextInt()
    var elemento = input-1
    val list = mutableListOf<Int>()
    list.add(input)
    while (elemento > 0){
        list.add(elemento)
        elemento--
    }
    var sum = 0
    for (e in list){
        sum += e
    }
    println(sum)
}