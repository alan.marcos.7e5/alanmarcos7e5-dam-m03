package cat.itb.alanmarcos7e5.dam.m03.uf2.functions

import java.util.*

/**
 *Fes un programa (en el fitxer CheapestPrice) que donada la llista de preus (sense decimals) de tot de productes, retorni el preu del producte més baix
 * Input
 * 5 3 4 7 8 -1
 * Output
 * El producte més econòmic val: 3€
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val userList = readIntList(scanner)
    val minOfList = min(userList)
    println("El producto mas econòmico vale: $minOfList$")
}