package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * L'usuari introduirà 2 llistes de valors, com s'indica al mètode readIntList.
 * Printa per pantalla són iguals si ha introduït la mateixa llista, o no són iguals si són diferents
 * Input
 * 2 5 10
 * 2 5 10
 * Output
 * són iguals
 */

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val firstList = readIntList(scanner)
    val secondList = readIntList(scanner)
    println(firstList)
    println(secondList)
    if (firstList == secondList) println("són iguals")
    else println("no són iguals")
}