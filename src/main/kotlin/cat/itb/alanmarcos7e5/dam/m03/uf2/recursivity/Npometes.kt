package cat.itb.alanmarcos7e5.dam.m03.uf2.recursivity

/**
 * Volem fer un programa que escrigui la cançó de N pometes té el pomer (és repeteix fins a que ja no queden pomes)
        N pometes té el pomer,
        de N una, de N una,
        N pometes té el pomer,
        de N una en caigué.

        Si mireu el vent d'on vé
        veureu el pomer com dansa,
        si mireu el vent d'on vé
        veureu com dansa el pomer.
 */
fun main() {
    println(cancionEstado(3))
}


fun estrofa(pometes: Int): String {
    return "$pometes pometes té el pomer,\n" +
            "de $pometes una, de $pometes  una,\n" +
            "$pometes  pometes té el pomer,\n" +
            "de $pometes  una en caigué.\n" +
            "\n" +
            "Si mireu el vent d'on vé\n" +
            "veureu el pomer com dansa,\n" +
            "si mireu el vent d'on vé\n" +
            "veureu com dansa el pomer.\n\n"
}

fun cancionEstado(pometes: Int): String{
    val song = estrofa(pometes)
    if (pometes == 1) {
        return song
    }
    val nextSong = cancionEstado(pometes-1)
    return song + nextSong
}
