import java.util.*

//Escriu un programa que llegeixi l'edat de l'usuari i mostri si té edat per treballar,
//l'edat mínima per treballar legalment és 16 i suposarem l'edat màxima als 65.

fun main() {
    //Leo edad
    val scanner = Scanner(System.`in`)
    val inputAge = scanner.nextInt()
    //comparo utilizando IntRange
    val activeAge = inputAge in 16..65
    println(activeAge)
    println("Adeu")
}