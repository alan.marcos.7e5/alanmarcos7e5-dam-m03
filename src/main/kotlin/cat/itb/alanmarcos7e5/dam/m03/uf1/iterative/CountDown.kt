package cat.itb.alanmarcos7e5.dam.m03.uf1.iterative

import java.util.*

/**
 *  L'usuari introdueix un enter (N) i es mostra per pantalla un compte enrere de N fins a 1.
 */


fun main() {
    val scanner = Scanner(System.`in`)
    val userInput = scanner.nextInt()
    for (values in userInput downTo 1) print(values)
}