package cat.itb.alanmarcos7e5.dam.m03.uf3.EnergyPrice

import cat.itb.alanmarcos7e5.dam.m03.uf3.EnergyPrice.logic.HourRange
import cat.itb.alanmarcos7e5.dam.m03.uf3.createJsonHttpClient
import io.ktor.client.request.*

/**
 * Get the info from the Api
 */

suspend fun loadFromApi(): Map<String, HourRange> {
    val client = createJsonHttpClient(true)
    return client.get("https://api.preciodelaluz.org/v1/prices/all?zone=PCB")
    //val dayPrices : List<HourRange> = dayPriceMap.values.toList() //convert the values of the map into List
}




