package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*
import kotlin.math.abs

/**
 * L'usuari introduix una llista de valors tal i com s'indica al mètode readIntList.
 * Imprimeix per pantalla el valor que està més proper a la mitjana dels valors de la llista
 * (calcula la mitjana dels valors primer i cerca el més proper després).
 * Input
 *  4 .. .. .. ..
 * Output
 *  9
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val valuesList = readIntList(scanner)
    val avgList = (valuesList.sum().toDouble() / valuesList.size) //media de la lista
    var closer = valuesList[0] //inicializo valor más cercano
    var difference = Double.MAX_VALUE //inicializo diferencia como el double más alto que se puede representar
    for (element in valuesList) {
        if (abs(element - avgList) < difference) {
            difference = abs(element - avgList)
            closer = element
        }
    }
    println(closer)
}