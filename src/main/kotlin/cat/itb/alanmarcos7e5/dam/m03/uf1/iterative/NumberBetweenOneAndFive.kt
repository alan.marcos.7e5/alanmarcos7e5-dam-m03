package cat.itb.alanmarcos7e5.dam.m03.uf1.iterative

import java.util.*

/**
 * Demanar a l'usuari un enter entre 1 i 5.
 * Si introdueix un número més gran o més petit, torna-li a demanar l'enter.
 * Imprimeix per pantalla: El número introduït: 3, substituint el 3 pel número.
 * input: 10 7 3
 * output: El número introduït: 3
 */

fun main() {
    val scanner = Scanner(System.`in`)
    var inputUser : Int
    do {
        print("Ingresa un entero entre 1 y 5: ")
        inputUser = scanner.nextInt()
    }while (inputUser !in 1..5)
    println("El número introducido es $inputUser")
}