package cat.itb.alanmarcos7e5.dam.m03.uf1.adventOfCode

import java.util.*
import kotlin.io.path.Path

/**
 *
 */

fun main() {
    val scanner = Scanner(Path("data/aventofcode/inputEx1.txt"))
    val inputList = mutableListOf<Int>()
    while (scanner.hasNext()) {
        val number = scanner.nextInt()
        inputList.add(number)
    }
    //PART 1
    val result = calculateIncreases(inputList)
    println("PART 1 = $result")
    //PART 2
    val result2 = secondPart(inputList)
    println("PART 2 = $result2")
}

fun calculateIncreases(list : List<Int>) : Int{
    var totalIncreases = 0
    for (i in 0..list.lastIndex) {
        if (i != 0) {
            if (list[i] > list[i-1]) totalIncreases++
        }
    }
    return totalIncreases
}

fun secondPart(list : List<Int>) : Int{
    var totalIncreases = 0
    var previousSumOfElements = 0
    val testingList = mutableListOf<Int>()
    for ((i,element) in list.withIndex()) {
        if (i <= (list.lastIndex - 2)) {
            val sumOfElements = element + list[i+1] + list[i+2]
            testingList.add(sumOfElements)
            if (i != 0) {
                if (sumOfElements > previousSumOfElements) totalIncreases++
            }
            previousSumOfElements = sumOfElements
        }
    }
    println(testingList)
    return totalIncreases
}