package cat.itb.alanmarcos7e5.dam.m03.uf5.exercices.lists

import java.util.Scanner

/**
 * Donada una llista d'enters (el primer número és el nombre d'enters de la llista),
 * elimina els que acaben amb 3, ordena'ls de major a menor, i imprimieix-los per pantalla.
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val size = scanner.nextInt()
    val myList = List(size){scanner.nextInt()}
    myList.filter { it % 10 != 3 }.sortedDescending().forEach { println(it) }
}