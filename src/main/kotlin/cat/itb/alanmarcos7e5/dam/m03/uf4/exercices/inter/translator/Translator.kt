package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.inter.translator

class Translator(val userInput: String): ITranslator {

    override fun translate(line: String): String {
        return "$line(traducido)"
    }

    override var translatedText: String = ""
        get() = changeUserInput(userInput)


}