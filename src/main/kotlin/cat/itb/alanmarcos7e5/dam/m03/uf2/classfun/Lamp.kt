package cat.itb.alanmarcos7e5.dam.m03.uf2.classfun

import java.util.*

/**
 *Implementa un programa que simuli una làmpara, amb les accions:
* TURN ON: Encén la làmpara
* TURN OFF: Apaga la làmpara
* TOGGLE: Canvia l'estat de la làmpara
* Després de cada acció mostra si la làmpara està encesa.
* Exemple
* Les línies que comencen per > són el que ha escrit l'usuari
* > TURN OFF
* false
* > TURN ON
* true
* > TURN OFF
* false
* > TOGGLE
* true
* > TOGGLE
* false
* > END
 */

data class Lamp(var state: Boolean){
    fun turnOn(){
        this.state = true
    }
    fun turnOff(){
        state = false
    }
    fun toogle(){
        state = !state
    }
    fun toOutput()=state.toString()
    fun action(userRequest: String) {
        when (userRequest){
            "TURN OFF" -> turnOff()
            "TURN ON" -> turnOn()
            "TOOGLE" -> toogle()
            else -> "Error"
        }
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    val lamp = Lamp(false)
    var userRequest = scanner.nextLine()
    while (userRequest != "END") {
        lamp.action(userRequest)
        println(lamp.toOutput())
        userRequest = scanner.nextLine()
    }
}


