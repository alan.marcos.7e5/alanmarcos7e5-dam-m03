package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.intro

class MovingMechanicalArm(var position: Double = 0.0): MechanicalArm() {

        fun move(movement: Double){
            if (turnedOn) {
                val newPosition = position + movement
                position = if (newPosition < 0.0) 0.0
                else newPosition
            }
        }
}
//no hace falta declarar estos valores del constructor, porque en clase padre tienen valores por defecto.
//openAngle: Double = 0.0, altitude: Double = 0.0, turnedOn: Boolean = false,
//openAngle, altitude, turnedOn