package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.quiz

import java.util.*

class Quiz(val scanner: Scanner, val listQuestions: List<Question>) {

    fun getCorrectAnswers(): Int {
        var correctsAnswers = 0
        for (question in listQuestions) if (question.userAnswer) correctsAnswers++
        return correctsAnswers
    }

    fun startQuiz(){
        for (question in listQuestions){
            question.printQuestion()
            println("Enter your answer: ")
            question.setUserAnswer(scanner.nextLine())
        }
    }


}