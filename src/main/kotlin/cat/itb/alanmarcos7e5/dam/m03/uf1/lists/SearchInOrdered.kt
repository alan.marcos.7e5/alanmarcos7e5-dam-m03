package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * Donat una llista d'enters indica si un cert valor existeix en el bucle, sense utilitzar l'any
 */

fun main() {
    val scanner = Scanner(System.`in`)
    var output = false
    val myInputList = List(10) {scanner.nextInt()}
    val myInputNumber = scanner.nextInt()
    for (element in myInputList) {
        if (element == myInputNumber) {
            output = true
            break
        }
    }
    println(output)
}