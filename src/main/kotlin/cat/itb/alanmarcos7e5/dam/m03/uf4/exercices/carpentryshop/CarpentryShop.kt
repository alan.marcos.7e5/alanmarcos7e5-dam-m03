package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.carpentryshop

import java.util.*

/**
 * Feu programa per fer els tiquets de venta.
 * A la fusteria venen taulells de fusta i llistons.
 *  -Els taulells de fusta tenen un preu per metre quadrat. Quan es venen es ven una llargada i una amplada.
 *  -Els llistons es venen per un preu al metre i es venen per metres.
 * Fes un programa que donada una llista de llistons i taulells en calculi el preu total.
 */
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val size = scanner.nextInt()
    scanner.nextLine()
    val listOfProductStr = List(size){scanner.nextLine()}
    val listOfProductObj = instanceObjects(listOfProductStr)
    val totalPrice = calculateTotalPrice(listOfProductObj)
    println("""El precio total es de: $$totalPrice""")
}

fun calculateTotalPrice(listOfProductObj: MutableList<WoodProduct?>): Double {
    var totalPrice = 0.0
    for (product in listOfProductObj) {
        if (product != null) totalPrice += product.totalPrice
    }
    return totalPrice
}

fun instanceObjects(listOfProduct: List<String>): MutableList<WoodProduct?> {
    val listOfProductObj = mutableListOf<WoodProduct?>()
    for (productStr in listOfProduct) {
        listOfProductObj += productToObj(productStr)
    }
    return listOfProductObj
}

fun productToObj(product: String): WoodProduct? {
    val list = product.split(" ")
    return when (list[0]){
        "Taulell" -> WoodTable(list[1].toDouble(),list[2].toDouble(), list[3].toDouble())
        "Llistó" -> WoodStrip(list[1].toDouble(), list[2].toDouble())
        else -> null
    }
}
