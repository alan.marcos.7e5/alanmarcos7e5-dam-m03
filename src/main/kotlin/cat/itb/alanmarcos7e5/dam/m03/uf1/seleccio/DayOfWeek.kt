package cat.itb.alanmarcos7e5.dam.m03.uf1.seleccio

import java.util.*

//Donat un enter, printa el dia de la setmana amb text (dilluns, dimarts, dimecres…).

fun main() {
    val scanner = Scanner(System.`in`)
    val inputUserDay = scanner.nextInt()
    val dayOfWeek = when (inputUserDay){
        1 -> "dilluns"
        2 -> "dimarts"
        3 -> "dimecres"
        4 -> "dijous"
        5 -> "divendres"
        6 -> "dissabte"
        7 -> "diuminge"
        else -> "Día inexistente"
    }
    println(dayOfWeek)
}