package cat.itb.alanmarcos7e5.dam.m03.uf2.generalexam

import java.util.*
/*
1
Prestige
OilCompany
4245
0.7

1
DeepwaterHorizon
PetroCo
24547
0.5

1
Bahamas2011
OilCompany
22547
0.5
*/
/**
 * OilSpillArchiveUi is a data class that allows user interact
 * with the program selecting the diferents showed options.
 */
data class OilSpillArchiveUi(val scanner: Scanner = Scanner(System.`in`).useLocale(Locale.UK)){
    val logicOil = LogicOil()

    /**
     * Initialize the program and shows the user options
     */
    fun start(){
        println(userOptions())
        var userChoice = scanner.nextLine().toInt()
        while (userChoice != 0) {
            when(userChoice){
                1 -> addSpillUi()
                2 -> listOfSpillUi()
                3 -> worstSpillUi()
                4 -> spillLitresUi()
                5 -> spillGravityUi()
                6 -> spillsForCompanyUi()
            }
            println(userOptions())
            userChoice = scanner.nextLine().toInt()
        }
    }
    //todo (make function to pretty print the spills)

    /**
     * Shows the liters of spilled oil from a spill name inputted by user
     * @see LogicOil.spillLitres
     */
    fun spillLitresUi() {
        val name = scanner.nextLine()
        val litres = logicOil.spillLitres(name)
        if (litres != null) println(litres)
        else "Can not find"
    }

    /**
     * Shows the gravity of spilled oil from a spill name inputted by user
     * @see LogicOil.spillGravity
     */
    fun spillGravityUi() {
        val name = scanner.nextLine()
        val gravity = logicOil.spillGravity(name)
        if (gravity != null) println(gravity)
        else "Can not find"
    }
    /**
     * Shows in a list the spills provoked from a company inputted by user
     * @see LogicOil.spillsCompany
     */
    //todo (add also de size of the list)
    fun spillsForCompanyUi() {
        val company = scanner.nextLine()
        println(logicOil.spillsCompany(company))
    }

    /**
     * Shows the info of the most gravity oil spilled
     * @see LogicOil.searchWorst
     */
    fun worstSpillUi() {
        val worst = logicOil.searchWorst()
        if (worst != null) println(worst) else println("There are not spills added")
    }

    /**
     * Shows all the archived spilled in a list
     */
    fun listOfSpillUi() {
        println(logicOil.listOfSpills)
    }

    /**
     * The interface to input a new spill in the archive
     * @see LogicOil.addSpill
     */
    fun addSpillUi() {
        val name = scanner.nextLine()
        val company = scanner.nextLine()
        val litres = scanner.nextInt()
        val toxicity = scanner.nextDouble()
        val spill = Spill(name, company, litres, toxicity)
        logicOil.addSpill(spill)
        scanner.nextLine()
    }


}
fun main() {
    OilSpillArchiveUi().start()
}

fun userOptions():String{
    return "Chose option:\n" +
            "1. Add oil spill\n" +
            "2. List oil spills\n" +
            "3. Worst oil spill\n" +
            "4. Show Spill litters\n" +
            "5. Show spill gravity\n" +
            "6. Spills for company\n" +
            "7. Worst company\n" +
            "0. Exit"
}