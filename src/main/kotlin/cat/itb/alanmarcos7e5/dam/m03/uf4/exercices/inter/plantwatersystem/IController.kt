package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.inter.plantwatersystem

interface IController {
    fun getHumidityRecord() : List<Double>
    fun startWatterSystem()
}