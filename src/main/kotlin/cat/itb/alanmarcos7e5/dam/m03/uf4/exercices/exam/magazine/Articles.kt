package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.exam.magazine

abstract class Article(val title: String, val author: String): Printable {
}

class Opinion(title: String, author: String, val text: String): Article(title, author) {
    override fun print(magazinePrinter: MagazinePrinter) {
        magazinePrinter.printTitle("Opinió: $title", author)
        magazinePrinter.printText(text)
    }
}

class Photographic(title: String,author: String, val listPhoto: List<String>): Article(title, author){
    override fun print(magazinePrinter: MagazinePrinter) {
        magazinePrinter.printTitle("Fotos: $title", author)
        magazinePrinter.printPhotos(listPhoto)
    }
}

