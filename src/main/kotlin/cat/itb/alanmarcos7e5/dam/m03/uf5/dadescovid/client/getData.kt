package cat.itb.alanmarcos7e5.dam.m03.uf5.dadescovid.client

import cat.itb.alanmarcos7e5.dam.m03.uf3.Tree
import cat.itb.alanmarcos7e5.dam.m03.uf3.createJsonHttpClient
import cat.itb.alanmarcos7e5.dam.m03.uf5.dadescovid.logic.Codes
import cat.itb.alanmarcos7e5.dam.m03.uf5.dadescovid.logic.DayData
import cat.itb.alanmarcos7e5.dam.m03.uf5.dadescovid.logic.Population
import io.ktor.client.request.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.io.path.readText

/**
 * This is to get de data from the API client
 */

suspend fun loadFromApi(): DayData{
    val client = createJsonHttpClient(true)
    return client.get("https://api.covid19api.com/summary")
}
/**
 * This is to get data from JSON file
 */
fun loadPopulationFile(): List<Population> {
    val filePath = obtainPath("Documents/DAMr1A/m03/JSON/population.json")
    return readPopulationFile(filePath)
}
fun loadCodesFile(): List<Codes>{
    val filePath = obtainPath("Documents/DAMr1A/m03/JSON/country_codes.json")
    return readCodesFile(filePath)
}

fun readCodesFile(filePath: Path): List<Codes> {
    //uncomment next line  and comment the next one to use the path
    // of the downloaded file
    //val textOfFile = filePath.readText()
    val textOfFile = Tree::class.java.getResource("/country_codes.json").readText()
    val serializer = Json { ignoreUnknownKeys=true }
    return serializer.decodeFromString<List<Codes>>(textOfFile)
}

fun readPopulationFile(filePath: Path): List<Population> {
    //uncomment next line  and comment the next one to use the path
    // of the downloaded file
    //val textOfFile = filePath.readText()
    val textOfFile = Tree::class.java.getResource("/population.json").readText()
    val serializer = Json { ignoreUnknownKeys=true }
    return serializer.decodeFromString<List<Population>>(textOfFile)
}

fun obtainPath(filePath: String): Path {
    val homePath = Path(System.getProperty("user.home"))
    return homePath.resolve(filePath)
}
