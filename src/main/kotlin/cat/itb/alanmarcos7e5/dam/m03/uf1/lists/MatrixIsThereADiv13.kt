package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * Donada la següent matriu
 * val matrix = listOf(listOf(2,5,1,6),listOf(23,52,14,36),listOf(23,75,81,62))
 * Imprimeix true si algún dels números és divisible entre 13, false altrement.
 */

fun main() {
    val matrix = listOf(listOf(2,5,1,6),listOf(23,52,14,36),listOf(23,75,81,62))
    print(matrixDivisibleBy13(matrix))
}

fun matrixDivisibleBy13(matrix : List<List<Int>>) : Boolean{
    var divisibleBy13 = false
    for (row in matrix) {
        for (cell in row) {
            if (cell % 13 == 0) {
                divisibleBy13 = true
                break
            }
        }
    }
    return divisibleBy13
}