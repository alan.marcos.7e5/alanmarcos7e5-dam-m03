package cat.itb.alanmarcos7e5.dam.m03.uf3

import java.util.*
import kotlin.io.path.Path

/**
 * Fes un programa que donat un path i un nom de fitxer, miri si hi ha algun fitxer a dins el path amb aquest nom (mirar a dins de carpetes també).
 * Per cada ocurrencia, imprimeix el path per pantalla.
 * Input
 * /home/user/
 * file.txt
 * output
 * file.txt
 * gsad1/dsa/file.txt
 * zsa/file.txt
 */

fun main() {
    //val scanner = Scanner(System.`in`)
    //val inputPath = scanner.nextLine()
    val debug = "/home/sjo"
    val debugFile = "file.txt"
    val file = Path(debug).toFile()
    file.walk().forEach {
        if (it.isFile){
            if (it.name == debugFile) println("EXISTS")
        }
    }

}