package cat.itb.alanmarcos7e5.dam.m03.uf5.dadescovid.ui

import cat.itb.alanmarcos7e5.dam.m03.uf5.dadescovid.client.loadCodesFile
import cat.itb.alanmarcos7e5.dam.m03.uf5.dadescovid.client.loadFromApi
import cat.itb.alanmarcos7e5.dam.m03.uf5.dadescovid.client.loadPopulationFile
import cat.itb.alanmarcos7e5.dam.m03.uf5.dadescovid.logic.Codes
import cat.itb.alanmarcos7e5.dam.m03.uf5.dadescovid.logic.DayData
import cat.itb.alanmarcos7e5.dam.m03.uf5.dadescovid.logic.Population

/**
 * UI, main file
 */

suspend fun main() {
    val dayData = loadFromApi()
    val listPopulation = loadPopulationFile()
    val listCodes = loadCodesFile()
    Ui(dayData,listPopulation,listCodes).start()
}

class Ui(val dayData: DayData, val listPopulation: List<Population>, val listCodes: List<Codes>){

    fun start(){
        top10DeathsUi()
        println()
        top10NewConfUi()
        println()
        dataUe()
        println()
        rateByCountry("Spain")
        println()
        top10PerCapita()
    }

    private fun rateByCountry(countryName: String) {
        val rate = dayData.searchRateByCountryName(listPopulation,listCodes,countryName)
        if (rate != null) {
            println("### $countryName Relative ###\n" +
                    "#### ${countryName.uppercase()} ####")
            println("Deaths per Capita: $rate")
        }else{
            println("The indicated country does not exists")
        }
    }

    private fun top10PerCapita() {
        val top10 = dayData.top10PerCapita(listPopulation,listCodes)
        println("#### Death By Population ####")
        var position = 1
        top10.forEach {
            println("${position}. ${it.name}: ${it.totalDeaths} population: ${it.population} rate: ${it.rate} %")
            position++
        }

    }

    private fun dataUe() {
        val dataUe = dayData.ueData()
        println("#### EU data ####")
        for ((k, v) in dataUe) println("$k: $v")
    }

    private fun top10NewConfUi() {
        val top10NewConfList = dayData.top10NewConfirmed()
        println("#### New confirmed ####")
        var position = 1
        top10NewConfList.forEach {
            println("$position. ${it.name} ${it.newConfirmed}")
            position++
        }
    }

    private fun top10DeathsUi() {
        val top10DeathsList = dayData.top10Deaths()
        println("#### Most deaths: ####")
        var position = 1
        top10DeathsList.forEach {
            println("${position}. ${it.name} ${it.totalDeaths}")
            position++
        }
    }

}