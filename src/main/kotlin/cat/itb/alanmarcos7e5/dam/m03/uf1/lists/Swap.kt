package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * Donada una llista de 4 números de tipus Int, intercanvia el primer per l'últim element.
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val listOfInt = MutableList(4){
        scanner.next().toInt()
    }
    println(listOfInt)
    val firstValue = listOfInt[0]
    val lastValue = listOfInt[3]
    listOfInt[0] = lastValue
    listOfInt[3] = firstValue
    println(listOfInt)
}