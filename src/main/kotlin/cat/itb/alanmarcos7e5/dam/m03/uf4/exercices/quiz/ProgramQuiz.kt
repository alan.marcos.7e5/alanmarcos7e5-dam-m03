package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.quiz

import java.util.*

/**
Volem fer un petit programa de preguntes.
Tenim dos tipus de preguntes:

opció múltiple
text lliure
Necessitaràs les següents classes

Question
FreeTextQuestion
MultipleChoiseQuestion
Quiz (conté una llista de preguntes)
Pas 1: Fes l'estructura de dades que et permeti guardar aquestes preguntes.
Pas 2: Fes un programa que, donat un quiz creat per codi, imprimeixi les preguntes.
Pas 3: Permet que l'usuari pugui respondre les preguntes.
Pas 4: Un cop acabat, imprimeix quantes respostes són correctes.
 */
fun main() {
    Ui().start()
}
class Ui(){
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)

    val question1 =
        FreeText("Capital de Argentina?","Buenos Aires")
    val question2 =
        MultipleChoice(listOf<String>("a-Madrid","b-Murcia"),"Capital de España?","a")

    val listQuestions = listOf<Question>(question1,question2)

    val quiz = Quiz(scanner,listQuestions)

    fun start() {
        quiz.startQuiz()
        toOutput()
    }
    fun toOutput(){
        println("Correct answers: ${quiz.getCorrectAnswers()}")
    }

}

