package cat.itb.alanmarcos7e5.dam.m03.uf2.classfun

import cat.itb.alanmarcos7e5.dam.m03.uf1.lists.readStringList
import java.util.*

/**
 * Input
 * 4
 * AAAAAAAAAF
 * BBBBBBBBBBF
 * AAAAAAAABBBBBBBBBBAABBBABBF
 * BF
 * Output
 * 9-0
 * 0-9
 * 9-11 0-1
 * 0-0
 */

data class SquashSet(val points: String, var service: String? = "A"){
    //fun findResult(list: List<Int>): List<Int> =

    fun countPoints(): List<Int>{
        var pointsA = 0
        var pointsB = 0
        for (c in points) {
            if (service=="A") {
                when(c){
                    'A'-> pointsA += 1
                    'B'-> service = "B"
                    else-> break
                }
            }else{
                when(c){
                    'A'-> service = "A"
                    'B'-> pointsB += 1
                    else-> break
                }
            }
        }
        return listOf(pointsA,pointsB)
    }
    fun findSetWinner(list: List<Int>): String?{
        var pointsA = list[0]
        var pointsB = list[1]
        return if (pointsA > pointsB) "A"
        else if (pointsA < pointsB) "B"
        else null
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    val pointsList = readStringList(scanner)
    println(pointsList)
    logic(pointsList)
}

fun logic(stringList: List<String>) {
    var serviceOfSet: String? = "A"
    for (e in stringList) {
        val squashSet = SquashSet(e,serviceOfSet)
        val pointsOfSet = squashSet.countPoints()
        serviceOfSet = squashSet.findSetWinner(pointsOfSet)
        println(toOutput(pointsOfSet))
    }
}
fun toOutput(list: List<Int>): String = "${list[0]} - ${list[1]}"


