package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * Volem fer un simulador d'un candau.
 * La nostra versió, taḿbé tindrà 8 botons, però el primer serà el 0.
 * A l'inici tots els botons estaran sense apretar.
 * L'usuari introduirà enters indicant quin botó ha d'apretar (o desapretar)
 * Quan introdueixi el -1, és que ja ha acabat i hem d'imprimir l'estat del candau
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val listPadlock = MutableList(8) { false }
    var userButton = scanner.next().toInt()
    while (userButton != -1){
        listPadlock[userButton] = !listPadlock[userButton]
        userButton = scanner.nextInt()
    }
    println(listPadlock)
}