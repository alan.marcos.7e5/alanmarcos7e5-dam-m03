package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * Printa per pantalla cap i cua si la llista de N valors introduïts per l'usuari són cap i cua
 * (llegits en ordre invers és la mateixa llista).
 * Input
 * 4 10 5 5 10
 * Output
 * cap i cua
 */

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val userList = readIntList(scanner)
    if (userList == userList.asReversed()) println("cap i cua")
    else println("no")
}