package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.intro

abstract class Vehicle(val name: String, val brand: VehicleBrand): Comparable<Vehicle> {
    override fun compareTo(other: Vehicle) = name.compareTo(other.name)
}