package cat.itb.alanmarcos7e5.dam.m03.uf1.seleccio

import java.util.*

//L'usuari escriu un valor que representa una nota
//Imprimeix "Excelent", "Notable", "Bé", "Suficient", "Suspès",
//"Nota invàlida" segons el la nota numèrica introduïda

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val inputNote = scanner.nextDouble()
    var outputAnswer = "Nota invàlida"
    if (inputNote > 0 && inputNote <= 10){
        if (inputNote < 5) outputAnswer = "Suspès"
        if (inputNote in 5.0..6.0) outputAnswer = "Suficient"
        if (inputNote in 6.0..7.5) outputAnswer = "Bè"
        if (inputNote in 7.5..9.0) outputAnswer = "Notable"
        if (inputNote > 9) outputAnswer = "Excelent"
        println(outputAnswer)
    }else{
        println(outputAnswer)
    }
}