package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.inter.plantwatersystem

import kotlin.random.Random

class PlantWaterController(var waterSystem: Boolean = false, var averageOfMeasures: Double? = null) : IController {

    override fun getHumidityRecord(): List<Double> {
        return List(20) { Random.nextDouble(-3.0, 15.0) }
    }

    override fun startWatterSystem() {
        val listOfHumidity = getHumidityRecord()
        waterSystem = calculateAvg(listOfHumidity) <= 10.0
    }

    fun calculateAvg(listOfHumidity: List<Double>): Double {
        averageOfMeasures = listOfHumidity.average()
        return averageOfMeasures!!
    }

}