package cat.itb.alanmarcos7e5.dam.m03.uf2.generalexam

/**
 * Spill is a data class to store information of a spilled oil from an oil base.
 * @param[name] name of the oil spill
 * @param[company] name of the company owner of the oil spill
 * @param[litres] amount of spilled oil liters
 * @param[toxicity] toxicity of the spilled oil
 */

data class Spill(val name: String,val company: String,val litres: Int,val toxicity: Double) {
    val gravity get()  = litres * toxicity
}