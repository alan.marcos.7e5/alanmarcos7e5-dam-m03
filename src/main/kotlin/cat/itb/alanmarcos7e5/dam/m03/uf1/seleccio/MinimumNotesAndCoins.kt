package cat.itb.alanmarcos7e5.dam.m03.uf1.seleccio

import java.util.*

//L'usuari introdueix una quantitat d'euros.
//Printa per pantalla el número mínim de cada tipus de bitllets i monedes per tenir aquesta quantitat

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val inputEuros = scanner.nextDouble() * 1000

    //logica de programa
    //500
    val notes500 = inputEuros.toInt() / 500000
    val rest500 = inputEuros % 500000
    //100
    val notes100 = rest500.toInt() / 100000
    val rest100 = rest500 % 100000
    //50
    val notes50 = rest100.toInt() / 50000
    val rest50 = rest100 % 50000
    //20
    val notes20 = rest50.toInt() / 20000
    val rest20 = rest50 % 20000
    //10
    val notes10 = rest20.toInt() / 10000
    val rest10 = rest20 % 10000
    //5
    val notes5 = rest10.toInt() / 5000
    val rest5= rest10 % 5000
    //1
    val notes1 = rest5.toInt() / 1000
    val rest1 = rest5 % 1000
    //0.5
    val notes50Cent = rest1.toInt() / 500
    val rest50Cent = rest1 % 500
    //0.2
    val notes20Cent = rest50Cent.toInt() / 200
    val rest20Cent = rest50Cent % 200
    //0.1
    val notes10Cent = rest20Cent.toInt() / 100
    val rest10Cent = rest20Cent % 100
    //0.05
    val notes5Cent = rest10Cent.toInt() / 50
    val rest5Cent = rest10Cent % 50
    //0.01
    val notes1Cent = rest5Cent.toInt() / 10
    val rest1Cent = rest5Cent % 10

    //salida por pantalla
    if (notes500 != 0) println("$notes500 de 500")
    if (notes100 != 0) println("$notes100 de 100")
    if (notes50 != 0) println("$notes50 de 50")
    if (notes20 != 0) println("$notes20 de 20")
    if (notes10 != 0) println("$notes10 de 10")
    if (notes5 != 0) println("$notes5 de 5")
    if (notes1 != 0) println("$notes1 de 1")
    if (notes50Cent != 0) println("$notes50Cent de 50 centimos")
    if (notes20Cent != 0) println("$notes20Cent de 20 centimos")
    if (notes10Cent != 0) println("$notes10Cent de 10 centimos")
    if (notes5Cent != 0) println("$notes5Cent de 5 centimos")
    if (notes1Cent != 0) println("$notes1Cent de 1 centimos")

}