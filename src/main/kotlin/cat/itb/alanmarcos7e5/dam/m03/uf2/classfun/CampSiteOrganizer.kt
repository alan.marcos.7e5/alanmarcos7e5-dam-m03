package cat.itb.alanmarcos7e5.dam.m03.uf2.classfun

import java.util.*

/**
 * Volem fer un petit programa per gestionar un camping. Volem tenir controlat quantes parcel·les tenim plenes i quanta gent tenim.
Quan s'ocupi una parcel·la l'usuari introduirà el número de persones i el nom de la reserva
ENTRA 2 Maria
Quan es buidi una parcel·la l'usuari introduirà el nom de la reserva
MARXA Maria
Per tancar el programa l'usuari introduirà el nom END
Cada cop que marxi algú, imprimeix el número total de persones que hi ha, i el número de parcel·les plenes.

Exemple
Les línies que comencen per > són el que ha escrit l'usuari
> ENTRA 2 Maria
parcel·les: 1
persones: 2
> ENTRA 5 Mar
parcel·les: 2
persones: 7
> MARXA Maria
parcel·les: 1
persones: 5
> END
 */

data class Camping(val listOfReserves: MutableList<Customer> = mutableListOf<Customer>()){
    val parcels get() = listOfReserves.size

    fun addCustomer(customer: Customer) {
        listOfReserves += customer
    }
    fun removeCustomer(customerName: String){
        for (e in listOfReserves) if (e.name == customerName) listOfReserves.remove(e)
    }
    fun persons(): Int {
        var persons = 0
        for (e in listOfReserves) {
            persons += e.persons
        }
        return persons
    }
    fun action(userInput: List<String>) {
        when(userInput[0]){
            "ENTRA"->{
                val customer = Customer(userInput[2],userInput[1].toInt())
                addCustomer(customer)
                toOutput()

            }
            "MARXA"->{
                removeCustomer(userInput[1])
                toOutput()
            }
        }
    }
    fun toOutput() {
        println("Parcelas: $parcels")
        println("Personas: ${persons()}")
    }
}
data class Customer(val name: String, val persons: Int)

fun main() {
    val scanner = Scanner(System.`in`)
    val camping = Camping()
    var reserveInput = scanner.nextLine()
    while ("END" !in reserveInput) {
        val splitedInput = reserveInput.split(" ")
        camping.action(splitedInput)
        reserveInput = scanner.nextLine()
    }
}
