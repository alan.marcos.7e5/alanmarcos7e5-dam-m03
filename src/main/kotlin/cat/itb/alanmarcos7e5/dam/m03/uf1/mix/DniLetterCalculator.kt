package cat.itb.alanmarcos7e5.dam.m03.uf1.mix

import java.math.BigInteger
import java.util.*

/**
 * Donat el número d'un dni, es pot calcular la lletra usant la següent formula
 * L'usuari introduirà el número del dni
 * Imprimeix el dni amb lletra inclosa
 * input: 12345678
 * output: 12345678Z
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val dniUser = scanner.nextLong()
    println(calculateLetter(dniUser))
}

fun calculateLetter(dni: Long): String {
    val letter = when ((dni % 23).toInt()) {
        0 ->  "T"
        1 ->  "R"
        2 ->  "W"
        3 ->  "A"
        4 ->  "G"
        5 ->  "M"
        6 ->  "Y"
        7 ->  "F"
        8 ->  "P"
        9 ->  "D"
        10 ->  "X"
        11 ->  "B"
        12 ->  "N"
        13 ->  "J"
        14 ->  "Z"
        15 ->  "S"
        16 ->  "Q"
        17 ->  "V"
        18 ->  "H"
        19 ->  "L"
        20 -> "C"
        21 -> "K"
        22 -> "E"
        else -> "Fail"
    }
    return dni.toString() + letter
}