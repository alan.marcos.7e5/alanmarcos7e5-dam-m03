package cat.itb.alanmarcos7e5.dam.m03.uf1.aliens

import java.util.*

/**
 * Sample Input 0

8 10 0
Sample Output 0

16
20
 */
fun main() {
    val scanner = Scanner(System.`in`)
    var list = mutableListOf<Int>()
    var input = scanner.nextInt()
    while (input!=0){
        list.add(input)
        input = scanner.nextInt()
    }

    for (element in list) {
        if (element!=0) print("${element*2} ")
    }

}