package cat.itb.alanmarcos7e5.dam.m03.uf3

import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.io.path.createDirectories
import kotlin.io.path.createFile

/**
 * Crea la carpeta ~/dummyfolders que contingui 100 carpetes numerades del 1 al 100.
 * Nota Pots obtenir la carpeta home amb la següent comanda
 * String homePath = System.getProperty("user.home");
 */
fun main() {
    val homePath = System.getProperty("user.home") // obtengo el path del usuario (String)
    val path = Path(homePath) //creo un Path con el string del home de usuario (Path)
    val filePath = path.resolve("dummyfolders") //obtengo el Path donde se encuentra path y creo un path "dummyflowers"
    filePath.createDirectories() //al path dummyflowers lo hago directorio
    createFolders(filePath) //para crear las carpetas dentro hago una función recursiva y repito casi los mismos pasos pero para cada carpeta nueva
}
fun createFolders(filePath: Path, name: Int = 1){
    val newFolder = filePath.resolve(name.toString())
    newFolder.createDirectories()
    val newFile = newFolder.resolve("archivo $name")
    newFile.createFile()
    if (name < 100) createFolders(filePath, name+1)
}
