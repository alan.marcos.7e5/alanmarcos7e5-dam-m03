package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.quiz

abstract class Question(val ask: String, val programAnswer: String, var userAnswer: Boolean = false) {

    abstract fun printQuestion()

    fun setUserAnswer(newUserAnswer: String) {
        userAnswer = programAnswer.lowercase() == newUserAnswer.lowercase()
    }


}