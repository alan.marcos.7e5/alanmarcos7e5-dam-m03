package cat.itb.alanmarcos7e5.dam.m03.uf1.data

import java.util.*

//L'usuari escriu un enter amb la seva edat i s'imprimeix ets major d'edat si és major d'edat

fun main() {
    //read age
    val scanner = Scanner(System.`in`)
    val inputAge = scanner.nextInt()
    //compare if is adult
    val isAdult : Boolean
    isAdult = inputAge >= 18
    if (isAdult)
        println("ets major d'edat")
}