package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * Compararé las listas elemento a elemento.
 */

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val firstList = readIntList(scanner)
    val secondList = readIntList(scanner)
    var equals = true
    if (firstList.size == secondList.size) {
        for ((i,element) in firstList.withIndex()) {
            if (element != secondList[i]) {
                equals = false
                break
            }else continue
        }
    }else equals = false
    if (equals) println("són iguals")
    else println("no son iguals")
}