package cat.itb.alanmarcos7e5.dam.m03.uf2.recursivity

import cat.itb.alanmarcos7e5.dam.m03.uf1.lists.readIntList
import java.util.*

/**
 * Este fichero es para rehacer algunos ejercicios de recursividad
 * o implementar nuevos, de cara al exámen
 */
fun main() {
    val scanner = Scanner(System.`in`)
    selectExcercise(scanner)
}

fun selectExcercise(scanner: Scanner) {
    println(userOptions())
    var userSelection = scanner.nextInt()
    while (userSelection != 0) {
        when(userSelection){
            1 -> {
                println("MULT:\n")
                val a = scanner.nextInt()
                val b = scanner.nextInt()
                println(multRecursive(a,b))
            }
            2 -> {
                println("POW:\n")
                val a = scanner.nextInt()
                val b = scanner.nextInt()
                println(powRecursive(a,b))
            }
            3 -> {
                println("DOT LINE:\n")
                val times = scanner.nextInt()
                println(dotRecursive(times))
            }
            4 -> {
                println("FACT:\n")
                val number = scanner.nextInt()
                println(factRecursive(number))
            }
            5 -> {
                println("SEARCH MAX IN LIST:\n")
                val userList = readIntList(scanner)
                println(searchMaxValueRecursive(userList))
            }
            6 -> {
                println("FIGURES:\n")
                val number = scanner.nextInt()
                println(getFiguresRecursive(number))
            }
            7 -> {
                println("FAMILY TREE:\n")
                val person = Person("Ot", Person("Jaume", Person("Pere"), Person("Mar")),
                    Person("Maria", Person("John", null, Person("Fatima", Person("Ali")))))
                printFamilyTreeRecursivity(person)
            }
            0 -> break
            else -> println(toOutputError("Inexistent choice."))
        }
        println(userOptions())
        userSelection = scanner.nextInt()
    }
}

fun toOutputError(s: String): String {
    return "ERROR, $s"
}

fun userOptions(): String {
    return "Select the excercise:\n" +
            "1- Multiply recursive\n" +
            "2- Power recursive\n" +
            "3- Dot Line recursive\n" +
            "4- Factorial recursive\n" +
            "5- Search max value recursive\n" +
            "6- Figures\n" +
            "7- Family tree\n" +
            "0- Exit "
}

fun multRecursive(a: Int, b: Int): Int {
    return if (b == 0) 0
    else a + multRecursive(a,(b-1))
}

fun powRecursive(a: Int, b: Int): Int {
    return if (b == 0) 1
    else a * powRecursive(a,(b-1))
}


fun factRecursive(number: Int): Long {
    return  if (number == 0) 1
    else number * factRecursive(number-1)
}

fun dotRecursive(times: Int): String {
    val dot = "."
    return if (times == 0) ""
    else dot + dotRecursive(times - 1)
}

fun searchMaxValueRecursive(userList: List<Int>, prevMax: Int = userList[0], index: Int = 0): Int {
    var max = prevMax
    val number = userList[index]
    if (number >= max) max = number
    return if (index == userList.lastIndex) max
    else searchMaxValueRecursive(userList, max, (index+1))
}

fun printFamilyTreeRecursivity(person: Person?, tabs: Int = 0) {
    repeat(tabs){print(" ")}
    if (person == null) println("- ?")
    else {
        println(person.name)
        printFamilyTreeRecursivity(person.father,tabs+1)
        printFamilyTreeRecursivity(person.mother,tabs+1)
    }
}
fun getFiguresRecursive(number: Int, figures: Int = 0): Int {
    return if (number < 1) figures
    else getFiguresRecursive(number/10,figures+1)
}
