package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.instrumentsimulator

/**
Volem fer un petit programa d'emulació d'instruments.
Tenim els següents instruments.

Tambor
Triangle
Cada tambor té un to que pot ser A, O, U.
Els tambors amb A fan TAAAM, els tambors amb O fan TOOOM i els tambors amb U fan TUUUM.

Els tirangles tenen una resonancia que va del 1 al 5.
Els triangles amb resonancia1 fan TINC, els de dos TIINC, … i els de 5 TIIIIINC.
 */

fun main(args: Array<String>) {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(2) // plays 2 times the sound
    }
}


