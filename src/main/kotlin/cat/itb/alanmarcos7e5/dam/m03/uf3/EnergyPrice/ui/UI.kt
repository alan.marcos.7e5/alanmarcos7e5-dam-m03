package cat.itb.alanmarcos7e5.dam.m03.uf3.EnergyPrice.ui


import cat.itb.alanmarcos7e5.dam.m03.uf3.EnergyPrice.loadFromApi
import cat.itb.alanmarcos7e5.dam.m03.uf3.EnergyPrice.logic.EnergyDayPrices
import cat.itb.alanmarcos7e5.dam.m03.uf3.EnergyPrice.logic.HourRange
import java.util.*

/**
 * User interface with diferent options
 */
suspend fun main() {
    val scanner = Scanner(System.`in`)
    val dayPriceMap = loadFromApi()
    Ui(scanner, dayPriceMap).start()
}

data class Ui(val scanner: Scanner, val priceMap: Map<String,HourRange>) {

    val energyDayMap = EnergyDayPrices(priceMap)
    val rangesUi = RangesUi(scanner,energyDayMap)

    fun start(){
        println(userOptions())
        var option = scanner.nextInt()
        scanner.nextLine()
        while (option != 0) {
            when (option){
                1-> rangesUi.cheapest()
                2-> rangesUi.expensive()
                3-> rangesUi.byHour()
                4-> rangesUi.complete()
                5-> rangesUi.toFile()
                6-> rangesUi.deleteFile()
            }
            println(userOptions())
            option = scanner.nextInt()
            scanner.nextLine()
        }
    }

    fun userOptions() = "Select an option:\n" +
                        "1: Cheapest range\n" +
                        "2: More expensive range\n" +
                        "3: Search a range\n" +
                        "4: View ranges\n" +
                        "5: Save in file\n" +
                        "6: Delete file\n" +
                        "0: Exit\n"
}
