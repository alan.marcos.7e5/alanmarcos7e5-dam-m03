package cat.itb.alanmarcos7e5.dam.m03.uf1.aliens

import java.util.*
import kotlin.io.path.Path

fun main() {

    val scanner = Scanner(System.`in`)
val alien1= """      .--.   |V|
     /    \ _| /
     q .. p \ /
      \--/  //
     __||__//
    /.    _/
   // \  /
  //   ||
  \\  /  \
   )\|    |
  / || || |
  |/\| || |
     | || |
     \ || /
   __/ || \__
  \____/\____/"""
    val alien2 = """     _____
 ___/     \___
`-._)     (_,-`
    \O _ O/
     \ - /
      `-'
       ||
      _||_
     |-..-|
     |/. \|
     |\__/|
   ._|//\\|_,
   `-((  ))-'
    __\\//__
    >_ /\ _<,
      ''"""
    val alien3 = """   _________
  /___   ___\
 //@@@\ /@@@\\
 \\@@@/ \@@@//
  \___ " ___/
     | - |
      \_/"""
    val alien4 = """o
 \_/\o
( Oo)                    \|/
(_=-)  .===O-  ~~Z~A~P~~ -O-
/   \_/U'                /|\
||  |_/
\\  |
{K ||
 | PP
 | ||
 (__\\"""
    val alien5 = """    o   o
     )-(
    (O O)
     \=/
    .-"-.
   //\ /\\
 _// / \ \\_
=./ {,-.} \.=
    || ||
    || ||
  __|| ||__ 
 `---" "---'"""
    val alien6 ="""   .-""${'"'}${'"'}-.        .-""${'"'}${'"'}-.
  /        \      /        \
 /_        _\    /_        _\
// \      / \\  // \      / \\
|\__\    /__/|  |\__\    /__/|
 \    ||    /    \    ||    /
  \        /      \        /
   \  __  /        \  __  /
    '.__.'          '.__.'
     |  |            |  |
     |  |            |  |"""


    var input = scanner.nextInt()
    val list = mutableListOf<Int>()
    while (input != -1){
        list.add(input)
        input = scanner.nextInt()
    }
    for (e in list){
        when (e) {
            1 -> println(alien1)
            2 -> println(alien2)
            3 -> println(alien3)
            4 -> println(alien4)
            5 -> println(alien5)
            6 -> println(alien6)
        }
    }

}

