package cat.itb.alanmarcos7e5.dam.m03.uf1.introfunciones

import java.util.*

//Volem fer un programa per imprimir targetes de visites.
//Demana a l'usuari el nom i el 1r cognom i el número despatx on treballa. Guarda't la informació en una classe nova.
//Imprimeix el següent text:
//Empleada: Mar Puig - Despatx: 24

//creo la clase
class Empleado(var name : String, var workPlace : Int)

/*
con los datos ingresados crea una instancia de Empleado
 */
fun crearEmpleado(name : String, trabajo : Int) : String{
    //guardar en una clase
    val myEmpleado = Empleado(name, trabajo)
    //guardar los datos de empleado en una variable string para luego mostrar
    val datosEmpleado = "Empleada: ${myEmpleado.name} - Despatx: ${myEmpleado.workPlace}"
    return datosEmpleado
}

fun main() {
    //leer nombre y apellido
    val scanner = Scanner(System.`in`)
    val userNameCognom = scanner.nextLine()
    //leer nro de trabajo
    val userWork = scanner.nextInt()
    //imprimo empleado
    println(crearEmpleado(userNameCognom, userWork))
}