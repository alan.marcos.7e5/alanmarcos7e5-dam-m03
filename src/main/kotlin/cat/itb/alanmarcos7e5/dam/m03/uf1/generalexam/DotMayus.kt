package cat.itb.alanmarcos7e5.dam.m03.uf1.generalexam

import java.util.*

/**
 * Treballem en una empresa que és dedica al software d'anàlisis de text.
 * Ens han encomanat la tasca de fer un petit programa que donat un text ens indiqui
 * si després d'un punt i final (. i salt de línia) hi trobem una paraula amb majuscules.
 * No validarem els punts i continuació.
L'usuari introduirà un text.
Imprimeix la línia on es produeix l'error (la primera líniea és la 1)
Input
10
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
praesent est ex, condimentum sit amet tincidunt vitae,
feugiat et elit.
Nullam semper sem ut volutpat fermentum. Duis euismod,
elit non commodo condimentum, quam lorem suscipit eros,
et scelerisque augue lacus at nisl.
etiam molestie diam in gravida rhoncus.
Donec vitae justo eu nisl viverra mattis in et nibh.
Etiam massa tortor, fermentum nec rhoncus in, pellentesque
sit amet elit.
Output
2
7
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val listSize = scanner.nextInt()
    scanner.nextLine()
    val userText = List(listSize){scanner.nextLine()}
    var previousDot = false
    for ((i,line) in userText.withIndex()) {
        var userLine = mutableListOf<Char>()
        for (char in line) {
            userLine += char
        }
        if (i != 0 && previousDot && !userLine[0].isUpperCase()) println(i+1)
        if (userLine[userLine.lastIndex] == '.') previousDot = true
        else previousDot = false
    }

}