package cat.itb.alanmarcos7e5.dam.m03.uf1.introfunciones

//Volem fer un petit programa en el que li introduïm un text i el digui més fort.
//Per fer-ho passarem totes les lletres a majúscules.

fun toMayus (text:String) : String{
    val textoModificado = text.uppercase()
    return textoModificado
}

fun main() {
    val textMin = "Hola, estoy usando Kotlin!"
    val modifiedText = toMayus(textMin)
    println(modifiedText)

}