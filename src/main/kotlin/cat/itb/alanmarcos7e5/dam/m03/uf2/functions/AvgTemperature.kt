package cat.itb.alanmarcos7e5.dam.m03.uf2.functions

import java.util.*

/**
 * Fes un programa que donades diferents temperatures (sense decimals), retorni la temperatura mitjana (amb decimals)
 * Input
 * 5 3 4 7 8 -1
 * Output
 * Ha fet 5.4 graus de mitjana
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val listOfTemperatures = readIntList(scanner)
    val average = avg(listOfTemperatures)
    println("Ha fet $average graus de mitjana")
}