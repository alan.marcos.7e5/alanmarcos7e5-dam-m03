package cat.itb.alanmarcos7e5.dam.m03.uf1.seleccio.exam

import java.util.*

/**
 *  Farem un programa per un radar de transit que assigna multes segons la velocitat dels cotxes.
 *  Si el radar detecta un cotxe a més de 120km/h el cotxe serà multat.
 *  Si la velocitat supera la màxima en més de 20 km/h serà una multa greu. Sinó serà una multa lleu.
 *
 *  Demana una velocitat a l'usuari.
 *   Imprimeix per pantalla el tipus de multa (Correcte, Multa lleu o Multa greu).
 *
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val carVelocity = scanner.nextInt()
    //logica de programa
    val velocityLimit = 120
    val estado = if (carVelocity > velocityLimit){
        if (carVelocity - velocityLimit > 20)
            "multa greu"
        else
            "multa leu"
    }else
        "Correcte"

    println(estado)
}