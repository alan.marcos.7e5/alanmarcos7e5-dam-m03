package cat.itb.alanmarcos7e5.dam.m03.uf2.classfun

import cat.itb.alanmarcos7e5.dam.m03.uf1.data.Robot
import java.util.*

/**
 * Fes un programa que permeti moure un robot en un plà 2D.
El robot ha de guardar la seua posició (X, Y) i la velocitat actual.
Per defecte, la posició serà (0, 0) i la velocitat és 1. La velocitat
indica la distància que recorre el robot en cada acció.
El robot té les següents accions:
DALT: El robot és mou cap a dalt (tenint en compte la velocitat).
BAIX: El robot és mou cap a baix (tenint en compte la velocitat).
DRETA: El robot és mou cap a la dreta (tenint en compte la velocitat).
ESQUERRA: El robot és mou cap a la esquerra (tenint en compte la velocitat).
ACCELERAR: El robot augmenta en 0.5 la velocitat. La velocitat màxima és 10.
DISMINUIR: El robot augmenta en 0.5 la velocitat. La velocitat mínima és 0.
POSICIO: El robot imprimeix la seua posició.
VELOCITAT: El robot imprimeix la seua velocitat.
El programa acaba amb l'acció END.

Les línies que comencen per > són el que ha escrit l'usuari
> POSICIO
La posició del robot és (0.0, 0.0)
> DALT
> DALT
> ESQUERRA
> POSICIO
La posició del robot és (-1.0, 2.0)
> ACCELERAR
> VELOCITAT
La velocitat del robot és 1.5
> DALT
> POSICIO
La posició del robot és (-1.0, 3.5)
> END
 */
data class BasicRobot(val position: MutableList<Double> = mutableListOf(0.0,0.0),
                      var velocity: Double = 1.0) {
    fun baix() {
        position[1] -= this.velocity
    }

    fun dalt() {
        position[1] += this.velocity
    }

    fun left() {
        position[0] -= this.velocity
    }

    fun rigth() {
        position[0] += this.velocity
    }

    fun accelerate() {
        this.velocity += 0.5
    }

    fun decrease() {
        this.velocity -= 0.5
    }
    fun toOutput(order: String): String {
        return if (order=="POSICIO"){
            "La posición es (${position[0]},${position[1]})\n"
        }else{
            "La velocidad es $velocity\n"
        }
    }
}

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    var userInput = scanner.nextLine()
    val robot = BasicRobot()
    while (userInput != "END") {
        print(action(robot,userInput))
        userInput = scanner.nextLine()
    }
}

fun action(robot: BasicRobot, order: String): String {
    var output = ""
    when(order){
        "BAIX"-> robot.baix()
        "DALT"-> robot.dalt()
        "ESQUERRA"-> robot.left()
        "DRETA"-> robot.rigth()
        "ACCELERAR" ->  robot.accelerate()
        "DISMINUIR" -> robot.decrease()
        "POSICIO","VELOCITAT" -> output = robot.toOutput(order)
        else -> output = "error\n"
    }
    return "$output"
}
