package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 *L'usuari entrarà un conjunt d'enters per pantalla. Quan introdueixi el -1, és que ja ha acabat.
 * Afegeix el primer a l'inici de la llista, el segon al final, el tercer a l'inici, etc.
 * Printa el resultat:
 * Input
 * 1 2 3 4 5 6 -1
 * Output
 * [5,3,1,2,4,6]
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val intList = mutableListOf<Int>()
    do {
        val number = scanner.nextInt()
        if (number == -1) break
        intList.add(number)
    } while (number != -1)
    val strangeList = MutableList(intList.size){it}
    for ((i,element) in intList.withIndex()) {
        if (!isPair(i)) {
            strangeList[intList.size - 1] = element
        }else strangeList[0] = element
    }
    println(strangeList)
}