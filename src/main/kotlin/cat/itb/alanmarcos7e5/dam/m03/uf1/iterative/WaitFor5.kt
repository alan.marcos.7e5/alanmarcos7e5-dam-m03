package cat.itb.alanmarcos7e5.dam.m03.uf1.iterative

import java.util.*

/**
 *  Farem un programa que preguna a l'usuari un enter fins a que entri el numero 5.
 *  L'usuari introdueix enters.
 *  Quan introdueixi el 5 imprimeix 5 trobat!
 */

fun main() {
    val scanner = Scanner(System.`in`)
    var inputUser = scanner.nextInt()
    while (inputUser != 5)
        inputUser = scanner.nextInt()
    println("5 trobat!")

}
