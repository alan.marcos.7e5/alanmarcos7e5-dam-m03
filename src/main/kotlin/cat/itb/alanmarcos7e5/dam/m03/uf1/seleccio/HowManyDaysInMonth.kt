package cat.itb.alanmarcos7e5.dam.m03.uf1.seleccio

import java.util.*

//Demanar un enter a l'usuari que indica el numero de més
//Retorna el número de dies del mes.

fun main() {
    val scanner = Scanner(System.`in`)
    val inputUser = scanner.nextInt()
    val numberOfDays = when (inputUser) {
        1,3,5,7,8,10,12 -> "tiene 31 días."
        4,6,9,11 -> "tiene 30 días."
        2 -> "tiene 28 días."
        else -> "Ingrese mes correcto"
    }
    if (inputUser in 1..12)
        println("El mes $inputUser $numberOfDays")
    else
        println(numberOfDays)
}