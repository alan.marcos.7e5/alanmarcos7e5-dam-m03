package cat.itb.alanmarcos7e5.dam.m03.uf3.generalexam

import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.nio.file.Path
import java.util.*
import kotlin.io.path.*


/**
 * Volem fer un petit programa per calcular les calories que ingerim al llarg del dia.

Cada cop que obrim el programa l'usuari introduirà el nom del menjar, els grams que n'ha ingerit i les kcal/gram que té l'aliment.

A continuació es mostrarà el nom de cada aliment, els grams i les calories totals.

Guarda la informació que necessitis en format json en un fitxer a un

~/food.json
Input1
Macarrons 80 0.131
Output1
Macarrons 80g, 10.48 kcal
Input2
Poma 100 0.063
Output2
Macarrons 80g, 10.48 kcal
Poma 100g, 63 kcal
 */
@Serializable
data class Aliment(val name: String, val weight: Double, val kcalPerGr: Double){
    val calories get() = kcalPerGr * weight
}

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val homePath = Path(System.getProperty("user.home"))
    val filePath = homePath.resolve("food.json")

    val listAliments = if (filePath.exists()) loadFromFile(filePath) else mutableListOf<Aliment>()
    val aliment = createAliment(scanner)
    listAliments += aliment

    saveToFile(filePath,listAliments)
    toOutput(listAliments)
}

fun loadFromFile(filePath: Path): MutableList<Aliment>{
    val textOfFile = filePath.readText()
    return Json.decodeFromString<MutableList<Aliment>>(textOfFile)
}

fun saveToFile(filePath: Path, listAliments: MutableList<Aliment>) {
    val serializer = Json.encodeToString(listAliments)
    filePath.writeText(serializer)
}

fun createAliment(scanner: Scanner): Aliment {
    println("name:")
    val name = scanner.nextLine()
    println("grs:")
    val weight = scanner.nextDouble()
    println("Kcal/gr:")
    val kcalPerGr = scanner.nextDouble()
    return Aliment(name, weight, kcalPerGr)
}
 fun toOutput(listAliments: MutableList<Aliment>){
     for (aliment in listAliments) {
         println("${aliment.name}, ${aliment.weight}, ${aliment.calories}")
     }
 }