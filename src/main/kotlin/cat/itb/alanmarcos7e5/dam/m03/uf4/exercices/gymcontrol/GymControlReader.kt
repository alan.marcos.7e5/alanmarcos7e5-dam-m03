package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.gymcontrol

interface GymControlReader {
    fun nextId(): String
}