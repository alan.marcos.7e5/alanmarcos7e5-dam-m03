package cat.itb.alanmarcos7e5.dam.m03.uf1.generalexam

import cat.itb.alanmarcos7e5.dam.m03.uf1.lists.readIntList
import java.util.*

/**
 * En una carrera de marató hi ha 50 corredors amb dorsals numerats de l'0 al 99.
 * Se sap que els dorsals acabats en 0 pertànyen a l'equip 0, els acabats en 1 a l'equip 1…
 * Sabent que cada corredor té la seva marca personal de temps per a la marató,
 * i que el programa va llegint les marques de cada un dels corredors (dorsal 0, dorsal 1, dorsal 2 …).
 * Es demana que imprimiu el temps total de cada equip.
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val listOfTimes = readIntList(scanner) //input of times for each athlete
    val listScoresTeams = MutableList(10){0} //there are 10 teams
    var team = 0
    for ((i,time) in listOfTimes.withIndex()) {
        if (i<=9) team = i
        else {
            var strI = i.toString()
            team = strI[1].toString().toInt()
        }
        listScoresTeams[team] += time
    }
    println(listScoresTeams)
}