package cat.itb.alanmarcos7e5.dam.m03.uf1.seleccio.exam

import java.util.*

/**
 * El Three Card Pocker és un joc de cartes on es poden fer combinacions de 3 cartes.
 * En la nostra versió del joc, jugarem només amb els números, no amb els pals. Així, les figures possibles seran:
 *   Trio 3 cartes del mateix número
 *   Escala 3 cartes amb números consecutius
 *   Parella 2 cartes del mateix número
 *   Número alt cap de les anteriors
 */

fun calculateCombination(firstCard : Int, secondCard : Int, thirdCard : Int):String{

    //Ordenando las cartas
    var biggestCard = 0
    var middleCard = 0
    var lowestCard = 0
    if (firstCard > secondCard && firstCard > thirdCard){
        biggestCard = firstCard
        if (secondCard > thirdCard){
            middleCard = secondCard
            lowestCard = thirdCard
        }else{
            middleCard = thirdCard
            lowestCard = secondCard
        }
    }
    else if (secondCard > firstCard && secondCard > thirdCard){
        biggestCard = secondCard
        if (firstCard > thirdCard){
            middleCard = firstCard
            lowestCard = thirdCard
        }else {
            middleCard = thirdCard
            lowestCard = firstCard
        }
    }
    else if (thirdCard > firstCard && thirdCard > secondCard){
        biggestCard = thirdCard
        if (firstCard > secondCard){
            middleCard = firstCard
            lowestCard = secondCard
        }else {
            middleCard = secondCard
            lowestCard = firstCard
        }
    }
    //trabajando el return con las cartas ordenadas
    return if (biggestCard == middleCard && middleCard == lowestCard) "Trio"
    else if (biggestCard == (middleCard + 1) && middleCard == (lowestCard + 1)) "Escala"
    else if (biggestCard == middleCard || middleCard == lowestCard || biggestCard == lowestCard) "Parella"
    else "Número alt"

}


fun main() {
    val scanner = Scanner(System.`in`)
    val userFirstCard= scanner.next().toInt()
    val userSecondCard= scanner.next().toInt()
    val userThirdCard= scanner.next().toInt()
    val userGameHand = calculateCombination(userFirstCard,userSecondCard,userThirdCard)
    println(userGameHand)
}