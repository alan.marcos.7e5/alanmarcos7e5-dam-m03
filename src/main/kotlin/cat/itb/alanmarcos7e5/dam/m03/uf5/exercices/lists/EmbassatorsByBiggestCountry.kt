package cat.itb.alanmarcos7e5.dam.m03.uf5.exercices.lists

import java.util.Scanner

/**
 * Ens volem guardar la informació de diferents embaixadors que hi ha a la nostre ciutat.
 * Cada enbaixador ho és d'un país determinat.
 * Llegeix els països i els embaixadors.
 * Imprimeix per pantalla els diferents embaixadors ordenats per la mida del païs que representen (el més gran primer).
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val listOfCountries = readCountries()
    val size = scanner.nextLine().toInt()
    val listOfEmbassators = List(size){
        val name = scanner.nextLine()
        val surname = scanner.nextLine()
        val countryName = scanner.nextLine()
        Embassator(name,surname,listOfCountries.find { it.name == countryName })
    }
    println(listOfEmbassators.sortedByDescending { it.country?.area })
}