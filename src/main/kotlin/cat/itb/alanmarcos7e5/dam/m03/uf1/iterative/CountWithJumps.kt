package cat.itb.alanmarcos7e5.dam.m03.uf1.iterative

import java.util.*


/**
 *  L'usuari introdueix dos valors enters, el final i el salt
    Escriu tots els numeros des de l'1 fins al final, amb una distància de salt
 */
fun main() {
    //lee data
    val scanner = Scanner(System.`in`)
    val finalCount = scanner.nextInt()
    val stepCount = scanner.nextInt()
    //logic
    for (i in 1..finalCount step stepCount) print("$i ")
}