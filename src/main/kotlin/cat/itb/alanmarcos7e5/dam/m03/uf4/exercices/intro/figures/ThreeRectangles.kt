package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.intro.figures

/**
 * Volem fer un programa que dibuixi rectangles de colors a la consola.
 * Volem que dibuixi els següents 3 rectanlges.
 * color: RED, llargada: 4, amplada: 5
 * color: YELLOW, llargada: 2, amplada: 2
 * color: GREEN, llargada: 3, amplada: 5
 * Crea la classe RectangleFigure
 */
class RectangleFigure(colorConstructor: String, val height: Int, val width: Int){
    val color = colorConstructor.uppercase()
    override fun toString(): String {
        return "color: $color, llargada: $height, amplada: $width"
    }
}

fun main() {
    val rectangleFigure1 = RectangleFigure(RED,4,5)
    println(rectangleFigure1)
    val rectangleFigure2 = RectangleFigure("YeLLow",2,2)
    println(rectangleFigure2)
    val rectangleFigure3 = RectangleFigure("Green",3,5)
    println(rectangleFigure3)
    val rectangleFigure4 = RectangleFigure("Green",3,5)
    println("${GREEN}$rectangleFigure4${RESET}");
}