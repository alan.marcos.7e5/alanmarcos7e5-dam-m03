package cat.itb.alanmarcos7e5.dam.m03.uf1.lists.exam

import java.util.*

/**
 * Crea una llista de 10 enters inicialitzats amb el valor 0.
 * L'usuari introduirà parelles de valors.
 * La primera indicarà la posició de la llista a modificar i la segona el valor a posar-hi.
 * Quan introdueixi la posició -1 és que s'ha d'acabar el programa i printar la llista en l'estat que ha quedat.
 * input:
 * 1 3
 * 4 8
 * 0 1
 * 1 5
 * -1
 * output:
 * [1, 5, 0, 0, 8, 0, 0, 0, 0, 0]
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val listInt = MutableList(10){0}
    var inputUserList = mutableListOf<Int>(0,0)
    inputUserList[0] = scanner.nextInt()

    while (inputUserList[0] != -1) {
        inputUserList[1] = scanner.nextInt()
        listInt[inputUserList[0]] = inputUserList[1]
        inputUserList[0] = scanner.nextInt()
    }

    println(listInt)
}