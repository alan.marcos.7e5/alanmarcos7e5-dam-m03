package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * Ingresar 10 valores.
 * multiplicarlos por la posicion que ocupan
 * sumar esas multiplicaciones
 * el residuo de la division modular de este resultado y 11 debe ser igual al ultimo digito de la lista
 * Si dicho residuo fuese 10, se pondrá X como dígito de control
 */


fun main() {
    val scanner = Scanner(System.`in`)
    val myIsbn = mutableListOf<Int>()// inicializo lista vacía
    val myIsbnInput = scanner.next()

    //bucle para iterar sobre la secuencia de char ingresada por el usuario
    for (element in myIsbnInput)
        if (element.isDigit())
            myIsbn.add(element.toString().toInt())
    println(myIsbn)

    //bucle para trabajar sobre la lista
    var totalSum = 0
    for ((i, element) in myIsbn.withIndex()){
        if (i == 9) break
        totalSum += (element*(i+1))
    }
    val modTotalSum = totalSum % 11
    val controlDigit = if (modTotalSum != 10)
        modTotalSum
    else
        "X"
    if (controlDigit == myIsbn[myIsbn.lastIndex])
        println("El ISBN es correcto")
    else
        println("El ISBN es incorrecto!!")

}

