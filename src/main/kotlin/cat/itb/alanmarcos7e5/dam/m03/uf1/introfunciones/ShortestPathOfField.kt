package cat.itb.alanmarcos7e5.dam.m03.uf1.introfunciones

import java.lang.Math.pow
import kotlin.math.sqrt

//Hem de travessar un camp d'un extrem (superior-esquerre) a un altre (inferior-dret)
//i volem saber quanta distància haurem de recorre. Només tenim la informació de l'amplada i l'alçada.
//Demana a l'usuari l'amplada i l'alçada d'un camp i imprimeix-ne la diagonal.

fun calcularHipotenusa(base:Double, altura:Double) = sqrt(pow(base, 2.0) + pow(altura.toDouble(), 2.0))



fun main() {
    val base = 3.2
    val height = 9
    val hipotenusa = calcularHipotenusa(base, height.toDouble())
    println(hipotenusa)
}