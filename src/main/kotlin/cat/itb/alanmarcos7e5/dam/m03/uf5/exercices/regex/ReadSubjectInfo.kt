package cat.itb.alanmarcos7e5.dam.m03.uf5.exercices.regex

import java.util.Scanner

/**
 * Volem fer un programa que donat el nom de la UF en un format determinat
 * ens l'imprimeixi en un format amigable per l'usuari.
Input
2
DAM-M03UF2
ASIX-M01UF09
Output
Estàs cursant la unitat formativa 2, del mòdul 3 de DAM.
Estàs cursant la unitat formativa 9, del mòdul 1 de ASIX.
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val listInputUser = List(size = scanner.nextLine().toInt()){scanner.nextLine()}
    println(listInputUser)

    val listSplited = listInputUser[0].split("-")
    println(listSplited)
    val regex = Regex("^DAM")
    println(regex)
    listInputUser.forEach {
        println( it.matches(regex))
    }
    //TODO continuar
}