package cat.itb.alanmarcos7e5.dam.m03.uf1.mix

import java.util.*

/**
 * Primer l'usuari introduirà els cromos repetits. Quan estigui escriurà -1.
 * Després l'usuari introduirà els cromos que li falten ordenats de petit a gran. Quan estigui escriurà -1.
 * Imprimeix els cromos a canviar
 * input
 * 3 5 2 8 3 9 6 1 5 -1
 * 1 2 6 7 11 15 -1
 * output:
 * 1 2 6
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val repeatsList = mutableListOf<Int>()
    val missingList = mutableListOf<Int>()
    var userInput = scanner.nextInt()
    while (userInput != -1) {
        repeatsList += userInput
        userInput = scanner.nextInt()
    }
    var userInputMissing = scanner.nextInt()
    while (userInputMissing != -1) {
        missingList += userInputMissing
        userInputMissing = scanner.nextInt()
    }
    println(missingList)
    println(repeatsList)
    println(binarySearch(missingList, repeatsList))
}

fun binarySearch(listToSearchIn: List<Int>, listToFind: List<Int>): List<Int>{
    val coincidences = mutableListOf<Int>()
    for (e in listToFind) {
        var maxIndex = listToSearchIn.size - 1
        var minIndex = 0
        while (maxIndex >= minIndex) {
            var guess = (maxIndex + minIndex) / 2
            if (e < listToSearchIn[guess]){
                maxIndex = guess - 1
            }else if (e > listToSearchIn[guess]){
                minIndex = guess + 1
            }else if (e == listToSearchIn[guess]) {
                coincidences += e
                break
            }
        }

    }
    return coincidences.sorted()
}