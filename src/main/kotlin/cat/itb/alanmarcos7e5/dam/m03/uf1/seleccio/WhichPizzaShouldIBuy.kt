package cat.itb.alanmarcos7e5.dam.m03.uf1.seleccio

import cat.itb.alanmarcos7e5.dam.m03.uf1.introfunciones.calculateCircleArea
import java.util.*

//olem comparar quina pizza és més gran, entre una rectangular i una rodona
//L'usuai entra el diametre d'una pizza rodona
//L'usuari entra els dos costats de la pizza rectangular
//Imprimeix "Compra la rodona" si la pizza rodona és més gran, o "Compra la rectangular" en qualsevol altre cas.

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    //leo usuario
    val inputDiameter = scanner.nextDouble()
    val inputSide1 = scanner.nextDouble()
    val inputSide2 = scanner.nextDouble()
    //inicializo variable de salida
    var outputAnswer : String = "Son iguales"
    //calculos de superficie de ambas pizzas
    val radio = inputDiameter / 2
    val circleArea = calculateCircleArea(radio)
    val rectArea = inputSide1 * inputSide2
    //comparo ambas areas
    if (circleArea > rectArea)
        outputAnswer = "Compra la rodona"
    else if (circleArea < rectArea)
        outputAnswer = "Compra la rectangular"
    else
        println(outputAnswer)
    println(outputAnswer)
}