package cat.itb.alanmarcos7e5.dam.m03.uf1.seleccio

import java.util.*

//Introdueix el número de persones i el número de galetes.
//Si a tothom li toquen el mateix número de galetes imprimeix "Let's Eat!",
//sinó imprimeix "Let's Fight"

fun main() {
    //read input data
    val scanner = Scanner(System.`in`)
    val inputPersons = scanner.nextInt()
    val inputCookies = scanner.nextInt()
    //compare
    val result = if (inputCookies % inputPersons == 0)
        "Let's Eat!"
    else
        "Let's Fight"
    println(result)
}