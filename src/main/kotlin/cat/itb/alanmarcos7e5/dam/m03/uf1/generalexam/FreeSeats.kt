package cat.itb.alanmarcos7e5.dam.m03.uf1.generalexam

import cat.itb.alanmarcos7e5.dam.m03.uf1.lists.readIntList
import java.util.*

/**
 * Com a part d'una aplicació de venta d'entrades d'un cine,
 * volem saber si en una fila determinada hi ha una certa quantitat de seients lliures consecutius.
 * Primer ens introduiran un enter que indica quantes entrades juntes vull comprar
 * Després la quantitat de seients per fila
 * Finalment els valors de la fila on 0 indica seient lliure i 1 seient ple
 * Indicarem: Hi ha x seients consecutius, o bé, No hi ha x seients consecutius (on x és el nombre de seients junts cercats)
 * Input
 * 3
 * 25
 * 0 1 0 1 1 1 1 0 1 1 1 0 1 0 1 0 0 1 0 0 0 1 1 1 0
 * Output
 * Hi ha 3 seients consecutius
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val userTickets = scanner.nextInt()
    val cineRow = readIntList(scanner)
    var consecutivos = 0
    var freeSeats = 0
    for (i in cineRow.indices) {
        if (cineRow[i] == 0) {
            consecutivos++
        }
        else {
            if (consecutivos >= userTickets) freeSeats = userTickets
            consecutivos = 0
        }
    }
    println(consecutivos)
    println(freeSeats)
    if (freeSeats == userTickets) println("Hi ha $freeSeats seients consecutius")
    else println("No hi ha $userTickets seients consecutius")

}