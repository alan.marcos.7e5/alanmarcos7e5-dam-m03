package cat.itb.alanmarcos7e5.dam.m03.uf2.functions

import java.util.*

/**
 * Fes un programa que donat diferents edats dels estudiants, retorni l'edat de l'estudiant amb més edat.
 * Input
 * 5 3 4 7 8 -1
 * Output
 * L'alumne més gran té 8 anys
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val listOfStudents = readIntList(scanner)
    val oldest = max(listOfStudents)
    println("L'alumne més gran té $oldest anys")
}