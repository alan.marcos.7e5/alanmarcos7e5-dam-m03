package cat.itb.alanmarcos7e5.dam.m03.uf1.data

import java.util.*

//Volem fer un joc 2D en la que diferents robots (tancs) lluiten al'arena.
//Tots els robots tenen les mateixes característiques, el que varia és la IA que els controla.
//Has de crear la classe robot per a què cada usuari pugui definir el seu robot.
//Per ara sabem que:
//- El robot té un cos: permet al robot desplaçar-se y girar. Per tant guardarem la seva posició. Es distingirà pel seu color.
//- Quan el tanc xoca perd vida
//- Quan el tanc dispara i encerta guanya vida proporcionalment

class Position(var x: Int, var y: Int)

class Robot(val color : String, var position : Position){
    //propiedades
    var vida = 10
    //TODO revisar return funciones
    fun disparar(){
        this.vida += 1
        println("Has disparado!")
    }
    fun chocar(){
        this.vida -= 2
        println("Chocaste!")
    }

}

fun main() {
    //leo datos ingresados para el robot
    println("Ingrese datos del robot: -color\n")
    val scanner = Scanner(System.`in`)
    val inputColor = scanner.nextLine()
    println("Ingrese datos de la posición: -x,y\n")
    val inputPositionX = scanner.nextInt()
    val inputPositionY = scanner.nextInt()
    //creo instancia de clase posicion 1
    var myPosition1 = Position(inputPositionX, inputPositionY)
    //creo instancia de clase robot 1
    var myRobot1 = Robot(inputColor, myPosition1)
    //imprimo Robot creado
    println("${myRobot1.color} ** (${myRobot1.position.x},${myRobot1.position.y}) ** ${myRobot1.vida}")
    //choca
    myRobot1.chocar()
    println("${myRobot1.color} ** (${myRobot1.position.x},${myRobot1.position.y}) ** ${myRobot1.vida}")
}