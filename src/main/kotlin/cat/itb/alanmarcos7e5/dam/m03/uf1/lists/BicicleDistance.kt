package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * Donada una velocitat d'una bicicleta en metres per segon,
 * indica els metres que haurà recorregut quan hagi passat 1,2,3,4,5,6,7,8,9 i 10 segons.
 * Input
 *  2.5
 * Output
 *  2.5 5.0 7.5 10.0 12.5 15.0 17.5 20.0 22.5 25.0
 */

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val bikeVelocity = scanner.nextDouble()
    //val secondsList = List(10){it+1}
    val bikeDistance = mutableListOf<Double>()
    for (second in 1..10) {
        bikeDistance.add(bikeVelocity * second)
    }
    println(bikeDistance)
}