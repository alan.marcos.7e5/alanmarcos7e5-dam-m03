package cat.itb.alanmarcos7e5.dam.m03.uf1.adventOfCode

import java.util.*
import kotlin.io.path.Path

/**
 * Day 2. Part 2
 */

fun main() {
    val scanner = Scanner(Path("data/aventofcode/inputDay2_2.txt"))
    val inputList = mutableListOf<String>()
    while (scanner.hasNext()) {
        val movement = scanner.nextLine()
        inputList.add(movement)
    }
    var depth = 0
    var horizontal = 0
    var aim = 0

    for (element in inputList) {
        if ("forward" in element){
            horizontal += extractNumber(element)
            depth += (aim * extractNumber(element))
        }
        else if ("down" in element) aim += extractNumber(element)
        else if ("up" in element) aim -= extractNumber(element)
    }
    println("depth = $depth      horizontal pos = $horizontal")
    val result = depth * horizontal
    println(result)
}

