package cat.itb.alanmarcos7e5.dam.m03.uf3

import java.time.LocalDateTime
import kotlin.io.path.*

/**
 * Fes un programa que fagi un backup del fitxer .profile.
 * Guarda el fitxer ~/.profile a la carpeta ~/backup/2021-02-16T09:59:04.343769/.profile
 * Podeu obtenir la data actual usant:
 * LocalDateTime.now().toString()
 */
fun main() {
    //home path
    val homePathString = System.getProperty("user.home")
    val homePath = Path(homePathString)
    //backup
    val backupPath = homePath.resolve("backup")
    backupPath.createDirectories()
    //date
    val actualDate = LocalDateTime.now().toString()
    val datePath = backupPath.resolve("$actualDate")
    datePath.createDirectories()
    //profile copy
    val profile = Path("${homePathString}/.profile")
    if (profile.exists()){
        val destination = datePath.resolve("profile") //quito "." para que no sea oculto
        profile.copyTo(destination)
    }

}