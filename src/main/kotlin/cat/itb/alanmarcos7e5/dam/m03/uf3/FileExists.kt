package cat.itb.alanmarcos7e5.dam.m03.uf3

import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.exists

/**
 * Demana a l'usuari un path. Retorna true si existeix, false sinó.
 * input: /home
 * output: true
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val path = Path(scanner.nextLine())
    println(path.exists())
}