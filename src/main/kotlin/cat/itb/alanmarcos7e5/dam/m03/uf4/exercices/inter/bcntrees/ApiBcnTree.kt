package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.inter.bcntrees

import cat.itb.alanmarcos7e5.dam.m03.uf3.BcnTree
import cat.itb.alanmarcos7e5.dam.m03.uf3.createJsonHttpClient
import io.ktor.client.request.*

class ApiBcnTree(): DataBcnTree {
    override suspend fun listTrees(): List<BcnTree> {
        val client = createJsonHttpClient(true)
        return client.get("https://opendata-ajuntament.barcelona.cat/resources/bcn/Arbrat/OD_Arbrat_Zona_BCN.json")
    }
}