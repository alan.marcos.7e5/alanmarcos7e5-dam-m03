package cat.itb.alanmarcos7e5.dam.m03.uf2.classfun

import java.util.*

val playerOne = "x"
val playerTwo = "o"

/**
 *
 */
data class Board(val listOfPieces: MutableList<MutableList<String>> =
    MutableList(3){ MutableList(3){"-"} }, var currentPlayer: Boolean = true) {

    fun identifyPlayer(): String {
        return if (currentPlayer) playerOne
        else playerTwo
    }

    fun isPositionEmpty(position: List<Int>): Boolean{
        val positionToChange = listOfPieces[position[0]][position[1]]
        return positionToChange == "-"
    }

    fun addPiece(position: List<Int>){
        listOfPieces[position[0]][position[1]] = identifyPlayer()
        this.currentPlayer = !this.currentPlayer
    }


    fun findWinner(): String? {
        /*
        el return hace una primer funciòn, si es nula (?:), ejecuta la otra, si también es nula
        ejecuta la tercera y retornará lo que retorne esa tercera.
        Si por ejemplo, la primera no es nula, entonces retornará lo que ésta retorne.
         */
        return findWinnerByRow() ?: findWinnerInColumn() ?: findWinnerInDiagonal()
    }
    fun findWinnerByRow(): String? {
        for (row in listOfPieces) {     //checking 3 in rows
            if (row[0] == row[1] && row[1] == row[2] && row[0] != "-") return row[0]
        }
        return null
    }
    fun findWinnerInColumn(): String? {
        for (i in 0..2) {       //checking 3 in columns
            if (listOfPieces[0][i] == listOfPieces[1][i] && listOfPieces[0][i] == listOfPieces[2][i]
                && listOfPieces[0][i] != "-"
            ) return listOfPieces[0][i]
        }
        return null
    }
    fun findWinnerInDiagonal(): String? {
        if (listOfPieces[0][0] == listOfPieces[1][1] && listOfPieces[0][0] == listOfPieces[2][2]
            && listOfPieces[0][0] != "-")
            return listOfPieces[0][0]
        else if (listOfPieces[0][2] == listOfPieces[1][1] && listOfPieces[0][2] == listOfPieces[2][0]
            && listOfPieces[0][2] != "-")
            return listOfPieces[0][2]
        return null
    }

    fun boardToString(): String {
        var output = ""
        for (row in listOfPieces) {
            var sum = 0
            for (e in row) {
                output += "$e "
                sum += 1
                if (sum == 3) output += "\n"
            }
        }
        return output
    }

    fun allFull(): Boolean {
        for (row in listOfPieces) {
            if ("-" in row) return false
        }
        return true
    }

    fun gameFinished(): Boolean {
        return allFull() || findWinner() != null
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    val board = Board()
    logic(board,scanner)
}

fun logic(board: Board, scanner: Scanner) {
    val position = MutableList(2){scanner.nextInt()}
    while (!board.gameFinished()) {
        if (board.isPositionEmpty(position)) { // verify that position not be busied
            board.addPiece(position)
            println(board.boardToString())
        } else println("Posición ocupada, coloca en otro lado la pieza")
        position[0] = scanner.nextInt() //x
        position[1] = scanner.nextInt() //y
    }
    //TODO (algo funciona mal y no se muestra el ganador)
    printWinnerOrBoardFull(board)
}

fun printWinnerOrBoardFull(board: Board): String? {
    val winner = board.findWinner()
    return if (winner != null) {
        "Ganador $winner"
    }else if (board.allFull()) {
        "El tablero está lleno"
    }else null
}




