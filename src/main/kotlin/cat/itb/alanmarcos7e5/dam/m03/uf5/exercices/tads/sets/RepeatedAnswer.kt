package cat.itb.alanmarcos7e5.dam.m03.uf5.exercices.tads.sets

import cat.itb.alanmarcos7e5.dam.m03.uf5.exercices.tads.maps.readPlates
import java.util.Scanner

/**
Volem fer un petit joc tipus Scatergories o Un, Dos tres, on els diferents jugadors
han de dir respostes a una pregunta, però no es poden fer repeticions.
Fes un programa que llegeixi la resposta per l'input i printi "MEEEC!" quan hi hagi una repetició.
El programa s'acaba quan l'usuari escriu END

Input
poma
pera
cirera
pera
cindria
meró
cirera
END

Output
MEEEC!
MEEEC!
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val listOfAnswers = readAnswers(scanner)
    println( listOfAnswers)
}

fun readAnswers(scanner: Scanner): Set<String> {
    val setOfAnswers = mutableSetOf<String>()
    var answer = scanner.nextLine()
    while (answer.uppercase() != "END") {
        if (answer in setOfAnswers) println("MEEEC!")
        setOfAnswers += answer
        answer = scanner.nextLine()
    }
    return setOfAnswers
}
