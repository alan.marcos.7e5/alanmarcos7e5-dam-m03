package cat.itb.alanmarcos7e5.dam.m03.uf1.introfunciones

import java.util.*

//Una web d'habitatges de lloguer ens ha proposat una ampliació.
//Volen mostrar l'àrea de les habitacions en lloguer.
//Fes un programa que ens ajudi a calcular les dimensions d'una habitació.
//Llegeix l'amplada i la llargada en metres (enters) i mostra'n l'àrea

fun areaHabitacion(ancho : Double, largo: Double): Double {
    val area = ancho * largo
    return area
}

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val inputAncho = scanner.nextDouble()
    val inputLargo = scanner.nextDouble()
    println(areaHabitacion(inputAncho, inputLargo))
}