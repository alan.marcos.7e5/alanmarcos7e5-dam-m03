package cat.itb.alanmarcos7e5.dam.m03.uf3

import java.nio.file.StandardOpenOption
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.writeText

/**
 * Volem fer un programa per guardar el codi secret d'un usuari en un fitxer (de forma no segura).
 * L'usuari introduirà un text per pantalla i l'haurem de guardar en el fitxer secret.txt de la carpeta actual.
 * Imprimeix guardat quan s'hagi escrit el fitxer.
 * Input
 * 328df72928s72
 * Output
 * guardat
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val secret = scanner.nextLine()

    val homePath = System.getProperty("user.home")
    val secretFile = Path(homePath+"/secret.txt")

    secretFile.writeText(secret)
    secretFile.writeText(secret, options =
    arrayOf(StandardOpenOption.APPEND))

    println("guardat")
}