package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.carpentryshop

class WoodTable(price: Double, width: Double, val high: Double) : WoodProduct(price,width) {
    override val totalPrice: Double
        get() = this.price * (this.width * this.high)
}