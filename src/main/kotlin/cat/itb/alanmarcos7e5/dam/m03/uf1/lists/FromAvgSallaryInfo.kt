package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * En una empresa ens diuen que volen coneixer el salari mitjà dels seus treballadors, i veure quin cobre per sota.
 * L'usuari introduirà una línia per cada treballador amb el seu sou, i el seu nom.
 * Un cop introduits els treballadors printa per pantalla el nom dels treballadors que cobren per sota del sou mitjà.
 * input: 3 Mar 3000 Ona 3500 Ot 2500
 * output: Ot
 */

class Employee(val name: String, val salary: Double) {
    override fun toString(): String {
        return "$name $salary"
    }
    fun emplSalary() : Double{ //metodo innecesario
        return salary
    }
}


fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val employees = List(scanner.nextInt()) {
        Employee(scanner.next(), scanner.nextDouble())
    }

    val salaries = mutableListOf<Double>()
    for (employee in employees) {
        salaries.add(employee.salary)
    }
    val avgSalary = salaries.average()
//    var sum = 0.0
//    var i = 0
//    for (employee in employees) {
//        sum+= employee.salary
//        i++
//    }
//    val avgSalary = sum / i


    for (employee in employees) {
        if (employee.salary < avgSalary) println(employee.name)
    }

}