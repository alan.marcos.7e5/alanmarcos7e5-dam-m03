package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.autonomouscar

class AutonomousCar(var strDirection: String = "stop"): ICar {
    fun doNextNSteps(n :Int){
        var steps = n
        while (steps > 0) {
            executeProcess()
            steps--
        }
    }

    private fun executeProcess() {
        if (!isThereSomethingAt(Direction.FRONT, true)) go(Direction.FRONT)
        else if (!isThereSomethingAt(Direction.RIGHT,true)) go(Direction.RIGHT)
        else if (!isThereSomethingAt(Direction.LEFT,true)) go(Direction.LEFT)
        else stop()
    }

    override fun isThereSomethingAt(direction: Direction,result: Boolean) = result //to debug

    override fun go(direction: Direction) {
        when (direction){
            Direction.FRONT -> println(toStringCar("front"))
            Direction.RIGHT -> println(toStringCar("right"))
            Direction.LEFT -> println(toStringCar("left"))
        }
    }

    override fun stop() {
        println(toStringCar("stop"))
    }

    fun toStringCar(str: String): String {
        return "Direction: $str"
    }
}