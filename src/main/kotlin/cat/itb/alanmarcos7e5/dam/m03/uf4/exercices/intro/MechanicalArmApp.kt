package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.intro

/**
Volem fer un petit programa que simuli un braç mecànic.
El braç mecànic

Pot estar encès o apagat
Tenir una certa obertura (en angles)
Tenir una certa alçada (en cm).
Volem que el braç pugui

Modificar si està encès o apagat
Modificar l'obertura (sumar a l'actual)
Modificar l'alçada (sumar a l'actual)
Per defecte un braç està apagat, amb obertura 0 i alçada 0.

Notes:

Si està apagat, no es mot moure ni obrir
Els graus d'obertura van de 0 graus fins a 360
L'alçada mínima és de 0 cm

*******************************************************************
Fes un programa que apliqui al braç les següents instruccions:

mechanicalArm.setTurnedOn(true)
mechanicalArm.updateAltitude(3)
mechanicalArm.updateAngle(180)
mechanicalArm.updateAltitude(-3)
mechanicalArm.updateAngle(-180)
mechanicalArm.updateAltitude(3)
mechanicalArm.setTurnedOn(false)
Després de cada instrucció printa per pantalla l'estat del robot

Output
MechanicalArm{openAngle=0.0, altitude=0.0, turnedOn=true}
MechanicalArm{openAngle=0.0, altitude=3.0, turnedOn=true}
MechanicalArm{openAngle=180.0, altitude=3.0, turnedOn=true}
MechanicalArm{openAngle=180.0, altitude=0.0, turnedOn=true}
MechanicalArm{openAngle=0.0, altitude=0.0, turnedOn=true}
MechanicalArm{openAngle=0.0, altitude=3.0, turnedOn=true}
MechanicalArm{openAngle=0.0, altitude=3.0, turnedOn=false}
 */
open class MechanicalArm(var openAngle: Double = 0.0, var altitude: Double = 0.0, var turnedOn: Boolean = false){

    fun updateAngle(inputAngle: Int){
        if (turnedOn) {
            val newAngle = this.openAngle + inputAngle
            if (newAngle in 0.0..360.0) this.openAngle = newAngle
        }
    }

    fun updateAltitude(inputAltitude: Int){
        if (turnedOn) {
            val newAltitude = this.altitude + inputAltitude
            if (newAltitude >= 0) this.altitude = newAltitude
        }
    }

    fun setStatus(inputStatus: Boolean){
        this.turnedOn = inputStatus
    }

    override fun toString(): String {
        return "openAngle= $openAngle, altitude= $altitude, turnedOn= $turnedOn"
    }

}

fun main() {
    val mechanicalArm = MechanicalArm()
    mechanicalArm.setStatus(true)
    println(mechanicalArm)
    mechanicalArm.updateAltitude(3)
    println(mechanicalArm)
    mechanicalArm.updateAngle(180)
    println(mechanicalArm)
    mechanicalArm.updateAltitude(-3)
    println(mechanicalArm)
    mechanicalArm.updateAngle(-180)
    println(mechanicalArm)
    mechanicalArm.updateAltitude(3)
    println(mechanicalArm)
    mechanicalArm.setStatus(false)
    println(mechanicalArm)
    println(mechanicalArm.openAngle)
}