package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.intro.figures

class Rectangle(color: String, val height: Int, val width: Int) : Figure(color) {
    override fun paintText(){
        repeat(height){
            repeat(width){
                val rectangleColor = prepareColor()
                print("${rectangleColor}X")
            }
            println()
        }
        println()
    }
}