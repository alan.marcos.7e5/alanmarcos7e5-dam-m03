package cat.itb.alanmarcos7e5.dam.m03.uf2.modularitat

/**
 * Volem fer una classe anomenada Cell
 * Una cel·la és una casella que es pot localitazar en la posicióI i posicióJ d'un taulell del buscamines.
 * Ha de tenir un indicador de si la casella o cel·la conté una bomba
 * (per defecte serà false)
 * Ha de tenir un indicador de destapada per poder gestionar les cel·les que l'usuari ha descobert
 * (per defecte serà false)
 * Una propietat adjacència que indica la quantitat de bombes que hi ha adjacents a la casella.
 * Inicialment quan creem una casella ha de ser -1, es modificarà el valor a posteriori
 */
data class Cell(val positionI: Int,val positionJ: Int,
                var bomb: Boolean = false,
                var discovered: Boolean = false){

    var adjacencies = -1

    /*
    showInformation
    X : si no ha estat oberta
    B : si ha estat oberta i conté una bomba
    3 : si ha estat oberta mostra el valor de bombes adjacents (valor -1 .. 8)
     */
    fun showInformation():String{
        return if (!this.discovered) "X"
        else if (this.discovered && this.bomb) "B"
        else this.adjacencies.toString()
    }
}

