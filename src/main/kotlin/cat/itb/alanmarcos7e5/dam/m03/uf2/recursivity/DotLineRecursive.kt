package cat.itb.alanmarcos7e5.dam.m03.uf2.recursivity

import java.util.*

/**
 * Demana un enter a l'usuari.
 * Imprimeix per pantalla tants punts com l'usuari hagi indicat
 * Usa una funció recursiva
 * input
 * 10
 * output
 * ..........
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val userInput = scanner.nextInt()
    println(makeDotline(userInput))
}

fun makeDotline(times: Int): String{
    val dot = "."
    return if (times == 1) dot
    else if (times == 0) ""
    else{
        val nextDot = makeDotline(times-1)
        dot + nextDot
    }
}
