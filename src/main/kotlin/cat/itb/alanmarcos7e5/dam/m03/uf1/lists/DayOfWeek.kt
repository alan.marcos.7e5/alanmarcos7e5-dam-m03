package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * Donat un enter, printa el dia de la setmana amb text (dilluns, dimarts, dimecres…),
 * tenint en compte que dilluns és el 0. Els dies de la setmana es guarden en una llista
 */

fun main() {
    val listOfDays =
        listOf("dilluns", "dimarts", "dimecres", "dijous", "divendres", "dissabte", "diumenge")

    val scanner = Scanner(System.`in`)
    val dayOfWeek = scanner.nextInt()
    println(listOfDays[dayOfWeek])
}