package cat.itb.alanmarcos7e5.dam.m03.uf5.exercices.regex

import java.util.Scanner

/**
 * Considerarem que una contrassenya és de dificultat alta si:

té més de 8 caracters
té lletres i números
té un dels següents caràcters: @,#,~,$
no té 3 lletres consecutives
no té 3 números consecutius
Llegeix els passwords per pantalla i printa sí són de dificultat alta. S'acaba amb la paraula END.

Input
uh34$i3os92i$
aaaa1$we23ew23
3e$@
END
Ouput
Dificultat alta
No és de dificultat alta
No és de dificultat alta
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val password = scanner.nextLine()
    val regexCant = ".{8,}".toRegex()
    //println(password.matches(regexCant)) //bien
    val regexWordNum = "(\\w+\\d+)".toRegex() //or , pero iría and
    //val regexWordNum = "[[:alnum:]]*".toRegex() //seguir probando
    println(password.matches(regexWordNum))

}