package cat.itb.alanmarcos7e5.dam.m03.uf1.aliens

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val inputAlien = readIntListAlien(scanner)
    val inputMe = readIntListAlien(scanner)

    if (inputMe[inputMe.lastIndex] <= inputAlien[inputMe.lastIndex]) println("Corre!")
    else println("Espera")

}

fun readIntListAlien(scanner: Scanner): List<Int> {
    val listSize = scanner.nextInt()
    return List(listSize) { scanner.nextInt() }
}