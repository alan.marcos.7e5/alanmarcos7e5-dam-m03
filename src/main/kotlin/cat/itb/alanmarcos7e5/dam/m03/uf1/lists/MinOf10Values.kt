package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * L'usuari entra 10 enters. Crea un list amb aquest valors.
 * Imprimeix per pantalla el valor més petit introduit.
 */

fun main() {
    println("Ingresar 10 valores.")
    val scanner = Scanner(System.`in`)
    val listOfInt = List(10) { scanner.nextInt() }
    println(listOfInt)
    var minOfList = listOfInt[0]
    for (element in listOfInt){
        if (element < minOfList){
            minOfList = element
        }
    }
    println("El mínimo valore ingresado es: $minOfList")
}