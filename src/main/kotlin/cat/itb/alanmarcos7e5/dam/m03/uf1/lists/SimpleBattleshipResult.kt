package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*


/**
 * Donada la següent configuració del joc Enfonsar la flota, indica si a la posició x, y hi ha aigua o un vaixell (tocat)
 * Configuració
 * On x són baixells i els 0 aigua, casella superior esquerra és la 0,0 i la superior dreta la 0,6
 * x x 0 0 0 0 x
 * 0 0 x 0 0 0 x
 * 0 0 0 0 0 0 x
 * 0 x x x 0 0 x
 * 0 0 0 0 x 0 0
 * 0 0 0 0 x 0 0
 * x 0 0 0 0 0 0
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val table = MutableList(7){ MutableList(7){0} }
    //to create de table
    table[0][0]=1
    table[0][1]=1
    table[0][6]=1
    table[1][2]=1
    table[1][6]=1
    table[2][6]=1
    table[3][1]=1
    table[3][2]=1
    table[3][3]=1
    table[3][6]=1
    table[4][4]=1
    table[5][4]=1
    table[6][0]=1

    val userX = scanner.nextInt()
    val userY = scanner.nextInt()

    if (table[userY][userX] == 1) println("Acertat")
    else println("Aigua")


}