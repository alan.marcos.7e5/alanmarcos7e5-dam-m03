package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * Volem fer un programa que ens digui quins alumnes no han vingut a classe.
 * L'usuari introduirà enters ordenats de major a menor, indicant la posició dels alumnes que han vingut a classe.
 * Quan introdueixi un -1 és que ja ha acabat.
 * Tenim la següent llista d'alumnes:
 * "Magalí", "Magdalena", "Magí","Màlika", "Manel", "Manela", "Mar", "Marc", "Margalida",
 * "Marçal", "Marcel", "Maria", "Maricel", "Marina", "Marta", "Martí", "Martina", "Mausi",
 * "Mei-Xiu", "Miquel", "Ming", "Mohamed"
 * Input
 * 3 2 1 0 -1
 * Output
 * [Manel, Manela, Mar, Marc, Margalida, Marçal, Marcel, Maria, Maricel, Marina, Marta, Martí, Martina, Mausi, Mei-Xiu, Miquel, Ming, Mohamed]
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val listPresences = mutableListOf<Int>()
    do{
        var position = scanner.nextInt()
        if (position == -1) break
        listPresences.add(position)
    }while (position != -1)

    println(listPresences)

    val listStudents = mutableListOf<String>("Magalí", "Magdalena", "Magí","Màlika", "Manel", "Manela", "Mar", "Marc",
        "Margalida", "Marçal", "Marcel", "Maria", "Maricel", "Marina", "Marta", "Martí", "Martina", "Mausi",
        "Mei-Xiu", "Miquel", "Ming", "Mohamed")

    for (element in listPresences) {
        listStudents.removeAt(element)
    }

    println(listStudents.sorted())
}