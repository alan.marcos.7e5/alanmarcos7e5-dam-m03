package cat.itb.alanmarcos7e5.dam.m03.uf3

import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.nio.file.Path
import java.util.*
import kotlin.io.path.*

/**
 * Fes un programa que ens ajudi a crear una llista de la compra.
 * Al executar-se l'usuari introdurià les dades d'un producte.
 * Del producte en guardarà el nom, el preu i la quantitat que en vol comprar.
 * Després imprimeix el resum de la compra que ha anat afegint en les diferents execucions del programa
 * (guarda la informació en un fitxer json a la carpeta home).
 * Input
 * 1 Macarrons 2.35
 * Output
 * -------- Compra --------
 * 1 Macarrons (2.35€) - 2.35€
 * -------------------------
 * Total: 2.35€
 * -------------------------
 */
@Serializable
data class Product(val name: String, val quantity: Int, val price: Double){
    val totalPrice get() = quantity * price
}


fun main() {
    //verify path
    val homePath = Path(System.getProperty("user.home"))
    val filePath = homePath.resolve("BuyList.json")

    val listOfProducts = if (filePath.exists()) loadFromFile(filePath) else mutableListOf<Product>()
    //input product user
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val qnty = scanner.nextInt()
    val name = scanner.next()
    val price = scanner.nextDouble()
    //create and add product to file
    addProductAndSave(qnty,name,price,filePath,listOfProducts)
    toOutput(listOfProducts)

}

fun saveToFile(filePath: Path, listOfProducts: MutableList<Product> = mutableListOf<Product>()) {
    val serialicer = Json.encodeToString(listOfProducts)
    filePath.writeText(serialicer) //no hace falta array stanrd option, porque por defecto es WRITE
}

fun loadFromFile(filePath: Path): MutableList<Product> {
    val textOfFile = filePath.readText()
    return Json.decodeFromString<MutableList<Product>>(textOfFile)
}

fun addProductAndSave(qnty: Int, name: String, price: Double, filePath: Path, listOfProducts: MutableList<Product>) {
    val product = Product(name,qnty,price)
    listOfProducts += product
    saveToFile(filePath,listOfProducts)
    /*val serialicer = Json.encodeToString(listOfProducts)
    filePath.writeText(serialicer, options = arrayOf(StandardOpenOption.WRITE))*/
}

fun toOutput(listProduct: List<Product>) {
    println("-----COMPRA----\n\n")
    var total = 0.0
    for (product in listProduct) {
        println("${product.quantity} ${product.name} (${product.price}) - ${product.totalPrice}\n")
        total += product.totalPrice
    }
    println("--------------")
    println("total: $total")
}