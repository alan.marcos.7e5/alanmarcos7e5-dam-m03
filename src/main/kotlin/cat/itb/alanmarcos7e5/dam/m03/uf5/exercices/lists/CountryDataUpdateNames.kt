package cat.itb.alanmarcos7e5.dam.m03.uf5.exercices.lists

/**
 * Llegeix per pantalla una llista de països,
 * modifica'n el nom i la capital per a que estiguin escrites en majúscules i imprimeix-los.
 */
fun main() {
    val listOfCountries = readCountries()
    listOfCountries.forEach {
        updateNames(it)
        println(it)
    }

}

fun updateNames(it: Country) {
    it.name = it.name.uppercase()
    it.capital = it.capital.uppercase()
}
