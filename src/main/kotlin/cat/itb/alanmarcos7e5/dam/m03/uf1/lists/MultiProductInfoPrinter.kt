package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * Llegeix el nom i preu de diferents productes i imprimeix-los per pantalla amb el format indicat.
 * input:
 *          3 Taula 232.32 Cadira 90.90 Bolígraf 76.22
 * output:
 *          El producte Taula val 232.32€
 *          El producte Cadira val 90.90€
 *          El producte Bolígraf val 76.22€
 */

class Product (val name: String, val price: Double)


fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val products = List(scanner.next().toInt()) {
        Product(scanner.next(), scanner.next().toDouble())
    }
    for (product in products) {
        println("El producto ${product.name} val ${product.price}")
    }
}