package cat.itb.alanmarcos7e5.dam.m03.uf1.adventOfCode

import java.util.*
import kotlin.io.path.Path
import kotlin.math.pow
import kotlin.reflect.typeOf

/**
 * Day 3. Binary diagnostic
 */

fun main() {
    val scanner = Scanner(Path("data/aventofcode/inputDay3.txt"))
    val inputList = mutableListOf<String>()
    while (scanner.hasNext()){
        val line = scanner.nextLine()
        inputList.add(line)
    }
    val gammaList = MutableList(12){0}
    val epsilonList = MutableList(12){0}
    val zeroList = MutableList(12){0}
    val oneList = MutableList(12){0}

    for (e in inputList) {
        for ((i,c) in e.withIndex()) {
            if (c == '0') zeroList[i] += 1
            else oneList[i] += 1
        }
    }
    for ((i,e) in zeroList.withIndex()) {
        if (e > oneList[i]){
            gammaList[i] = 0
            epsilonList[i] = 1
        }
        else {
            gammaList[i] = 1
            epsilonList[i] = 0
        }
    }

    println("zeros = $zeroList \nones = $oneList \ngamma = $gammaList \nepsilon = $epsilonList")
    val result = binaryToDecimal(gammaList) * binaryToDecimal(epsilonList)
    println(result)

}

fun binaryToDecimal(binaryNumber : List<Int>) : Int {
    val invBinaryNumber = binaryNumber.asReversed()
    var decimalNumber = 0
    for ((i,e) in invBinaryNumber.withIndex()) {
        decimalNumber += (e * Math.pow(2.0, i.toDouble())).toInt()
    }
    return decimalNumber
}