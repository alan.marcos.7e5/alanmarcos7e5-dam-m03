package cat.itb.alanmarcos7e5.dam.m03.uf5.exercices.regex

import java.util.Scanner

/**
 * Donat una linia de text, conta el nombre de paraules plurals que té.
 * Considerarem plurals totes les paraules que acaben en s.
Input
Lorem ipsum dolors sit amet, consectetur adipisicis elit, sed eiusmod tempors incidunt ut labore et dolores magna aliqua.
Output
4
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val inputUser = scanner.nextLine()
    val regex = ".*s".toRegex()
    val inputToList = inputUser.split(" ")
    /*var count = 0
    inputToList.forEach {
        if (it.matches(regex)) count++
    }
    println(count)*/

    println( inputToList.count { it.matches(regex) })


}