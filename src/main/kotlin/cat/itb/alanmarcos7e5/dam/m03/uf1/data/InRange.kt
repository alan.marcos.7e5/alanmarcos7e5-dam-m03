import java.util.*

//Escriu un programa que llegeixi 5 enters. El primer i el segon creen un rang,
//el tercer i el quart creen un altre rang.
//Mostra true si el cinquè valor està entre els dos rangs, sino false.

fun main() {
    //ingreso dos valores para rango
    val scanner = Scanner(System.`in`)
    val valor1 = scanner.nextInt()
    val valor2 = scanner.nextInt()
    val myRange1 = valor1..valor2
    //ingreso 3y4 para otro rango
    val valor3 = scanner.nextInt()
    val valor4 = scanner.nextInt()
    val myRange2 = valor3..valor4
    //ingreso 5to valor
    val valor5 = scanner.nextInt()
    //compruebo usando IntRange
    val inRange = valor5 in myRange1 && valor5 in myRange2
    println(inRange)
}