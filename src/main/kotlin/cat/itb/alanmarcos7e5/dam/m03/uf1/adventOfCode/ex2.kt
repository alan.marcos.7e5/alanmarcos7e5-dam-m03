package cat.itb.alanmarcos7e5.dam.m03.uf1.adventOfCode

import java.util.*
import kotlin.io.path.Path

/**
 * DAY 2
 */

fun main() {
    val scanner = Scanner(Path("data/aventofcode/inputDay2.txt"))
    val inputList = mutableListOf<String>()
    while (scanner.hasNext()) {
        val movement = scanner.nextLine()
        inputList.add(movement)
    }
    var depth = 0.0
    var horizontal = 0.0
    var badTyped = 0

    for (element in inputList) {
        if ("forward" in element){
            horizontal += extractNumber(element)
        }
        else if ("down" in element) depth += extractNumber(element)
        else if ("up" in element) depth -= extractNumber(element)
        else badTyped++
    }
    println("depth = $depth      horizontal pos = $horizontal")
    val result = depth * horizontal
    println(result)
}

fun extractNumber(element : String) : Int{
    var strDigit = ""
    for (c in element) {
        if (c.isDigit()) strDigit += c
    }
    return strDigit.toInt()
}