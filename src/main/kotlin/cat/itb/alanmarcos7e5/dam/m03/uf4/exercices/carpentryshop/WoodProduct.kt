package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.carpentryshop

abstract class WoodProduct(val price: Double, val width: Double) {
    abstract val totalPrice: Double
}