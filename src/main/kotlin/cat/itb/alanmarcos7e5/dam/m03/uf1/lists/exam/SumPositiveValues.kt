package cat.itb.alanmarcos7e5.dam.m03.uf1.lists.exam

import cat.itb.alanmarcos7e5.dam.m03.uf1.lists.readIntList
import java.util.*

/**
 * L'usuari introduirà una llista d'enters, com s'indica al mètode ListReader.
 * Un cop llegits tots, printa per pantalla la suma de tots els valors positius.
 * input:
 * 5
 * -1 4 4 9 -7
 * output:
 * 17
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val userInput = readIntList(scanner)
    var positiveSum = 0
    for (element in userInput) {
        if (element > 0) positiveSum += element
    }

    println(positiveSum)
}