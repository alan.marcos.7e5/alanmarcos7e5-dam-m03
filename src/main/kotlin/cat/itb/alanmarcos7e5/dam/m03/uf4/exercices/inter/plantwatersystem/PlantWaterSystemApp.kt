package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.inter.plantwatersystem

/**
 * Volem fer un petit sistema de rec automàtic
 * El sistema rebrà del sensor les 20 últims registres d'humitat (valors decimals).
 * Si la mitja d'aquests valors és inferior a 2 hem d'activar el sistema de rec automàtic.
 *  el sistema tindrà dos mètodes
 */

fun main() {
    val plantWaterController = PlantWaterController()
    println(plantWaterController.averageOfMeasures)
    println(plantWaterController.waterSystem)
    plantWaterController.startWatterSystem()
    println(plantWaterController.averageOfMeasures)
    println(plantWaterController.waterSystem)
}