package cat.itb.alanmarcos7e5.dam.m03.uf1.data

import java.util.*

//Volem fer un programa per imprimir targetes de visites.
//Demana a l'usuari el nom i el 1r cognom i el número despatx on treballa. Guarda't la informació en una classe nova.
//Imprimeix el següent text:
//Empleada: Mar Puig - Despatx: 24

//creo la clase
class Empleado(var name : String, var workPlace : Int)

fun main() {
    //leer nombre y apellido
    val scanner = Scanner(System.`in`)
    val userNameCognom = scanner.nextLine()
    //leer nro de trabajo
    val userWork = scanner.nextInt()
    //guardar en una clase
    val myEmpleado : Empleado = Empleado(userNameCognom, userWork)
    //imprimir los datos de la variable que es del tipo de la clase Empleado
    println("Empleada: ${myEmpleado.name} - Despatx: ${myEmpleado.workPlace}")
}