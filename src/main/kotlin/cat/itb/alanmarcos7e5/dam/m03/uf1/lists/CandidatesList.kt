package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * Volem fer un petit programa per un partit polític. Quan hi ha eleccions, el partit presenta una llista de candidats.
 * Cada candidat té una posició a la llista.
 * El programa primer llegirà la llista de candidats (primer introduirem la quantitat i després un candidat per línia).
 * Un cop llegida la llista, l'usuari podrà preguntar quin candidat hi ha a cada posició.
 * El programa acaba quan introdueixi -1.
 * Tingues en compte que els politics no són informàtics,
 * i si indiquen la posició 1, volen dir el primer polític de la llista.
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val inputSize = scanner.nextLine().toInt() //cantidad de candidates
    val listCandidates = List(inputSize) {
        scanner.nextLine() //ingresar uno a uno los candidatos
    }

    println(listCandidates)

    var userQuery = scanner.nextLine()
    while (userQuery != "-1") {
        println(listCandidates[userQuery.toInt()-1])
        userQuery = scanner.nextLine()
    }

}