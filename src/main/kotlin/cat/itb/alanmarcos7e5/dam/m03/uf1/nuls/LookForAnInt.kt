package cat.itb.alanmarcos7e5.dam.m03.uf1.nuls

import cat.itb.alanmarcos7e5.dam.m03.uf1.lists.readIntList
import java.util.*

/**
 * Fes un programa que donada una llista d'enters i un enter, imprimeixi la primera aparició de l'enter o null.
 * Exemple 1
 * Input
 * 5
 * 1 3 9 4 3
 * 22
 * Output
 * null
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val inputList = readIntList(scanner)
    val toSearch = scanner.nextInt()
    val position = search(inputList , toSearch)
    println(position)
}

fun search(list : List<Int> , toSearch : Int) : Int? {
    for (i in list.indices) {
        if (list[i] == toSearch){
            return i
        }
    }
    return null
}
