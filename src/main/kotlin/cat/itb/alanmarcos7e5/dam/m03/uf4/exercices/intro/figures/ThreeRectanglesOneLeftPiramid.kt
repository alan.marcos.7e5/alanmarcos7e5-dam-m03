package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.intro.figures

/**
 * Volem fer un programa que dibuixi rectangles i piràmides (aliniades a l'esquerre) de colors a la consola.
Volem que dibuixi el següents rectanlges.
rectangle: color: RED, llargada: 4, amplada: 5
triangle: color: YELLOW, altura: 3
rectangle color: GREEN, llargada: 3, amplada: 5
Output (sense colors)
XXXXX
XXXXX
XXXXX
XXXXX

X
XX
XXX

XXXXX
XXXXX
XXXXX
 */

fun main() {
    val listOfAny = listOf<Any>(Rectangle(GREEN,2,4), Piramid(CYAN,4),Line(BLUE,5))
    for (figure in listOfAny){
        when (figure){
            is Rectangle -> figure.paint()
            is Piramid -> figure.paint()
            is Line -> figure.paint()
            else -> "Neither"
        }
    }
}