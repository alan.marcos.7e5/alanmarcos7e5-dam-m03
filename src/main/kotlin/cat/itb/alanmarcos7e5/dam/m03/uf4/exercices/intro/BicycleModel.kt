package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.intro

/**
 * Una empresa de venda de bicicletes vol fer una aplicació per tenir constància del seu estoc. De cada model de bicicleta en vol guardar el nom, marxes i marca.
 * De cada marca en vol guardar el nom i el país (el nom del país).
 * Crea les classes necessàries per guardar aquesta informació.
 * Fes un petit programa que crei 2 bicicletes d'una única marca i les imprimeixi per pantalla (no s'ha de llegir res de l'entrada).
 * Output
 * BicycleModel{name='Jett 24', gears=5, brand=BycicleBrand{name='Specialized', country='USA'}}
 * BicycleModel{name='Hotwalk', gears=7, brand=BycicleBrand{name='Specialized', country='USA'}}
 */
data class VehicleBrand(val name: String, val country: String)

class BicycleModel (name: String, vehicleBrand: VehicleBrand, val gears: Int): Vehicle(name,vehicleBrand){
    override fun toString(): String {
        return "name: $name - brand: $brand - $gears"
    }
}

fun main() {
    val vehicleBrand = VehicleBrand("Specialized","USA")
    val bike = BicycleModel("Jett 24",vehicleBrand,5)
    val bike2 = BicycleModel("Hotwalk",vehicleBrand,7)
    println(bike)
    println(bike2)
}