package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.quiz

class MultipleChoice
    (val answerOptions: List<String>, ask: String, programAnswer: String, userAnswer: Boolean = false):
    Question(ask, programAnswer, userAnswer) {

    override fun printQuestion() {
        println(ask)
        printAnswerOptions()
    }

    private fun printAnswerOptions() {
        for (answer in answerOptions) println(answer)
    }
}