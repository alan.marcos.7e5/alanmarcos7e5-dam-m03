package cat.itb.alanmarcos7e5.dam.m03.uf6.beachapp.data

import cat.itb.alanmarcos7e5.dam.m03.uf6.beachapp.models.Beach
import cat.itb.alanmarcos7e5.dam.m03.uf6.beachapp.models.CityWater
import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.util.stream.Collectors.toMap

class BeachDao (val database: BeachDatabase){
    val connection: Connection get() = database.connection!!

    fun createTableIfNotExists() {
        val createQuery = "CREATE TABLE IF NOT EXISTS beaches" +
                "(id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR, city VARCHAR, waterQuality INTEGER)"
        val createStatement = connection.prepareStatement(createQuery)
        createStatement.execute()
    }

    fun list(): List<Beach> {
        val query = "SELECT * FROM beaches"
        val listStatement = connection.createStatement()
        val result: ResultSet = listStatement.executeQuery(query)
        return toBeachList(result)
    }

    /**
     * Converts result set to list of beaches
     */
    private fun toBeachList(result: ResultSet): List<Beach> {
        val listBeach: MutableList<Beach> = mutableListOf()
        while (result.next()) {
            listBeach += toBeach(result)
        }
        return listBeach
    }

    /**
     * Converts result set to a beach on the current position of the set
     */
    private fun toBeach(result: ResultSet): Beach {
        val id = result.getInt("id")
        val name = result.getString("name")
        val city = result.getString("city")
        val waterQuality = result.getInt("waterQuality")
        return Beach(id,name,city,waterQuality)
    }

    fun insert(newBeach: Beach) {
        val insertQuery = "INSERT INTO beaches(id, name, city, waterQuality) VALUES(?, ?, ?, ?)"
        val insertStatement: PreparedStatement = connection.prepareStatement(insertQuery)
        insertStatement.setInt(1, newBeach.id)
        insertStatement.setString(2, newBeach.name)
        insertStatement.setString(3, newBeach.city)
        insertStatement.setInt(4, newBeach.waterQuality)
        insertStatement.execute()
    }

    fun update(id: Int, newWaterQuality: Int) {
        val updateQuery = "UPDATE beaches SET(waterQuality) = (?) WHERE id = ?"
        val updateStatement = connection.prepareStatement(updateQuery)
        updateStatement.setInt(1, newWaterQuality)
        updateStatement.setInt(2, id)
        updateStatement.execute()
    }

    fun listByWater(): MutableList<CityWater> {
        val query = "SELECT city, avg(waterQuality) AS water FROM beaches GROUP BY city ORDER BY water DESC"
        val groupStatement = connection.createStatement()
        val result: ResultSet = groupStatement.executeQuery(query)
        return toMap(result)
    }

    private fun toMap(result: ResultSet): MutableList<CityWater> {
        //val mapCity: MutableMap<String, Double> = mutableMapOf()
        val listCity = mutableListOf<CityWater>()
        while (result.next()) {
            val city = result.getString("city")
            val avg = result.getDouble("water")
            //mapCity[city] = avg
            listCity += CityWater(city, avg)
        }
        //return mapCity
        return listCity
    }

}