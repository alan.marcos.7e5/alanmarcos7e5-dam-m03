package cat.itb.alanmarcos7e5.dam.m03.uf3.BicingStations.ui

/**
 * This app is to practice APIs and JSON files.
 * At the first start it takes data from the bicing stations API of Bcn and save this data in a json file in pc(local).
 * All the info queried by user is taked from this local json file created.
 * Previously to close the app the local json file created it´s deleted.
 */
import cat.itb.alanmarcos7e5.dam.m03.uf3.BicingStations.client.loadFromApi
import cat.itb.alanmarcos7e5.dam.m03.uf3.BicingStations.logic.BicingStationsBcn
import cat.itb.alanmarcos7e5.dam.m03.uf3.BicingStations.logic.LogicOfStation
import kotlinx.serialization.json.Json
import java.nio.file.Path
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import java.util.*
import kotlin.io.path.*

suspend fun main() {
    val bcnStationsApi = loadFromApi()
    Ui(bcnStationsApi).start()
}

data class Ui(val dataFromApi: BicingStationsBcn, val scanner: Scanner = Scanner(System.`in`)){
    val filePath = obtainPath()

    val bcnStations = if (!filePath.exists()) {
        writeBicingFile(filePath,dataFromApi)
        readBicingFile(filePath)
    } else readBicingFile(filePath)

    val logicOfStation = LogicOfStation(bcnStations)
    val bicingStationsUi = BicingStationsUi(scanner, logicOfStation)

    fun start() {
        println(userOptions())
        var userSelection = scanner.nextInt()
        scanner.nextLine()

        while (userSelection != 0){
            when (userSelection){
                1-> bicingStationsUi.findStation()
                //TODO (continue putting the options)
                7-> bicingStationsUi
                0-> break
            }

            println(userOptions())
            userSelection = scanner.nextInt()
            scanner.nextLine()
        }
        println(deleteBicingFile(filePath))
    }

    private fun obtainPath(): Path {
        val homePath = Path(System.getProperty("user.home"))
        return homePath.resolve("Documents/DAMr1A/m03/JSON/station_status_api.json")
    }

    private fun userOptions(): String {
        return  "1-Station\n" +
                "2-ListOf Stations\n" +
                "3-Availables in station\n" +
                "4-Total availables\n" +
                "5-Total docks\n" +
                "6-OutOfService\n" +
                "7-Take from an API\n" +
                "0-Exit\n"
    }
}

fun writeBicingFile(filePath: Path, dataFromApi: BicingStationsBcn){
    val serialicer = Json.encodeToString(dataFromApi)
    filePath.writeText(serialicer)
}

fun readBicingFile(filePath: Path): BicingStationsBcn {
    val textOfFile = filePath.readText()
    val serialicer = Json { ignoreUnknownKeys = true }
    return serialicer.decodeFromString<BicingStationsBcn>(textOfFile)
}

fun deleteBicingFile(filePath: Path): String{
    return if (filePath.exists()){
        val parentName = filePath.parent
        val fileName = filePath.fileName
        filePath.toFile().delete()
        "$parentName\n$fileName Deleted successfully\n"
    }else "${filePath.name} not exists!"
}

