package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

/**
 * Inicialitza un list de floats de tamany 50, amb el valor 0.0f a tots els elements.
 * Després asigna els els valors següents a les posicions indicades i printa la llista:
 * primera: 31.0f
 * segona: 56.0f
 * vintena: 12.0f
 * última: 79.0f
 */

fun main() {
    val listOfFloats = MutableList(50){0.0f}
    println(listOfFloats)
    listOfFloats[0] = 31.0f
    listOfFloats[1] = 56.0f
    listOfFloats[19] = 12.0f
    listOfFloats[listOfFloats.lastIndex] = 79.0f
    println(listOfFloats)
}