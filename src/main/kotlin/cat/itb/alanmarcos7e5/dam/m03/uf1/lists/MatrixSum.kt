package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * Implementa un programa que demani dos matrius a l'usuari i imprimeixi la suma de les dues matrius.
 * Input
 * 3 4
 * 1 2 3 4
 * 2 1 2 1
 * 3 2 1 5
 * 3 4
 * 1 2 3 4
 * 4 3 2 1
 * 2 2 2 2
 * Output
 * 2 4 6 8
 * 6 4 4 2
 * 5 4 3 7
 */

fun main() {
    /*val scanner = Scanner(System.`in`)
    val matrix1 = scannerReadIntMatrix(scanner)
    val matrix2 = scannerReadIntMatrix(scanner)
    val matrixSum = mutableListOf<MutableList<Int>>()*/
    val matrix1 = mutableListOf(mutableListOf(3, 4), mutableListOf(1, 2, 3, 4), mutableListOf(4, 3, 2, 1), mutableListOf(2, 2, 2, 2))
    val matrix2 = mutableListOf(mutableListOf(3, 4), mutableListOf(1, 2, 3, 4), mutableListOf(4, 3, 2, 1), mutableListOf(2, 2, 2, 2))
    val matrixSum = mutableListOf(MutableList(2){0} , MutableList(4){0} , MutableList(4){0} , MutableList(4){0})
    println(matrix1)
    println(matrix2)
    for ((i,row) in matrix1.withIndex()) {
        for ((innerI,cell) in row.withIndex()) {
            val cellSum = cell + matrix2[i][innerI]
            matrixSum[i][innerI] = cellSum
        }
    }
    println(matrixSum)
}