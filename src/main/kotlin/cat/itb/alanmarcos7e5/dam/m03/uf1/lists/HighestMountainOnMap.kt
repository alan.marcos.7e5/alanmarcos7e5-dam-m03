package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

/**
 * Usant les imatges d'un satel·lit hem pogut fer raster o (mapa de bits)
 * [https://ca.wikipedia.org/wiki/Mapa_de_bits] que ens indica l'alçada d'un punt concret d'un mapa.
 * Hem obtingut la següent informació:
 * val map =listOf(listOf(1.5,1.6,1.8,1.7,1.6),listOf(1.5,2.6,2.8,2.7,1.6),listOf(1.5,4.6,4.4,4.9,1.6),
 * listOf(2.5,1.6,3.8,7.7,3.6),listOf(1.5,2.6,3.8,2.7,1.6));
 * Digues en quin punt(x,y) es troba el cim més alt i la seva alçada
 * Output
 * x, y: 1.5 metres
 * Substituint x i y, per les cordenades i 1.5 metres per l'alçada del cim més alt.
 */

fun main() {
    val mountain = HighestMountain(0,0,0.0)
    val result = highestMountain(mountain)
   /* val x = mountain.x
    val y = mountain.y
    val maxim = mountain.maxim*/
    println(mountain)
}

data class HighestMountain(var x : Int, var y :Int , var maxim : Double)
fun highestMountain (mountain: HighestMountain) {
    val map =listOf(listOf(1.5,1.6,1.8,1.7,1.6),listOf(1.5,2.6,2.8,2.7,1.6),
        listOf(1.5,4.6,4.4,4.9,1.6),listOf(2.5,1.6,3.8,7.7,3.6),listOf(1.5,2.6,3.8,2.7,1.6))
    var maxim = map[0][0]
    var x = 0
    var y = 0
    for ((i,element) in map.withIndex()) {
        for ((innerI,innerElement) in element.withIndex()) {
            if (innerElement > maxim) {
                maxim = innerElement
                x = i
                y = innerI
            }
        }
    }
    mountain.x = x
    mountain.y = y
    mountain.maxim = maxim

}