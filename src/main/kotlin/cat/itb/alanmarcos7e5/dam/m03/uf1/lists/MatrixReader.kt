package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * Dins del fitxer ListReader defineix la funció següent, que llegeix una matriu d'enters de l'usuari.
L'usuari primer entrarà el número de files i columnes i després els diferents enters.
Nota: semblant a ListReader
 */

/**
 * Reads an int matrix from user input.
 * The user first introduces the size of the matrix(NxM).
 * Then, introduces the integers one by one.
 * @return matrix of values introduced of size NxM
 */
fun scannerReadIntMatrix(scanner: Scanner): List<List<Int>> {
    println("Type size of the matrix: ")
    val matrixSize = scanner.nextInt()
    println("Follow the instructions for the inner lists.")
    return List(matrixSize) { readIntList(scanner) }
}

fun main() {
    val scanner = Scanner(System.`in`)
    println(scannerReadIntMatrix(scanner))
}