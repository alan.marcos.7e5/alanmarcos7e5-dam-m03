package cat.itb.alanmarcos7e5.dam.m03.uf1.iterative

import java.util.*

/**
 * S'imprimeix una piràmide d'altura N de #
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val height = scanner.nextInt()
    var i = 1
    val output = "#"
    while (i <= height) {
        repeat(i) { print(output) }
        println()
        i++
    }
}