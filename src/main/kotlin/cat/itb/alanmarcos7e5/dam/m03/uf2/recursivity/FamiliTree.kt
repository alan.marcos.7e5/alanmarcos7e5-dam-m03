package cat.itb.alanmarcos7e5.dam.m03.uf2.recursivity

/**
 * Volem fer un petit programa que imprimeixi per pantalla l'arbre genealògic d'una persona.
 * Ens han donat ja feta la classe Persona i les dades de la família.
 * Implemneta la funció printFamilyTree per a que produeixi l'output de mostra.
 *  ver el output que debería dar en web UF2
 */
data class Person(val name: String, val father: Person? = null, val mother: Person? = null)

fun main() {
    val person = Person("Ot", Person("Jaume", Person("Pere"), Person("Mar")),
        Person("Maria", Person("John", null, Person("Fatima", Person("Ali")))))
    printFamilyTree(person)
}

fun printFamilyTree(person: Person?, tabs: Int = 0) {
    repeat(tabs){ print("   ")}
    if (person == null) println("- ?")
    else {
        println("- ${person.name}")
        printFamilyTree(person.father,tabs+1)
        printFamilyTree(person.mother,tabs+1)
    }
}

