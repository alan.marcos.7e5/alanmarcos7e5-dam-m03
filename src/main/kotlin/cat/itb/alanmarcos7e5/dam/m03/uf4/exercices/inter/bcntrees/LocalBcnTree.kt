package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.inter.bcntrees

import cat.itb.alanmarcos7e5.dam.m03.uf3.BcnTree
import cat.itb.alanmarcos7e5.dam.m03.uf3.Tree
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

class LocalBcnTree() : DataBcnTree {
    override suspend fun listTrees(): List<BcnTree> {
        val text = Tree::class.java.getResource("/OD_Arbrat_Zona_BCN.json").readText()
        val serialicer = Json { ignoreUnknownKeys = true }
        return serialicer.decodeFromString<List<BcnTree>>(text)
    }
}