package cat.itb.alanmarcos7e5.dam.m03.uf5.exercices.lists

import cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.carpentryshop.WoodProduct
import cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.carpentryshop.WoodStrip
import cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.carpentryshop.instanceObjects
import java.util.*

/**
 * Usa les classes i funcions de l'exercici UF4.CarpentryShop i llegeix un conjunt de productes.
 * Imprimeix per pantalla:
 * Preu total dels llistons venuts
 * La llargada del producte més car
 * Quantitat de productes de més de 100€
 */

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val size = scanner.nextInt()
    scanner.nextLine()
    val listOfProductStr = List(size){scanner.nextLine()}
    val listOfProductObj = instanceObjects(listOfProductStr).filterNotNull()
    calculateCarpentryData(listOfProductObj)
}

fun calculateCarpentryData(listOfProductObj: List<WoodProduct>) {
    //val totalPrice = listOfProductObj.filter{it is WoodStrip}.sumOf { it!!.totalPrice }
    val totalPrice = listOfProductObj.filterIsInstance<WoodStrip>().sumOf { it.totalPrice }
    println("Preu total: $totalPrice")

    val mostExpensive = listOfProductObj.maxOf { it.totalPrice }
    listOfProductObj.filter { it.totalPrice == mostExpensive }.forEach { println(it.width) } //large most expensive

    val maxProduct = listOfProductObj.maxByOrNull { it.totalPrice }
    println("EL largo del que tiene el mayor precio es: ${maxProduct!!.width}")

    val upperThan100 = listOfProductObj.count { it.totalPrice > 100 }
    println(upperThan100)
    listOfProductObj.forEach { println(it.totalPrice) } //all the total prices
}
