package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * Indica si una paraula és palíndrom.
 * Els strings funcionen com si fossin llistes de chars
 * Exemples de palíndroms:
 * cec, cuc, gag, mínim, nan, nen, pipiripip…
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val inputUser = scanner.next()
    scanner.nextLine()
    val myListedInput = mutableListOf<Char>()
    for (character in inputUser) {
        myListedInput.add(character)
    }
    val myLstedInputReverse = myListedInput.asReversed()
    if (myListedInput == myLstedInputReverse) {
        println("$inputUser es palíndromo")
    } else {
        println("$inputUser no es palíndromo\n $myLstedInputReverse")
    }
}