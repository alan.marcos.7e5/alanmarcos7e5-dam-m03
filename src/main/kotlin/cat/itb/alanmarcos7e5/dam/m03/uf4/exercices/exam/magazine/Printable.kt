package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.exam.magazine

interface Printable{
    /**
     * Prints the printable to the printer
     */
    fun print(magazinePrinter: MagazinePrinter)
}