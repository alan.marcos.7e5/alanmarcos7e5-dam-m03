package cat.itb.alanmarcos7e5.dam.m03.uf2.recursivity

import java.util.*

/**
 * L'usuari introduirà primer la llargada de la llista. Després introduirà les diferents caselles fixades. Escriurà -1 quan ja hagi acabat.
 * Indica si és "possible" o "impossible"
 * Nota: la primera casella i l'última de la llista sempre estan definides.
 * Input
 * 12
 * 0 5
 * 6 11
 * 8 11
 * 11 8
 * -1
 * Output
 * possible
 */
//TODO no me sale
fun main() {
    val scanner = Scanner(System.`in`)
    val incompleteList = makeLIst(scanner) //make list to analyze
    val properList = verifyValues(incompleteList)
    if (properList){
        compareNumbers(incompleteList)
    }
    else outputError()
}

fun compareNumbers(list: MutableList<Int?>, prevNum: Int? = null, i: Int = 0, ) {
    var numDif: Int
    var indDif: Int
    val number = list[i]
    //if (list[i+1] == null)
}


fun makeLIst(scanner: Scanner): MutableList<Int?> {
    val size = scanner.nextInt()
    val list: MutableList<Int?> = MutableList(size){null}
    var i = scanner.nextInt()
    while (i != -1) {
        val number = scanner.nextInt()
        list[i] = number
        i = scanner.nextInt()
    }
    return list
}

fun verifyValues(list: MutableList<Int?>) = (list.first() != null) and (list.last() != null)
fun outputError()="The first and the last values must be different from null"
