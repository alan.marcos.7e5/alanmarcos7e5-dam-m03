package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.autonomouscar

interface ICar {
    /**
     * @param result was added only to debug and change easy the condition in the way of car
     */
    fun isThereSomethingAt(direction: Direction, result: Boolean) : Boolean
    fun go(direction : Direction)
    fun stop()
}