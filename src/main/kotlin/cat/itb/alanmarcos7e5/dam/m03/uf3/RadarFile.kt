package cat.itb.alanmarcos7e5.dam.m03.uf3

import cat.itb.alanmarcos7e5.dam.m03.uf2.functions.avg
import java.nio.file.Path
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.exists

/**
 * Un radar de transit va guardant les diferents velocitats que registra en un fitxer.
 * Guarda les velocitats en km/hora, separades per un espai. Et pots descarregar un exemple de fitxer aquí
 * Fes un programa demani a l'usuai la ruta del fitxer i que calculi la velocitat màxima, mitjana i mínima detectada.
 * Input
 * /home/sjo/radar.txt
 * Output
 * Velocitat màxima: 100km/h
 * Velocitat mínima: 50km/h
 * Velocitat mitjana: 75km/h
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val userInput = scanner.nextLine()
    val path = Path(userInput)
    val scannerData = Scanner(path)
    if (path.exists()){
        val radarList= generateList(scannerData)
        println(calculateData(radarList))
    }
}

fun calculateData(radarList: MutableList<Int>): String {
    return "velocidad media: ${avgVelocity(radarList)}\n" +
            "vel max: ${maxVel(radarList)}\n"+
            "vel min: ${minVel(radarList)}"

}

fun minVel(radarList: MutableList<Int>): Int {
    var min = radarList[0]
    for (e in radarList) {
        if (e < min) min = e
    }
    return min
}

fun maxVel(radarList: MutableList<Int>): Int {
    var max = radarList[0]
    for (e in radarList) {
        if (e > max) max = e
    }
    return max
}

fun avgVelocity(radarList: MutableList<Int>) = avg(radarList)

fun generateList(scanner: Scanner): MutableList<Int> {
    val inputList = mutableListOf<Int>()
    while (scanner.hasNextInt()) {
        inputList += scanner.nextInt()
    }
    return inputList
}
