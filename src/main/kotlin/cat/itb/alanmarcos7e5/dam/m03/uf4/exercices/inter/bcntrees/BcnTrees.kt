package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.inter.bcntrees

import java.util.Scanner
/**
 * obtener una lista de arboles desde API o desde un fichero local
 */

suspend fun main() {
    val scanner = Scanner(System.`in`)
    val useApi = scanner.nextLine()
    val scientificNameOfTree = scanner.nextLine()
    val dataBcnTree: DataBcnTree = if (LoadFrom.valueOf(useApi) == LoadFrom.NETWORK) ApiBcnTree() else LocalBcnTree()
    val listOfTrees = dataBcnTree.listTrees()
    println(dataBcnTree.countOfSpecie(listOfTrees,scientificNameOfTree))
}

enum class LoadFrom{
    NETWORK, LOCAL
}



