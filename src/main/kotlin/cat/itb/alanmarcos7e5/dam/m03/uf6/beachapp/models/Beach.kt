package cat.itb.alanmarcos7e5.dam.m03.uf6.beachapp.models

/**
 * Volem fer un programa per gestionar l'estat de les platges.
 * De cada platja ens en volem guardar
 * un identificador numèric (introduït, no generat),
 * el nom, la ciutat i qualitat de l'aigua (valor entre 1 i 5).
 */

data class Beach(val id: Int, val name: String, val city: String, var waterQuality: Int){
    //constructor(name: String, city: String, waterQuality: Int): this(0, name, city, waterQuality)
}

