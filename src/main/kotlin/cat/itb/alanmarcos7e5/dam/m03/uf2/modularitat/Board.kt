package cat.itb.alanmarcos7e5.dam.m03.uf2.modularitat

/**
 * Volem fer una classe anomenada Board (taulell)
 * Per crear un objecte (instància) d'aquest tipus necessitarem indicar quina mida
 * tindrà el nostre taulell n (serà quadrat) i la quantitat de bombes que contindrà.
 * (Suposarem que bombes < n*n , es a dir que les bombes caben en el taulell)
 * Tindrà una propietat matriu que serà el llistat de caselles.
 * Fes ús de la classe Cell creada anteriorment
 * var matrix = MutableList(n) { i -> MutableList(n) { j -> Cell(i, j) } }
 */

data class Board(val n: Int, val bombsCounty: Int){
    var matrix = MutableList(n){i -> MutableList(n) { j -> Cell(i, j) }} //consultar esto

    fun addBombs(listOfCell: List<Cell>){
        for (cell in listOfCell) {
            matrix[cell.positionI][cell.positionJ] //TODO put bomb
        }
    }
    //TODO continue
}
