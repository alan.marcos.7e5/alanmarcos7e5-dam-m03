package cat.itb.alanmarcos7e5.dam.m03.uf2.generalexam

/**
 * Contains the list of spills and the logic of the program
 */
data class LogicOil(val listOfSpills: MutableList<Spill> = mutableListOf()){
    /**
     * Add a spill to the list of spills
     * @param spill it´s previously created in the UI
     * @see OilSpillArchiveUi.addSpillUi
     */
    fun addSpill(spill: Spill){
        listOfSpills.add(spill)
    }

    /**
     * search the most gravity spill in the list
     * @return the most or null if the list is empty
     */
    fun searchWorst(): Spill? {
        if (listOfSpills.isEmpty()) return null
        var worst = listOfSpills.first()
        for (s in listOfSpills) {
            if (s.gravity > worst.gravity) worst = s
        }
        return worst
    }

    /**
     * search the spills that have a same company
     * @param company string that indicates the name of company to search
     * @return a list of spills
     */
    fun spillsCompany(company: String): List<Spill> {
        val list = mutableListOf<Spill>()
        for (s in listOfSpills) {
            if (s.company == company) list += s
        }
        return list
    }

    /**
     * search the gravity of a spill
     * @return a double or null if can´t search the indicated spill
     * @see searchSpill
     */
    fun spillGravity(name: String): Double? {
        val spill = searchSpill(name)
        return if (spill != null) spill.gravity
        else null
    }
    /**
     * search the liters of a spill
     * @return a double or null if can´t search the indicated spill
     * @see searchSpill
     */
    fun spillLitres(name: String): Int? {
        val spill = searchSpill(name)
        return if (spill != null) spill.litres
        else null
    }

    /**
     * search a spill by an indicated name
     * @return a spill or null if not exists a spill with the indicated name
     */
    fun searchSpill(name: String): Spill?{
        for (s in listOfSpills){
            if (s.name == name) return s
        }
        return null
    }
    //todo (complete this las item)
/*    fun worstGravity(): Spill{
        for (s in listOfSpills) {
            for () {
            }
        }
    }*/


}