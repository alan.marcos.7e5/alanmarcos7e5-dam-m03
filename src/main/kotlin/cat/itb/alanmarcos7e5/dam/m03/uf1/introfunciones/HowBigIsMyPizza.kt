package cat.itb.alanmarcos7e5.dam.m03.uf1.introfunciones

import java.util.*

/*
Llegeix el diametre d'una pizza rodona i imprimeix la seva superfície.
 */

fun calculateCircleArea(radius : Double) : Double{
    val circleArea = Math.PI * radius * radius
    return  circleArea
}

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val inputDiameter = scanner.nextDouble()
    val circleArea = calculateCircleArea(inputDiameter/2)
    println(circleArea)
}