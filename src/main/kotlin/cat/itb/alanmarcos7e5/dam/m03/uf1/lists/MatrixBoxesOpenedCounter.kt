package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * Un banc té tot de caixes de seguretat en una graella, enumerades per fila i columna del 0 al 3.
 * Volem registar quan els usuaris obren una caixa de seguretat, i al final del dia, fer-ne un recompte.
 * L'usuari introduirà parells d'entrers del 0 al 3 quan s'obri la caixa indicada.
 * Quan introduiexi l'enter -1, és que s'ha acabat el dia. Printa per pantalla el nombre de cops que s'ha obert.
 * Input
 * 1 1 1 3 3 3 1 1 3 3 3 0 0 3 -1
 * Output
 * [[0, 0, 0, 1], [0, 2, 0, 1], [0, 0, 0, 0], [1, 0, 0, 2]]
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val caixa = MutableList(4){ MutableList(4){0} }

    val userList = mutableListOf<Int>()
    var userInput = scanner.nextInt()
    while (userInput != -1){
        userList += userInput
        userInput = scanner.nextInt()
    }

    for (i in 0 until userList.size step 2) {
        if (i < userList.size-1) {
            val userY = userList[i]
            val userX= userList[i+1]
            caixa[userY][userX]++
        }
    }
    println(caixa)
}