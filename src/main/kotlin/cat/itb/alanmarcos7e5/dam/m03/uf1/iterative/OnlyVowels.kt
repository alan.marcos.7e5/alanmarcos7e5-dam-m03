package cat.itb.alanmarcos7e5.dam.m03.uf1.iterative

import java.util.*

/**
 *  Donada una llista de lletres, imprimeix únicament les vocals que hi hagi.
    L'entrada consta de dues parts:

    Primer s'indica la quantitat de lletres
    A continuació venen les lletres separades per espais en blanc
    Imprimeix totes les que són vocals.
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val listLength = scanner.nextInt() //cantidad de letras
    for (i in 0 until listLength){
        val letter = scanner.next() //leo con el scanner la siguiente letra
        when (letter.toString()){
            "a","e","i","o","u" -> print(letter)
        }
    }
}