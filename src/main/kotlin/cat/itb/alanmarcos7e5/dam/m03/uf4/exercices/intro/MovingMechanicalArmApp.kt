package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.intro

/**
 * Volem afegir un nou tipus de braç mecànic que es pugui desplaçar per una cinta.
En concret, el braç mecànic mòbil es pot desplaçar un nombre real (double) de metres endavant i endarrere. Comença a la posició 0, no es pot despaçar en una posició inferior a la 0.

Crea les classes que necessitis per a poder-ho representar. Hem de tenir en compte que volem poder seguir creant robots del tipus MechanicalArm (que no es puguin moure).

Fes un programa que apliqui al braç les següents instruccions:

toggle()
updateAltitude(3)
move(4.5)
updateAngle(180)
updateAltitude(-3)
updateAngle(-180)
updateAltitude(3)
move(-4.5)
toggle()
 */
fun main() {
    val movingMechanicalArm = MovingMechanicalArm()
    val position = movingMechanicalArm.position
    val altitude = movingMechanicalArm.altitude
    println(altitude)
    movingMechanicalArm.move(2.3)
    println(movingMechanicalArm.position)
    println(movingMechanicalArm.turnedOn)
    movingMechanicalArm.setStatus(true)
    println(movingMechanicalArm.turnedOn)
    movingMechanicalArm.move(2.3)
    println(movingMechanicalArm.position)
}

