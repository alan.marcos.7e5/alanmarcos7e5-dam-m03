package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * Un banc té tot de caixes de seguretat, enumerades del 0 al 10.
 * Volem registar quan els usuaris obren una caixa de seguretat, i al final del dia, fer-ne un recompte.
 * L'usuari introduirà enters del 0 al 10 quan s'obri la caixa indicada.
 * Quan introduiexi l'enter -1, és que s'ha acabat el dia. Printa per pantalla el nombre de cops que s'ha obert.
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val listBoxes = MutableList(11) { 0 }
    var openedBox = scanner.nextInt()
    while (openedBox != -1){
        listBoxes[openedBox] ++
        openedBox = scanner.nextInt()
    }
    println(listBoxes)
}