package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.instrumentsimulator

class Triangle(val resonance: Int): Instrument() {
    override fun sound() {
        val repeatedResonance = "I".repeat(resonance)
        print("T${repeatedResonance}NC ")
    }
}