package cat.itb.alanmarcos7e5.dam.m03.uf1.mix

import cat.itb.alanmarcos7e5.dam.m03.uf1.lists.scannerReadIntMatrix
import java.util.*

/**
 * No obstant, el pare no és capac d'implementar un programa que, un cop digitalitzades les divisions,
 * digui si són o no correctes. ¿Podries ajudar-lo?
 * El programa haurà de dir quines divisions són incorrectes i quines correctes.
 * input:

2 2 1 0
4 2 5 7
6 3 2 0
-1
output:

correcte
error
correcte
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val matrix = mutableListOf<List<Int>>()

    var userInput = scanner.nextInt()
    while (userInput != -1) {
        val userList = mutableListOf<Int>()
        var i = 0
        while (i < 4) {
            userList += userInput
            i++
            if (i < 4) userInput = scanner.nextInt()
        }
        matrix += userList
        userInput = scanner.nextInt()
    }

    val result = calculateDivision(matrix)
    for (e in result) println(e)
}

fun calculateDivision(matrix: List<List<Int>>): List<String>{
    val output = mutableListOf<String>()
    for (row in matrix) {
        output += if (row[0] / row[1] == row[2] && row[0] % row[1] == row[3]) "correcte"
        else "error"
    }
    return output
}