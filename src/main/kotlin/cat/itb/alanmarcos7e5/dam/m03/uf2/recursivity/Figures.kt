package cat.itb.alanmarcos7e5.dam.m03.uf2.recursivity

import java.util.*

/**
 * Fer un programa que llegeixi un nombre enter positiu n i
 * escrigui el nombre de xifres que té.
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val number = scanner.nextInt()
    println(calculateCifresRecursive(number))
}

fun calculateCifresRecursive(number: Int, cifres: Int = 0): Int {
    return if (number < 1) cifres
    else calculateCifresRecursive(number/10,cifres+1)
}

