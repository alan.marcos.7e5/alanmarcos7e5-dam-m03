package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * L'usuari entrarà un conjunt d'enters per pantalla. Quan introdueixi el -1, és que ja ha acabat.
 * Imprimeix-los en odre invers.
 * Input
 * 1 2 3 4 8 2 -1
 * Output
 * [2, 8, 4, 3, 2, 1]
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val intList = mutableListOf<Int>()
    var number = scanner.nextInt()
    while (number != -1){
        intList.add(0,number)
        number = scanner.nextInt()
    }
    println(intList)
}