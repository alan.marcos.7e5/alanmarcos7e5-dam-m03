package cat.itb.alanmarcos7e5.dam.m03.uf2.recursivity

import java.util.*

/**
 * Calcula el factorial d'un valor
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val number = scanner.nextInt()
    println(factorialRecursive(number))
}

fun factorialRecursive(number: Int): Long {
    return if (number == 0) 1
    else number * factorialRecursive(number-1)
}
