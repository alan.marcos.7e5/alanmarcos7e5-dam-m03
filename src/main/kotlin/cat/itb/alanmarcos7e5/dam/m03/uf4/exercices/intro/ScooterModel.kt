package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.intro

class ScooterModel(name: String, brand: VehicleBrand, val power: Double): Vehicle(name,brand) {
    override fun toString(): String {
        return "name: $name - brand: $brand - $power"
    }
}