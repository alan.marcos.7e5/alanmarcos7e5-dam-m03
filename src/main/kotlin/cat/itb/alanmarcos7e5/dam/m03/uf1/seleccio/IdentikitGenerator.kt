package cat.itb.alanmarcos7e5.dam.m03.uf1.seleccio

import java.util.*

//Un grup d'investigadors ens ha demanat un programa per generar retrats robots.
//L'usuari ha d'introduïr el tipus de cabells, ulls, nas i boca i s'imprimeix
//per pantalla un dibuix de com és el sospitós.
//Els cabells poden ser arrissats @@@@@, llisos VVVVV o pentinats XXXXX
//Els ulls poden ser aclucats .-.-., rodons .o-o. o estrellats .*-*.
//El nas pot ser aixafat ..0.., arromangat ..C.. o agilenc ..V..
//La boca pot ser normal .===., bigoti .∼∼∼. o dents-sortides .www.

fun hair (typeHair : String) : String{
    val outputHair = when (typeHair) {
        "arrissats" -> "@@@@@"
        "llisos" -> "VVVVV"
        "pentinats" -> "XXXXX"
        else -> "Ingrese un cabello correcto"
    }
    return outputHair
}

fun eyes (typeEyes : String) : String{
    val outputEyes = when (typeEyes) {
        "aclucats" -> ".-.-."
        "rodons" -> ".o-o."
        "estrellats" -> ".*-*."
        else -> "Ingrese un tipo de ojos correcto"
    }
    return outputEyes
}

fun nose(typeNose : String) : String{
    val outputNose = when (typeNose) {
        "aixafat" -> "..0.."
        "arromangat" -> "..C.."
        "agilenc" -> "..V.."
        else -> "Ingrese un tipo de naríz correcto"
    }
    return outputNose
}

fun mouth(typeMouth : String) : String{
    val outputMouth = when (typeMouth) {
        "normal" -> ".===."
        "bigoti" -> ".∼∼∼."
        "dents-sortides" -> ".www."
        else -> "Ingrese un tipo de naríz correcto"
    }
    return outputMouth
}


fun main() {
    //Lectura de usuario
    val scanner = Scanner(System.`in`)
    println("CABELLO: ")
    val inputHair = scanner.nextLine()
    println("OJOS: ")
    val inputEyes = scanner.nextLine()
    println("NARIZ: ")
    val inputNose = scanner.nextLine()
    println("BOCA: ")
    val inputMouth = scanner.nextLine()
    //Lógica de programa
    //Cabello
    val outputHair = hair(inputHair)

    //Ojos
    val outputEyes = eyes(inputEyes)

    //Naríz
    val outputNose = nose(inputNose)

    //Boca
   val outputMouth = mouth(inputMouth)

    //Salida por pantalla
    println("$outputHair\n$outputEyes\n$outputNose\n$outputMouth")

}