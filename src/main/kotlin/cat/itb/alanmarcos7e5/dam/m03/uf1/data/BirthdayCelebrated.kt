import java.util.*

//Digues si una persona ha celebrat l'aniversari enguany.
//Suposarem les variables següents: diaAniversari, mesAniversari, diaActual, MesActual

fun main() {
    val scanner = Scanner(System.`in`)
    println("Ingresa dia de cumpleaños: ")
    val diaAniversari = scanner.nextInt()
    println("Ingresa mes de cumpleaños: ")
    val mesAniversari = scanner.nextInt()
    println("Ingresa dia actual ")
    val diaActual = scanner.nextInt()
    println("Ingresa mes actual: ")
    val mesActual= scanner.nextInt()
    //comparo los datos
    val celebration = mesAniversari < mesActual || (diaAniversari <= diaActual && mesAniversari <= mesActual)
    println(celebration)
}