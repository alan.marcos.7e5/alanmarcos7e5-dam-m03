package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.motto

fun main() {
    val lisOfTeams =
        listOf<Team>(
            BasketTeam("Mosques", "Bzzzanyarem"),
            VoleyTeam("Dragons!", "Grooarg", "Verde!"),
            GolfTeam("Abelles", "Piquem Fort","Cachi Chien 99"))
    shoutMottos(lisOfTeams)
}

fun shoutMottos(teams: List<Team>){
    for(team in teams)
        team.shoutMotto()
}