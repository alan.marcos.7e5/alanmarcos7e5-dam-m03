package cat.itb.alanmarcos7e5.dam.m03.uf3.BicingStations.client

import cat.itb.alanmarcos7e5.dam.m03.uf3.BicingStations.logic.BicingStationsBcn
import cat.itb.alanmarcos7e5.dam.m03.uf3.createJsonHttpClient
import io.ktor.client.request.*

suspend fun loadFromApi(): BicingStationsBcn{
    val client = createJsonHttpClient(true)
    return client.get("https://api.bsmsa.eu/ext/api/bsm/gbfs/v2/en/station_status")
}