package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * L'usuari entra 10 enters. Imprimeix-los en l'odre invers al que els ha entrat.
 * Existeix algun mètode de llistes que ja implementa aquesta tasca?
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val inputUser = List(10) {scanner.nextInt()}
    println(inputUser)
    println(inputUser.asReversed()) //la forma con mètodo
    val invertedInput = mutableListOf<Int>()
    for (i in (inputUser.size-1) downTo 0){
        invertedInput.add(inputUser[i])
    }
    println("Lista invertida manualmente: $invertedInput")
}