package cat.itb.alanmarcos7e5.dam.m03.uf1.seleccio

import java.util.*

//Volem fer el joc de pedra paper tisora.
//L'usuari introdueix dos enters (1)pedra, (2) paper, (3) tisora.
//Imprimeix per pantall Guanya el primer, Guanya el segon, Empat segons els enters introduits.

fun main() {
    val scanner = Scanner(System.`in`)
    println("introdueix dos enters (1)pedra, (2) paper, (3) tisora.")
    val inputUser1 = scanner.nextInt()
    val inputUser2 = scanner.nextInt()
    var outputAnswer : String = "Empat"
    if (inputUser1 == 1){
        if (inputUser2 == 2) outputAnswer = "Guanya el segon"
        if (inputUser2 == 3) outputAnswer = "Guanya el primer"
        println(outputAnswer)
    }
    if (inputUser1 == 2){
        if (inputUser2 == 1) outputAnswer = "Guanya el primer"
        if (inputUser2 == 3) outputAnswer = "Guanya el segon"
        println(outputAnswer)
    }
    if (inputUser1 == 3){
        if (inputUser2 == 2) outputAnswer = "Guanya el primer"
        if (inputUser2 == 1) outputAnswer = "Guanya el segon"
        println(outputAnswer)
    }
}