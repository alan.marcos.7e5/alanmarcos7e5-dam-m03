package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.quiz

class FreeText(ask: String, programAnswer: String, userAnswer: Boolean = false) :
    Question(ask, programAnswer, userAnswer) {

    override fun printQuestion() {
        println(ask)
    }
}