package cat.itb.alanmarcos7e5.dam.m03.uf5.exam


import java.util.Scanner

/**
Tenim les dades d'un sensor de qualitat d'aire.
Volem fer un programa que llegeixi una llista de registres de la consola i imprimeixi per pantalla:
El registre amb el no2 més alt
La mitjana de pm10 de tots els registres a partir del 2021
El nombre de registres en els que s'han superat algun dels llindars.
El no2 no hauria de ser superior a 4, el pm2.5 superior a 15 i el pm10 no hauria de ser superiror a 45.
De l'any 2022 els tres pitjors casos de n02, amb els pitjors casos al capdavant.
 */

/**
De cada registre tenim:
l'any, el mes, el dia, la quantitat de no2 (dioxid nitrós),
de pm10 ( partícules sòlides o líquides disperses a l'atmosfera, amb un diàmetre d'entre 2,5 i 10 µm.)
i pm2.5.
 */
data class Register(val year: String, val month: String, val day: String,
val no2: Double, val pm10: Double, val pm25: Double){
}

fun main() {
    val scanner = Scanner(System.`in`)
    val listOfRegisters = List(scanner.nextLine().toInt()){
        val year = scanner.next()
        val month = scanner.next()
        val day = scanner.next()
        val no2 = scanner.next().toDouble()
        val pm10 = scanner.next().toDouble()
        val pm25 = scanner.next().toDouble()
        Register(year, month, day, no2, pm10, pm25)
    }
    scanner.nextLine()
    //El registre amb el no2 més alt
    println( findMaxNo2(listOfRegisters) ?: "No se encontró registro")
    //La mitjana de pm10 de tots els registres a partir del 2021
    println( getPm10Avg(listOfRegisters,"2021") )
    //El no2 no hauria de ser superior a 4, el pm2.5 superior a 15 i el pm10 no hauria de ser superiror a 45.
    println( dangerousCount(listOfRegisters) )
    //De l'any 2022 els tres pitjors casos de n02, amb els pitjors casos al capdavant.
    printBadCases(listOfRegisters)
   /* val badCasesList = getBadCases(listOfRegisters, "2022", 3)
    var i = 1
    badCasesList.forEach {
        println("$i. $it")
        i++
    }*/

}

fun printBadCases(listOfRegisters: List<Register>) {
    val badCasesList = getBadCases(listOfRegisters, "2022", 3)
    var i = 1
    badCasesList.forEach {
        println("$i. $it")
        i++
    }
}

fun dangerousCount(listOfRegisters: List<Register>) = listOfRegisters.count {it.pm10 > 45 || it.no2 > 4 || it.pm25 > 15}

fun getBadCases(listOfRegisters: List<Register>, year: String, n: Int): List<Register> {
     return listOfRegisters.filter { it.year == year }.sortedByDescending { it.no2 }.take(n)
}

fun getPm10Avg(listOfRegisters: List<Register>, year: String): Double {
    val listOf2021 = listOfRegisters.filter { it.year >= year }
    var sum = 0.0
    listOf2021.forEach { sum += it.pm10 }
    return sum / listOf2021.size
}

fun findMaxNo2(listOfRegisters: List<Register>): Register? {
    var maxNo2 = 0.0
    var register: Register? = null
    listOfRegisters.forEach {
        if (it.no2 > maxNo2) {
            maxNo2 = it.no2
            register = it
        }
    }
    return register
}
