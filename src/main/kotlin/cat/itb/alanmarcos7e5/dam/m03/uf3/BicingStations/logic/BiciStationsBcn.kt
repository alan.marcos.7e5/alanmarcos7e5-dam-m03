package cat.itb.alanmarcos7e5.dam.m03.uf3.BicingStations.logic

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class BicingStationsBcn(val data: Data)

@Serializable
data class Data(@SerialName("stations")val listOfStations: List<Station>)

@Serializable
data class Station
    (@SerialName("station_id") val id: Int, @SerialName("num_bikes_available")val availables: Int,
     @SerialName("num_docks_available")val docks: Int, val status: String,
     @SerialName( "num_bikes_available_types")val availablesTypes: BikeType)

@Serializable
data class BikeType(val mechanical: Int, val ebike: Int)