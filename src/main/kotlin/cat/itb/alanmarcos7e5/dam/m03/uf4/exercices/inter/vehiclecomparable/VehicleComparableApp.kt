package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.inter.vehiclecomparable

import cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.intro.BicycleModel
import cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.intro.ScooterModel
import cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.intro.Vehicle
import cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.intro.VehicleBrand

/**
 * L'empresa que fa bicicletes i patinets, ja té molt d'estoc.
 * Vol poder ordenar els models per nom.
 */

fun main() {
    val brand = VehicleBrand("AFX","España")
    val scooter = ScooterModel("Afx",brand,2.0)
    val bicycle = BicycleModel("SEGUNDO",brand,3)
    val listOfVehicles = mutableListOf<Vehicle>(bicycle,scooter)
    println(listOfVehicles)
    listOfVehicles.sort()
    println(listOfVehicles)
}