package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * L'usuari introduirà enters. Enmagatzema en una llista la primera vegada que l'usuari entri un valor.
 * Quan introdueixi un -1 és que ja ha acabat.

 * Input
 *      1 3 9 3 5 1 9 2 -1
 * Output
 *      [1, 3, 9, 5, 2]
 */

fun main() {
    val scanner = Scanner(System.`in`)
    var inputNumber = scanner.nextInt()
    val listNumbers = mutableListOf<Int>()

    while (inputNumber != -1) {
        if (inputNumber !in listNumbers) listNumbers.add(inputNumber)
        inputNumber = scanner.nextInt()
    }
    println(listNumbers)
}