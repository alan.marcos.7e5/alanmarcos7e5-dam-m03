package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.inter.bcntrees

import cat.itb.alanmarcos7e5.dam.m03.uf3.BcnTree

interface DataBcnTree {
    suspend fun listTrees(): List<BcnTree>

    fun countOfSpecie(listOfTrees: List<BcnTree>, specie: String): Int{
        var count = 0
        for (tree in listOfTrees) {
            if (tree.scientificName == specie) count++
        }
        return count
    }
}