package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.inter.translator

interface ITranslator {
    var translatedText: String

    fun changeUserInput(userInput: String): String {
        val userList = userInput.trimIndent().split("\n")
        val translatedUserList = mutableListOf<String>()
        for (i in userList.indices) {
            translatedUserList += translate(userList[i])
        }
        return makeText(translatedUserList)
    }

    fun makeText(translatedUserList: MutableList<String>): String {
        var textFromList = ""
        for (element in translatedUserList) textFromList += "${element}\n"
        return textFromList
    }

    fun translate(line: String): String
}