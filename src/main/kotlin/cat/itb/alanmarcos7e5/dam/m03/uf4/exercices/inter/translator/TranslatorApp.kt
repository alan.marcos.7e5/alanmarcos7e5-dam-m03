package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.inter.translator

import java.util.*

/**
 * Volem fer un programa per la traducció simultània.
 * L'usuari introdurià text per pantalla, i al finalitzar la línia imprimirem la versió traduida.
 * Degut al gran nombre de empreses que ofereixen serveis de traducció volem que
 * ens sigui molt senzill de passar d'una a l'alte.
 * A més a més, aquesta part encara no està implementada.
 * Fes un programa que llegeixi linies per pantalla i les imprimeixi traudides.
 * Per ara, el traductor retornarà el mateix text introduit.
 */
fun main() {
    val toTranslate = """
        Primera línea
        Segunda línea,
        Tercera línea y última.
    """
    val translator = Translator(toTranslate)
    println(translator.translatedText)
}