package cat.itb.alanmarcos7e5.dam.m03.uf1.data

import java.util.*

//Demana dos enters a l'usuari i imprimeix el valor més gran

fun main() {
    val scanner = Scanner(System.`in`)
    val integerA = scanner.nextInt()
    val integerB = scanner.nextInt()
    val result : Int
    if (integerA > integerB){
        result = integerA
    } else {
        result = integerB
    }
    println(result)
}