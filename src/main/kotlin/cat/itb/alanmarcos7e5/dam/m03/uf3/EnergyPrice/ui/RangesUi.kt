package cat.itb.alanmarcos7e5.dam.m03.uf3.EnergyPrice.ui

import cat.itb.alanmarcos7e5.dam.m03.uf3.EnergyPrice.logic.EnergyDayPrices
import cat.itb.alanmarcos7e5.dam.m03.uf3.EnergyPrice.logic.HourRange
import java.util.*

/**
 * Interface between logic and screen
 */
data class RangesUi(val scanner: Scanner, val energyDayMap: EnergyDayPrices){

    fun toOutputRange(range: HourRange?, out: String) {
        println("\n----$out----\n")
        if (range != null) {
            println(range.hour)
            println(range.price)
        }else println("Some error ocurred")
    }

    fun cheapest(){
        val cheapestPrice = energyDayMap.findCheapest()
        toOutputRange(cheapestPrice,"Cheapest range of day")
    }
    fun expensive(){
        val expensivePrice = energyDayMap.findExpensive()
        toOutputRange(expensivePrice,"Expensive range of day")
    }

    fun complete(){
        val completePrices = energyDayMap.allRanges()
        toOutputCompleteRange(completePrices)
    }

    private fun toOutputCompleteRange(completePrice: List<HourRange>) {
        println("----Complete ranges of day----\n")
        for (hourRange in completePrice) {
            println("${hourRange.hour} hs: ${hourRange.price} ${hourRange.units}")
        }
    }

    fun byHour() {
        println("Select the range you want\n")
        val selectedRange = scanner.nextLine()
        val priceByHour = energyDayMap.findByHour(selectedRange)
        toOutputRange(priceByHour,"Selected hour")
    }

    fun toFile() {
        val filePath = energyDayMap.creatingPath()
        println("Saving file in ${filePath}\n")
        energyDayMap.saveFile(filePath)
        val mapFromFile = energyDayMap.loadFromFile(filePath)
        println(mapFromFile)
    }

    fun deleteFile() {
        val filePath = energyDayMap.creatingPath()
        val deleteFile = energyDayMap.deleteFile(filePath)
        if (deleteFile) println("Se borró el archivo de $filePath")
        else println("No existía el archivo.")
    }
}
