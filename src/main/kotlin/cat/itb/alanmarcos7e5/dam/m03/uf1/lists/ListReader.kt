package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * Crea una funció ListReader i defineix la funció següent, que llegeix una llista d'enters de l'usuari.
 * L'usuari primer entrarà el número d'enters a introduïr i després els diferents enters.
 * Nota: a l'exercici ListSortedValues ja has fet aquesta funció.
 */

fun readDoubleList(scanner: Scanner): List<Double> {
    print("Input size of list: ")
    val listSize = scanner.nextInt()
    print("Input values of the list: ")
    return List(listSize) { scanner.nextDouble() }
}

fun readIntList(scanner: Scanner): List<Int> {
    print("Input size of list: ")
    val listSize = scanner.nextInt()
    print("Input values of the list: ")
    return List(listSize) { scanner.nextInt() }
}

fun readStringList(scanner: Scanner): List<String> {
    val listSize = scanner.nextInt()
    return List(listSize) {scanner.next()}
}

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.US)
    val myList = readIntList(scanner)
    println(myList)
}

