package cat.itb.alanmarcos7e5.dam.m03.uf1.nuls

import java.util.*

/**
 * Fes un programa que donats dos números enters imprimeixi el resultat de la seva divisió.
 * Si la divisió no és possible ha d'imprimir null
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val toDivide = scanner.nextInt()
    val divider = scanner.nextInt()
    var result : Int? = null
    if (divider != 0) result = toDivide/divider
    println(result ?: "Math error divide by 0")
}