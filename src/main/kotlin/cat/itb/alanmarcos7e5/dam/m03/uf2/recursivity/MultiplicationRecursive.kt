package cat.itb.alanmarcos7e5.dam.m03.uf2.recursivity

import java.util.*

/**
 * Calcula el producte de dos naturals mitjançant la suma i crides recursives.
 *      Input
 *      5 3
 *      Output
 *      15
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val value1 = scanner.nextInt()
    val value2 = scanner.nextInt()
    println(mult(value1,value2))
}
fun mult(a: Int, b: Int): Int{
    return if (b == 0) 0
    else a + mult(a,b-1)
}
