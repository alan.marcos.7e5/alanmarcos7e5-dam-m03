package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.instrumentsimulator

class Drump(val touch: String): Instrument() {
    override fun sound() {
        val repeatedTouch = touch.repeat(3)
        print("T${repeatedTouch}M ")
    }
}