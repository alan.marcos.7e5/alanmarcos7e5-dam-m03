package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import cat.itb.alanmarcos7e5.dam.m03.uf1.data.project.scanner
import java.util.*

/**
 * Donada una llista de països amb la seva població, volem imprimir els
 * noms dels països amb més població d'un numero indicat per l'usuari.
 * L'usuari primer introduirà el nombre de països i la informació de cada país (nom i població).
 * Finalment introduirà el mínim de població desitjada.
 * Imprimeix els noms dels països amb més població que la introduida.
 */

data class Country(val name:String, val population: Int)

fun main() {
    val scanner = Scanner(System.`in`)
    val countries = List(scanner.nextInt()) {
        scanner.nextLine()
        Country(scanner.nextLine(), scanner.nextInt())
    }
    scanner.nextLine()
    //println("Introduce min value of population:")
    val minPopulation = scanner.nextInt()
    for (element in countries) {
        if (element.population >= minPopulation) println("${element.name} has ${element.population}")
    }
}