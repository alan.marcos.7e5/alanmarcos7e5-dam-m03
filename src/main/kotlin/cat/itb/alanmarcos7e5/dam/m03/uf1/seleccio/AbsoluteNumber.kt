package cat.itb.alanmarcos7e5.dam.m03.uf1.seleccio

import java.util.*
import kotlin.math.abs

//Printa el valor absolut d'un enter entrat per l'usuari.

fun main() {
    val scanner = Scanner(System.`in`)
    var inputUser = scanner.nextInt()
    if (inputUser < 0){
        inputUser *= -1
    }
    println(inputUser)

}