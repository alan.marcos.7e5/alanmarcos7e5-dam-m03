package cat.itb.alanmarcos7e5.dam.m03.uf5.exercices.tads.maps

import cat.itb.alanmarcos7e5.dam.m03.uf1.lists.readIntList
import java.util.Scanner

/**
 * Volem tenir registrats els diferents cartells que té una carretera. La carretera fa més del 1000km, i volem registrar
el metre al que hi ha al cartell.
Posteriorment volem poder obtenir el cartell que hi ha a un metre determinat.

L'usuari primer introduirà el número de cartells, i per cada cartell el metre i el text.
Després introduirà el nombre de consultes i, per cada consulta un metre. S'ha d'imprimir el cartell o no hi ha cartell.

Input
3
103 Lorem
164829 Ipsum
36837 Dolor
3
97362
164829
103
Output
no hi ha cartell
Ipsum
Lorem
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val mapOfSign = createMap(scanner)
    println(mapOfSign)
    readSign(scanner, mapOfSign)
}

fun readSign(scanner: Scanner, mapOfSign: Map<Int, String>) {
    val listOfMeters = readIntList(scanner)
    for (meter in listOfMeters) {
        if (meter in mapOfSign) println(mapOfSign[meter])
        else println("no road sign at specified meter")
    }
}

fun createMap(scanner: Scanner): Map<Int,String>{
    val mapOfSign = mutableMapOf<Int,String>()
    val size = scanner.nextLine().toInt()
    repeat(size){
        mapOfSign += Pair(scanner.nextInt(), scanner.nextLine())
    }
    return mapOfSign
}