package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.carpentryshop

class WoodStrip(price: Double, width: Double) : WoodProduct(price, width) {
    override val totalPrice: Double
        get() = this.width * this.price
}