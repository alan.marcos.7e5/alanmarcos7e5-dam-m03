package cat.itb.alanmarcos7e5.dam.m03.uf2.recursivity

import cat.itb.alanmarcos7e5.dam.m03.uf1.lists.readIntList
import java.util.*

/**
 * L'usuari introduirà 1 array d'enters, com s'indica al mètode ListReader.
 * Un cop llegits tots, printa per pantalla el valor més gran.
 * Usa una funció recursiva per calcular el màxim.
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val list = readIntList(scanner)
    println(searchMaxRecursive(list))
}

fun searchMaxRecursive(list: List<Int>, previousMax: Int = list[0], i: Int = 0): Int {
    var max = previousMax
    val number = list[i]
    if (max < number) max = number
    return if (i == list.lastIndex) max
    else searchMaxRecursive(list,max,i+1)
}
