package cat.itb.alanmarcos7e5.dam.m03.uf1.iterative

import java.util.*

/**
 *  Ens han demanat que fem un programa per un torneig de Go. Necessitem fer un programa que donada
 *  una llista de puntuacions, ens digui qui és el guanyador del torneig.
 *  L'usuari introduirà el nom del participant i la puntuació, cada un en una línia.
 *  Quan ja no hi hagi més participants entrara la paraula clau END.
 *  Imprimeix per pantalla el guanyador del concurs i els punts obtinguts.
 */

fun main() {
    //Primer ingreso del usuario
    val scanner = Scanner(System.`in`)
    tournamentSimple(scanner)
    var participantData = scanner.nextLine()
    //Logica de programa complejo
    val mapOfPlayers : MutableMap<String,Int> = mutableMapOf() // inicializo map vacío //quizas cambiar a var
    while (!participantData.equals("END")){
        addToMap(participantData, mapOfPlayers)
        participantData = scanner.nextLine()
    }
    mapOfPlayers.remove("END")
    println(mapOfPlayers)
    val winnerScore = mapOfPlayers.values.maxOrNull()
    val winnerName = findKeyOfMaxValue(mapOfPlayers,winnerScore)
    println("El ganador es $winnerName con $winnerScore puntos!")
}

//AGREGA LA LINEA COMO K,V EN EL MAP
fun addToMap(participantData: String, mapOfPlayers: MutableMap<String, Int>) {
    var buildName = ""
    var buildScore = ""
    for (character in participantData) {
        if (character.isDigit()) buildScore += character //si es dígito lo voy sumando al Score
        else buildName += character //si no es dígito lo voy sumando al Name
    }
    val buildNameStr = buildName.trim()
    val buildNameInt = buildScore.toInt()
    mapOfPlayers[buildNameStr] = buildNameInt
}

//EJECUTA LA BÚSQUEDA DE LA K DEL V MAX
fun findKeyOfMaxValue(mapToAnalyse : MutableMap<String,Int>, maxValueOfMap : Int?) : String{
    var mapMaxKey : String? = null
    //itero sobre las Keys y comparo sus valores con el maxValueOfMap obtenido anteriormente
    for (name in mapToAnalyse.keys){
        mapMaxKey = if (mapToAnalyse[name] == maxValueOfMap) name
        else continue
    }
    return "$mapMaxKey"

}

//EJECUTA EL MODO SENCILLO DEL PROGRAMA
fun tournamentSimple(scanner : Scanner) {
    var winnerScore = 0
    var winnerName = ""
    var name : String
    var score = 0
    do {
        name = scanner.nextLine()
        if (name != "END") score = scanner.nextLine().toInt() //Intellijei me recomendó cambiar equals por !=

        if (score > winnerScore){
            winnerScore = score
            winnerName = name
        }

    }while (name!="END")


    println("Modo Simple --> Ganador $winnerName puntaje $winnerScore")
}