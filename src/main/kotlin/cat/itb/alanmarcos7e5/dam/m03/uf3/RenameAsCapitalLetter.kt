package cat.itb.alanmarcos7e5.dam.m03.uf3

import java.io.File
import java.nio.file.Path
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.exists
import kotlin.io.path.listDirectoryEntries

/**
 * Demana a l'usuari un path.
 * Renombra tots els fitxers que hi ha a dins per tal que els noms
 * estiguin amb lletres majuscules.
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val userInput = scanner.nextLine()
    val path = Path(userInput)
    if (path.exists()) renameFiles(path) else println("Inexistent path")
}

fun renameFiles(path: Path) {
    val file = path.toFile()
    file.walk().forEach {
        if (it.isFile){
            val parentFile = it.parent.toString()
            val nameFile = it.name.toString()
            val newNameFile = parentFile+"/"+nameFile.uppercase()
            it.renameTo(File(newNameFile))
        }
    }
}
