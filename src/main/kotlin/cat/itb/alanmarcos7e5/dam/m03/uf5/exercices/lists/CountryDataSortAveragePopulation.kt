package cat.itb.alanmarcos7e5.dam.m03.uf5.exercices.lists

import java.util.Scanner

/**
 * Imprimeix la mitjana de població dels països de menys de 1200000 km2.
 */
fun main() {
    val listOfCountries = readCountries()
    println(listOfCountries.filter { it.area <= 1200000 }.map { it.area * it.density }.average()) //opcion sin modificar data class
    println(listOfCountries.filter { it.area <= 1200000 }.map { it.population }.average()) //opcion agregando el get en data class
}

fun readCountries(scanner: Scanner = Scanner(System.`in`)): List<Country> {
    val size = scanner.nextLine().toInt()
    val listOfCountries = List(size){
        Country(scanner.next(), scanner.next(), scanner.next().toInt(), scanner.next().toInt())
    }
    return listOfCountries
}