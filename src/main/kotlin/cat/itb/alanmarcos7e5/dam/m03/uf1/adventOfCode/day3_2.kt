package cat.itb.alanmarcos7e5.dam.m03.uf1.adventOfCode

import java.util.*
import kotlin.io.path.Path

/**
 * Day 3. Binary diagnostic
 */

fun main() {
    val scanner = Scanner(Path("data/aventofcode/inputDay3.txt"))
    val inputList = mutableListOf<String>()
    while (scanner.hasNext()){
        val line = scanner.nextLine()
        inputList.add(line)
    }

    val totalyResult = getBinaryO2(inputList) * getBinaryCO2(inputList)
    println(totalyResult)
}

fun getBinaryO2(inputList: MutableList<String>) : Int{
    var inputListO2 = inputList
    val valueToGetO2 = MutableList(12) { 0 }
    val zeroListO2 = MutableList(12) { 0 }
    val oneListO2 = MutableList(12) { 0 }

    var gralIndexO2 = 0

    while (inputListO2.size > 1) {
        for (e in inputListO2) {
            if (iterativeEachCharacter(e, gralIndexO2)) zeroListO2[gralIndexO2] += 1
            else oneListO2[gralIndexO2] += 1
        }
        valueToGetO2[gralIndexO2] = mostCommonO2(zeroListO2, oneListO2, gralIndexO2) //the value that mind

        val provisionalList = mutableListOf<String>()
        for ((i, e) in inputListO2.withIndex()) {
            //to remove the binaries sequences which no has the value in the
            //specified index
            if (e[gralIndexO2].toString().toInt() == valueToGetO2[gralIndexO2]) provisionalList += e
        }
        inputListO2 = provisionalList
        gralIndexO2++
    }
    println(inputListO2)

    return finalConvert(inputListO2)
}

fun getBinaryCO2(inputList: MutableList<String>) : Int{
    var inputListCO2 = inputList
    val valueToGetCO2 = MutableList(12) { 0 }
    val zeroListCO2 = MutableList(12) { 0 }
    val oneListCO2 = MutableList(12) { 0 }

    var gralIndexCO2 = 0

    while (inputListCO2.size > 1) {
        for (e in inputListCO2) {
            if (iterativeEachCharacter(e, gralIndexCO2)) zeroListCO2[gralIndexCO2] += 1
            else oneListCO2[gralIndexCO2] += 1
        }
        valueToGetCO2[gralIndexCO2] = mostCommonCO2(zeroListCO2, oneListCO2, gralIndexCO2) //the value that mind
        if (valueToGetCO2[gralIndexCO2] == 1) valueToGetCO2[gralIndexCO2] = 0
        else valueToGetCO2[gralIndexCO2] = 1

        val provisionalListCO2 = mutableListOf<String>()
        for ((i, e) in inputListCO2.withIndex()) {
            //to remove the binaries sequences which no has the value in the
            //specified index
            if (e[gralIndexCO2].toString().toInt() == valueToGetCO2[gralIndexCO2]) provisionalListCO2 += e
        }
        inputListCO2 = provisionalListCO2
        gralIndexCO2++
    }
    println(inputListCO2)
    return finalConvert(inputListCO2)
}

fun iterativeEachCharacter(binaryNumber : String, index : Int) : Boolean{
    //iters for each character of a binary number, but break in the specified index
    var incrementZero = false
    for ((i,c) in binaryNumber.withIndex()) {
        if (i == index){
            if (c == '0') {
                incrementZero = true
                break
            }
        }else continue
    }
    return incrementZero
}

fun mostCommonO2(zeroList: List<Int>, oneList: List<Int>, index: Int) : Int {
    //get the common value for each bit
    var valueCommonForIndex = 0
    for ((i,e) in zeroList.withIndex()) {
        if (i == index){
            valueCommonForIndex = if (e > oneList[i]) 0
            else 1
            break
        }else continue
    }
    return valueCommonForIndex
}

fun mostCommonCO2(zeroList: List<Int>, oneList: List<Int>, index: Int) : Int {
    //get the common value for each bit
    var valueCommonForIndex = 0
    for ((i,e) in zeroList.withIndex()) {
        if (i == index){
            valueCommonForIndex = if (e > oneList[i]) 0
            else 1
            break
        }else continue
    }
    return valueCommonForIndex
}

fun finalConvert(list: List<String>): Int {
    val finalList = mutableListOf<Int>()
    for (element in list) {
        for (c in element){
            finalList.add(c.toString().toInt())
        }

    }
    return binaryToDecimal(finalList)
}
