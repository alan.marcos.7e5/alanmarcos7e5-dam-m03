package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * Printa per pantalla ordenats si la llista de N valors introduïts per l'usuari estan ordenats.
 * L'usuari primer entrarà el número d'enters a introduir i després els diferents enters.
 * Input
 *      5
 *      1 25 25 81 90
 *      5
 *      1 25 24 81 90
 * Output
 *      ordenats
 *      desordenats
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val userSizeNumber = scanner.nextInt()
    val userList = List(userSizeNumber){scanner.nextInt()}
    var ordered = true
    var minim = userList[0]
    for (element in userList) {
        if (element < minim) {
            ordered = false
            break
        }else
            minim = element
    }
    if (ordered)
        println("ordenats")
    else
        println("desordenats")
}