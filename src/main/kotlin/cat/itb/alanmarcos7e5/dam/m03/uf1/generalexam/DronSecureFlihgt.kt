package cat.itb.alanmarcos7e5.dam.m03.uf1.generalexam

/**
 * Volem fer volar el nostre nou dron per una zona segura
Des d'una zona molt elevada el dron ha captat una imatge de l'àrea que volem analitzar si és segura, busquem un punt que no estigui cobert d'arbres.
A partir de l'anàlisis de la imatge hem obtingut una matriu de 10x11 (10 columnes, 11 files) que indica si hi ha un arbres 🌳 o no 🟩.
Considerem una zona segura una àrea de 3X3 lliure i en mostrarem el punt central

La zona groga és la zona segura, el punt vermell indica el centre de la zona segura (5,7). Tenint en compte que la casella superior esquerra és la 0,0 i la superior dreta és la 9,0.
Input
🟩 🟩 🌳 🟩 🌳 🟩 🟩 🟩 🌳 🌳
🟩 🌳 🟩 🌳 🟩 🟩 🟩 🌳 🌳 🟩
🟩 🟩 🟩 🟩 🌳 🟩 🌳 🟩 🟩 🟩
🌳 🌳 🌳 🌳 🌳 🟩 🟩 🟩 🌳 🌳
🟩 🟩 🌳 🟩 🌳 🌳 🌳 🌳 🌳 🌳
🟩 🟩 🌳 🟩 🌳 🟩 🟩 🟩 🌳 🌳
🌳 🟩 🟩 🌳 🟩 🟩 🟩 🌳 🌳 🟩
🟩 🌳 🌳 🌳 🟩 🟩 🟩 🌳 🟩 🟩
🟩 🌳 🟩 🌳 🟩 🟩 🟩 🌳 🌳 🟩
🟩 🟩 🌳 🟩 🌳 🟩 🟩 🟩 🌳 🌳
🟩 🌳 🌳 🟩 🟩 🌳 🟩 🌳 🟩 🟩
Output
5 7
 */