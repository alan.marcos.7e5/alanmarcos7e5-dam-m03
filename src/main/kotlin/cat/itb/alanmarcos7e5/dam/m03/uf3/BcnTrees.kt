package cat.itb.alanmarcos7e5.dam.m03.uf3

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.util.*

/**
 * [{"latitud": "41.4389859", "nom_cientific": "Chamaerops humilis", "codi": "0000002AR",
 * "espai_verd": "Can Ensenya, C.V. (Fabra i Puig 439, Villalba dels Arcs; parterres)",
 * "adreca": "Pg Fabra i Puig, 449", "catalogacio": "", "nom_districte": "NOU BARRIS",
 * "tipus_element": "PALMERA ZONA", "nom_barri": "LA GUINEUETA", "geom": "POINT (430310.29 4587826.297)",
 * "longitud": "2.1658066", "nom_catala": "Margall\u00f3", "x_etrs89": "430310.290",
 * "data_plantacio": null, "codi_barri": "48", "nom_castella": "Palmito", "codi_districte": "08",
 * "tipus_reg": "ASPERSI\u00d3", "categoria_arbrat": "PRIMERA", "cat_especie_id": 1155, "tipus_aigua": null, "y_etrs89": "4587826.297"}
 */

/**
 * El següent fitxer conté la informació dels diferents arbres que hi ha a Barcelona
 * Fes un programa en el que l'usuari introdueixi el nom científici d'un arbre i l'aplicació ens indiqui quans arbres
 * hi ha d'aquest tipus a la ciutat de Barcelona
 * Aquest informació ha estat extreta d'aquesta web:
 * https://opendata-ajuntament.barcelona.cat/resources/bcn/Arbrat/OD_Arbrat_Zona_BCN.json
 * Input
 * Chamaerops humilis
 * Output
 * 218
 */
@Serializable
data class Tree(@SerialName ("nom_cientific")val scientificName: String)

fun main() {
    val scanner = Scanner(System.`in`)
    val userScientificName = scanner.nextLine()
    println(readTreesFile(userScientificName))
}
fun readTreesFile(userScientificName: String): Int {
    val text = Tree::class.java.getResource("/OD_Arbrat_Zona_BCN.json").readText()
    val serializer = Json { ignoreUnknownKeys = true }
    val listTree = serializer.decodeFromString<List<Tree>>(text)
    var count = 0
    for (tree in listTree) if (tree.scientificName == userScientificName) count++
    return count
}