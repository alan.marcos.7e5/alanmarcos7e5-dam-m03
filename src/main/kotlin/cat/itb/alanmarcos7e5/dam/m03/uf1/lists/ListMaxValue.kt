package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * L'usuari introduirà una llista d'enters, com s'indica al mètode readIntList.
 *Un cop llegits tots, printa per pantalla el valor més gran.
 *El mètode que has implementat forma part de les utilitats de llistes, és a dir és una funció de la classe List.
 * A partir d'ara el pots utilitzar quan el necessitis fent ús de maxOf()
 *Comprova que saps utilitzar aquest mètode.
 */


fun main() {
    val scanner = Scanner(System.`in`)
    val userList = readIntList(scanner)
    println(userList.maxOrNull()!!) // se coloca la excalmación para aceptar valores nulos CREO
}