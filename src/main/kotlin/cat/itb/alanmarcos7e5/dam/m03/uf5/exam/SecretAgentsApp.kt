package cat.itb.alanmarcos7e5.dam.m03.uf5.exam

import java.util.Scanner

/**
 *Vols (1) afegir, (2) renombrar, (3) cercar, (0) sortir?"
 */
data class Agent(val name: String, val surname: String, var alias: String){}

fun main() {
    val scanner = Scanner(System.`in`)
    val secretAgentApp = SecretAgentApp(scanner)
    Ui(scanner,secretAgentApp).start()
}

class Ui(val scanner: Scanner, val secretAgentApp: SecretAgentApp){

    fun start(){
        showOptions()
        var inputUser = scanner.nextLine().toInt()
        while (inputUser != 0) {
            when (inputUser){
                1 -> println( secretAgentApp.createAgent() )
                2 -> secretAgentApp.rename()
                3 -> println( secretAgentApp.searchByAlias() ?: "No existe el agente con ese alias" )
                0 -> break
                else -> break
            }
            showOptions()
            inputUser = scanner.nextLine().toInt()
        }
    }

    private fun showOptions() {
        println("Vols (1) afegir, (2) renombrar, (3) cercar, (0) sortir?")
    }
}
class SecretAgentApp
    (val scanner: Scanner, val agentMap: MutableMap<String,Agent> = mutableMapOf<String,Agent>()){
    //TODO me falto hacer las verificaciones de Regex
    fun createAgent(): String {
        val inputAlias = scanner.nextLine()
        return if(verifyAlias(inputAlias)){
            val name = scanner.nextLine()
            val surname = scanner.nextLine()
            val agent = Agent(name,surname,inputAlias)
            agentMap[inputAlias] = agent
            "Creado"
        }else "EL alias ya existe"
    }

    private fun verifyAlias(alias: String) = alias !in agentMap.keys

    fun rename(){ //TODO me falto hacer las comprobaciones del nuevo alias
        val oldAlias = scanner.nextLine()
        if (!verifyAlias(oldAlias)) {
            val newAlias = scanner.nextLine()
            val agent = agentMap[oldAlias]
            agent?.alias = newAlias
            agentMap.remove(oldAlias)
            agentMap[newAlias] = agent!!
        }

    }
    fun searchByAlias(): Agent?{
        val alias = scanner.nextLine()
        return if (!verifyAlias(alias)) agentMap[alias]
        else null
    }

}
