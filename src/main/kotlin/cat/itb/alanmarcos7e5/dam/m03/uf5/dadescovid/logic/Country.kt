package cat.itb.alanmarcos7e5.dam.m03.uf5.dadescovid.logic

import kotlinx.serialization.Serializable
import kotlinx.serialization.SerialName

/**
Contains the info of a country
 */
@Serializable
class Country
    (@SerialName("Country") val name: String, @SerialName("CountryCode") val code: String,
     @SerialName("NewConfirmed")val newConfirmed: Int, @SerialName("TotalConfirmed")val totalConfirmed: Long,
     @SerialName("NewDeaths")val newDeaths: Int, @SerialName("TotalDeaths")val totalDeaths: Int,
     @SerialName("NewRecovered")val newRecovered: Int, @SerialName("TotalRecovered")val totalRecovered: Int){

    var population: Long? = null
    val rate get() = obtainRate()

    private fun obtainRate(): Double? {
        return if (this.population != null) (totalDeaths/population!!.toDouble()) * 100
        else null
    }

    fun getPopulation(listOfPopulation: List<Population>){
        listOfPopulation.forEach {
            if (it.code == this.code) this.population = it.population
        }
    }
}

/**
 * Contains code of country and population
 */
@Serializable
data class Population (var code: String, val population: Long){

    fun changeCodeToShort(listOfCodes: List<Codes>){
        listOfCodes.forEach {
            if (it.longCode == this.code) this.code = it.shortCode
        }
    }
}

/**
 * Contains 2 words code of country and 3 words code of country
 */
@Serializable
data class Codes(@SerialName("short")val shortCode: String, @SerialName("long")val longCode: String)