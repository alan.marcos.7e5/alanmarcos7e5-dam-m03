package cat.itb.alanmarcos7e5.dam.m03.uf5.exercices.lists

import java.lang.Math.abs
import java.util.*

/**
 * Llegeix una llista d'enters.
Imprimeix per pantalla el número més pròxim a 100, i el més llunyà.

Input
971 6 4878 1354 102 302154 -2
Output
Més proper a 100: 102
Més llunyà a 100: 302154
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val listInput = List(7){scanner.nextInt()}
    println(listInput)
    var nearNum: Int? = null
    var farNum: Int? = null
    var nearDist = Int.MAX_VALUE
    var farDist = 0
    listInput.forEach {
        val distance = abs(it-100)
        if (distance > farDist){
            farDist = distance
            farNum = it
        }
        if (distance < nearDist){
            nearDist = distance
            nearNum = it
        }
    }
    println(farNum)
    println(farDist)
    println(nearNum)
    println(nearDist)
}