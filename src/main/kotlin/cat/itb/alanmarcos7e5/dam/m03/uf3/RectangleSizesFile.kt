package cat.itb.alanmarcos7e5.dam.m03.uf3

import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlin.io.path.Path
import kotlin.io.path.readText


/**
 * Volem fer un programa que ens digui l'area de rectangles enmagatzemats
 * en un fitxer json guardat a dins de la carpeta resources.
 *Et pots descarregar el fitxer aquí.
 *Imprimeix la superfície dels diferents rectangles.
 *Output
 *Un rectangle de 98 x 85 té 8330 d'area.
 *Un rectangle de 71 x 3 té 213 d'area.
(...)
 */
//plugin de serializable en gradle de proyecto
//cargar la dependencia de JSON en el gradle de proyecto
@Serializable
data class RectanglesSizes(val width: Double, val height: Double, val color: Color)

@Serializable
data class Color(val name: String)

fun main() {
    //esta linea puedo usarla para obtener de una carpeta dentro del proyecto, solamente podré leer
    val text = RectanglesSizes::class.java.getResource("/rectangles.json").readText()
    //otra manera sería leer el path con el método readText que tiene la clase Path:
    //val text = Path("src/main/resources/rectangles.json").readText()
    println(text) // es el contenido del fichero, lo leyó con la línea anterior y lo tenemos en String.
    val rectangle = Json.decodeFromString<List<RectanglesSizes>>(text)
    println(rectangle)
}
