package cat.itb.alanmarcos7e5.dam.m03.uf5.exercices.tads.maps

import java.util.Scanner

/**
 * Una empresa vol tenir enregistrada la informació els seus empleats. De cada empleat en vol tenir el seu
 * dni, nom, cognoms i adreça. Vol poder accedir ràpidament a les dades dels empleats segons el seu DNI.

Fes un programa que llegeixi un conjunt d'empleats per pantalla (primer introduirà la quantitat d'empleats).
Després imprimeix les dades del empleats pel DNI entrat.
Acaba el programa quan introdueixi END.

Input
2
12345678Z
Mar
Puig
Av. Pi 42
87654321T
Ot
Pi
C. Mar 33
12345678Z
87654321T
87654321T
END
Output
Mar Puig - 12345678Z, Av. Pi 42
Ot Pi - 87654321T, C. Mar 33
Ot Pi - 87654321T, C. Mar 33

 */

class Employee(val dni: String, val name: String, val surname: String, val address: String){
    override fun toString(): String {
        return "$name $surname - $dni, $address"
    }
}

fun main() {
    val scanner = Scanner(System.`in`)
    val listOfEmployees = readEmployees(scanner)
    val listOfDniInput = readDniInputs(scanner)
    val mapEmployees = listOfEmployees.associateBy { it.dni }
    printEmployees(listOfDniInput, mapEmployees)
}

fun printEmployees(listOfDni: List<String>, mapEmployees: Map<String, Employee>) {
    for (dni in listOfDni){
        val employee = mapEmployees[dni] ?: "No such employee with this dni"
        println(employee)
    }
}

fun readDniInputs(scanner: Scanner): List<String> {
    val listOfDni = mutableListOf<String>()
    var inputDni = scanner.nextLine()
    while (inputDni.uppercase() != "END") {
        listOfDni += inputDni
        inputDni = scanner.nextLine()
    }
    return listOfDni
}

fun readEmployees(scanner: Scanner): List<Employee> {
    val size = scanner.nextLine().toInt()
    val listOfEmployees = List(size){
        Employee(scanner.nextLine(), scanner.nextLine(), scanner.nextLine(), scanner.nextLine())
    }
    return listOfEmployees
}
