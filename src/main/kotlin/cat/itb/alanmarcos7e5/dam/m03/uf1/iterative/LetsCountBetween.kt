package cat.itb.alanmarcos7e5.dam.m03.uf1.iterative

import java.util.*

//L'usuari introdueix dos valors enters.
//Printa per pantalla tots els valors que hi ha entre els dos valors introduits ordenats de menor a major


fun main() {
    val scanner = Scanner(System.`in`)
    val startNUmber = scanner.nextInt()
    val endNumber = scanner.nextInt()
    for (values in startNUmber+1 until endNumber) //siempre debe ser más pequeño a más grande, sino usar downTo
        print(values)
}