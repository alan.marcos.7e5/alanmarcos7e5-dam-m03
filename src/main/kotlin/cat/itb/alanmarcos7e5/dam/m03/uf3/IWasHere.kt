package cat.itb.alanmarcos7e5.dam.m03.uf3

import java.nio.file.Path
import java.nio.file.StandardOpenOption
import java.time.LocalDateTime
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.createFile
import kotlin.io.path.exists
import kotlin.io.path.writeText

/**
 * Fes un programa que cada cop que s'executi afegeixi una linia al fitxer
 * ~/i_was_here.txt amb la següent linia.
 * I Was Here: 2021-02-16T09:59:04.343769
 */

fun main() {
    val homePathString = System.getProperty("user.home")
    val dateTime = LocalDateTime.now().toString()
    val homePath = Path(homePathString)
    val iWasHereFile = homePath.resolve("IwasHere.txt")

    if (!iWasHereFile.exists()) iWasHereFile.createFile()
    addLine(iWasHereFile,dateTime)
}

fun addLine(fileToWrite: Path, dateTime: String) {
    val lineToAdd = "I was here: $dateTime"
    fileToWrite.writeText(lineToAdd+"\n", options = arrayOf(StandardOpenOption.APPEND))
}
