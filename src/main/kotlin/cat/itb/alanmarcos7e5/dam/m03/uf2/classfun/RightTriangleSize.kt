package cat.itb.alanmarcos7e5.dam.m03.uf2.classfun

import java.util.*
import kotlin.math.pow
import kotlin.math.roundToInt
import kotlin.math.sqrt

/**
 * Volem fer un programa que ens digui l'area i el perímetre d'una llista de triangles rectangles
 * L'usuari indrodueix el número de triangles
 * L'usuari entra la base i l'altura del triangle
 * Imprimeix la superfície i el perímetre dels diferents triangles.
 * Nota: No pots imprimir els triangles fins a que s'hagin llegit tots.
 * Input
 * 2
 * 2.0 4.0
 * 3.5 5.4
 * Output
 * Un triangle de 2.0 x 4.0 té 4.0 d'area i XX de perímetre.
 * Un triangle de 3.5 x 5.4 té 9.45 d'area i XX de perímetre.
 */

data class Triangulo(var base: Double, var height: Double){
    val area get() = (base*height)/2
    private val hipotenusa = sqrt(base.pow(2.0) + height.pow(2.0))
    val perimeter get() = hipotenusa + height + base
    fun toOutput()="Un triángulo de $base * $height tiene $area de área y $perimeter de perímetro."
}


fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val listSize = scanner.nextInt()
    //scanner.nextLine()
    val trianglesList = mutableListOf<Triangulo>()
    while (trianglesList.size < listSize) {
        val triangulo = Triangulo(scanner.nextDouble(), scanner.nextDouble())
        trianglesList += triangulo
    }
    for (e in trianglesList) println(e.toOutput())
}