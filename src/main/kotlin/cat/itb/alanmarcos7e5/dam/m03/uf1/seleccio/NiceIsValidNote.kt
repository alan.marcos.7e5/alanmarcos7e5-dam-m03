package cat.itb.alanmarcos7e5.dam.m03.uf1.seleccio

import java.util.*

//L'usuari escriu un enter
//imprimeix bitllet vàlid si existeix un bitllet d'euros amb
// la quantitat entrada, bitllet invàlid en qualsevol altre cas.

fun main() {
    val scanner = Scanner(System.`in`)
    val inputUser = scanner.nextInt()
    var outputAnswer : String
    if (inputUser == 5 || inputUser == 10 || inputUser == 20 ||
        inputUser == 50 || inputUser == 100){
        outputAnswer = "bitllet vàlid"
    }else{
        outputAnswer = "bitllet invàlid"
    }
    println(outputAnswer)
}