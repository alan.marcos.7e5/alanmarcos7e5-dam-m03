package cat.itb.alanmarcos7e5.dam.m03.uf1.iterative

import java.util.*

/**
 *   Volem contar quantes línies té un text introduït per l'usuari.
 *
 *   L'usuari introduiex un text per pantalla que pot tenir salts de línia.
 *   Per indicar que ha acabat d'introduïr el text escriurà una línia amb la paraula clau END
 *   Imprimeix el nombre de línies que ha introduït l'usuari (sense contar la END)
 *   Recorda que per comparar dos Strings no pots usar == i has d'usar

 */


fun main() {
    val scanner = Scanner(System.`in`)
    var inputUser = scanner.nextLine()
    var line = 0
    while (!inputUser.equals("END")){
        inputUser = scanner.nextLine()
        line++
    }
    println(line)
}