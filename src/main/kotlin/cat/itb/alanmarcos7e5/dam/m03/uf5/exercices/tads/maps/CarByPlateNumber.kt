package cat.itb.alanmarcos7e5.dam.m03.uf5.exercices.tads.maps

import java.util.Scanner

/**
Un concesionari de contxes de segona ma vol tenir la informació dels cotxes que té en estoc.
De cada cotxe en vol guardar la matrícula, el model i el color.

Vol poder accedir rapidament a la informació del cotxe usant la matrícula del cotxe.

Input
3
2322UUY Opel Blanc
4738URP Seat Groc
3798YHT Opel Verd
4738URP
3798YHT
END
Output
4738URP Seat Groc
3798YHT Opel Verd
 */
data class Car (val plate: String, val model: String, val color: String)

fun main() {
    val scanner = Scanner(System.`in`)
    val listOfCars = readCars(scanner)
    val listOfPlates = readPlates(scanner)
    val mapOfCars = listOfCars.associateBy { it.plate }
    printCars(mapOfCars, listOfPlates)
}

fun printCars(mapOfCars: Map<String, Car>, listOfPlates: List<String>) {
    for (plate in listOfPlates) {
        val car = mapOfCars[plate]
        val text = car ?: "No such car with this plate"
        if (car != null) println("${car.plate} ${car.model} ${car.color}")
        else println(text)
    }
}

fun readPlates(scanner: Scanner): List<String> {
    var inputPlate = scanner.nextLine()
    val listOfPlates = mutableListOf<String>()
    while (inputPlate.uppercase() != "END") {
        listOfPlates += inputPlate
        inputPlate = scanner.nextLine()
    }
    return listOfPlates
}

fun readCars(scanner: Scanner): List<Car> {
    val size = scanner.nextLine().toInt()
    val listOfCars = List(size){
        Car(scanner.next(), scanner.next(), scanner.next())
    }
    scanner.nextLine()
    return listOfCars
}
