package cat.itb.alanmarcos7e5.dam.m03.uf1.nuls

/**
 * Una agencia d'espionatge antidroga ha pogut aconseguir una llista dels noms de grans caps de la droga que fa temps que estava buscant.
 * El problema és que la llista està en un format una mica incomode de llegir, primer hi ha tots els seus noms,
 * després els seus cognoms i finalment la seva data de naixement.
Fes un petit programa que imprimeixi els noms, cognoms i data de naixement. El primer valor és el nombre de delinqüents

Input
4
Pablo
Joaquín "El Chapo"
Osiel
Miguel Ángel
Escobar
Guzmán
Cárdenas Guillén
Félix Gallardo
1949
1968
1967
1946
Output
Pablo Escobar 1949
Joaquín "El Chapo" Guzmán 1968
Osiel Cárdenas Guillén 1967
Miguel Ángel Félix Gallardo 1946
 */

//TODO COMPLETAR
