package cat.itb.alanmarcos7e5.dam.m03.uf2.recursivity

import java.util.*

/**
 * Calcula la potència de dos naturals mitjançant el producte
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val base = scanner.nextInt()
    val power = scanner.nextInt()
    println(calculatePower(base,power))
}

fun calculatePower(base: Int, power: Int): Int {
    if (power == 0) return 1
    return base * calculatePower(base,power-1)
}
