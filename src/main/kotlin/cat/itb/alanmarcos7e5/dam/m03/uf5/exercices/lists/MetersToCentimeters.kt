package cat.itb.alanmarcos7e5.dam.m03.uf5.exercices.lists

import cat.itb.alanmarcos7e5.dam.m03.uf1.lists.readIntList
import java.util.Scanner

/**
 * Llegeix una llista de distàncies per la consola (en format enter, usant IntegerLists.readIntegerList).
 * Converteix cada una de les distàncies a cm i imprimeix-les per pantalla.
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val myList = readIntList(scanner)
    myList.forEach { println(it*1000) } //por cada valor lo imprime multiplicado por 1000, no modifica myList original
    println("****************")
    myList.map { it*1000 }.forEach { println(it) } //crea lista nueva e imprime por cada valor
}