package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.motto

open class Team(val name: String, val shout: String) {
    open fun shoutMotto() {
        println("$name, $shout")
    }
}

class BasketTeam(name: String, shout: String): Team(name, shout){
    override fun shoutMotto() {
        println("1 2 3! $shout")
    }
}

class VoleyTeam(name: String, shout: String, val color: String): Team(name, shout){
    override fun shoutMotto() {
        println("$color $shout")
    }
}

class GolfTeam(name: String, shout: String, val playerName: String): Team(name, shout){
    override fun shoutMotto() {
        println("$playerName $shout")
    }

}