package cat.itb.alanmarcos7e5.dam.m03.uf2.functions

import java.util.*

/**
 * Volem fer un programa que analitzi els resultats d'un partit de bàsquet. Hem de tenir en compte que en un partit de bàsquet es poden fer 1, 2 o 3 punts.
 * Donat els diferents resultats analitza que està passant.
 * L'usuari introduirà primer el nom dels dos equips i després el resultat actual
 * Input
 * Juventut
 * Manresa
 * 0 2
 * 0 5
 * 2 5
 * 3 5
 * 3 7
 * -1
 * Output
 * Cistella de Manresa
 * Triple de Manresa
 * Cistella de Juventut
 * Tir lliure de Juventut
 * Cistella de Manresa
 * Guanaya Manresa
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val team1 = scanner.nextLine()
    val team2 = scanner.nextLine()
    val scoreList = readIntList(scanner)
    println(resumeMatch(scoreList,team1,team2))
    println(winner(scoreList,team1,team2))
}

fun resumeMatch(list: List<Int>, team1: String, team2: String): List<String> {
    var valueTeam1 = 0
    var valueTeam2 = 0
    val resumeList = mutableListOf<String>()
    for (i in list.indices) {
        if (i%2==0){
            val difference = list[i] - valueTeam1
            if (difference != 0) resumeList += "${scores(difference)} $team1"
            valueTeam1 = list[i]
        }else{
            val difference = list[i] - valueTeam2
            if (difference != 0) resumeList += "${scores(difference)} $team2"
            valueTeam2 = list[i]
        }
    }
    return resumeList
}

fun winner(list: List<Int>, team1: String, team2: String): String {
    val finalScoreTeam1 = list[list.lastIndex - 1]
    val finalScoreTeam2 = list[list.lastIndex]
    return if (finalScoreTeam1 == finalScoreTeam2) "Empate"
    else if (finalScoreTeam1 < finalScoreTeam2) "Guanaya $team2"
    else "Guanaya $team1"
}

fun scores(score: Int): String {
    return when (score){
        0 -> ""
        1 -> "Tir lliure de"
        2 -> "Cistella de"
        3 -> "Triple de"
        else -> "error"
    }
}
