package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.autonomouscar

enum class Direction {
    FRONT, LEFT, RIGHT
}