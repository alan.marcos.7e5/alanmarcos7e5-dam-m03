package cat.itb.alanmarcos7e5.dam.m03.uf1.iterative.project

/**
 * Un jugador coloca en secret quatre peces d'algun dels sis colors (es pot repetir el color i posar-los en qualsevol ordre)
 * i l'oponent ha d'esbrinar-los en un nombre limitat de torns.
 * A cada torn, qui intenta endevinar coloca quatre peces
 * de colors i l'altre respon indicant amb fitxes blanques i negres depenent de la combinació introduïda.
 *      En el nostre cas l'usuari és el jugador que intenta endevinar la combinació.
 *      La nostra tasca principal és programar l'algoritme que avalua si la suposició feta pel jugador és correcte o no
 *      Enlloc d'utilitzar colors farem servir la combinació de lletres ABCDEF
 */

import java.util.*

data class Evaluation(val rightPosition: Int, val wrongPosition: Int)
fun main() {
    val scanner = Scanner(System.`in`)
    println("Vols jugar en mode 1vs1 (1) o solitari (2)?")
    val mode = scanner.nextInt()
    scanner.nextLine()

    val slots = 12 //change slots value as you want

    var secret : String = when (mode) {
        1 -> {
            println("Comença el joc!")
            println("Introdueix la paraula secreta")
            scanner.next()
        }
        2 -> {
            println("Comença el joc!")
            randomChar()
        }
        else -> ""
    }
    if (secret.isNotEmpty()) {
        //println("for debug= $secret") //uncomment this line for debug only
        println(logicProgram(scanner,secret,slots))
    }else println("MODO DE JUEGO ERRONEO")
}

fun randomChar(): String {
    val allowedChars = 'A'..'F'
    var randomSecret = ""
    repeat(4){
        randomSecret = "$randomSecret${allowedChars.random()}"
    }
    return randomSecret
}

fun evaluateWord(secret: String, guess: String): Evaluation {
    var rightPosition = 0
    var wrongPosition = 0
    for ((i,letter) in guess.withIndex()) {
        if (letter == secret[i]) {
            rightPosition++
        }else {
            if (letter in secret) wrongPosition++
        }
    }
    return Evaluation(rightPosition,wrongPosition)
}

fun logicProgram(scanner: Scanner, secret: String, slots : Int) : String{
    var output : String
    var insideSlots = slots
    var insideGuess : String
    do{
        //user guess input
        println("Introdueix una combinació")
        insideGuess = scanner.next()

        if (insideGuess != "end") {
            insideSlots --
            var evaluation = evaluateWord(secret, insideGuess)
            println("Posicions correctes: ${evaluation.rightPosition}, " +
                    "Posicions incorrectes: ${evaluation.wrongPosition}")
        } else break
    }while (secret != insideGuess && insideSlots != 0)
    //changing the output
    output = if (secret == insideGuess) "Enhorabona! has guanyat"
    else if (insideGuess == "end") "Has sortit del joc."
    else "Fi del joc. Has perdut!"
    return output
}

