package cat.itb.alanmarcos7e5.dam.m03.uf5.exercices.tads.maps

import java.util.Scanner

/**
 * S'ha de votar el delegat de la classe, i per fer-ho has decidit fer un petit programa
que conti els vots i imprimeixi el resultat.

Cada usuari introduirà el nom de l'estudiant al que vota. Quan ja no hi hagi més vots escriurà END.
Imprimeix els total de vots.

Input
Mar
Mar
Ot
Iu
Ona
Iu
Mar
Mar
END
Output
Mar: 4
Ot: 1
Iu: 2
Ona: 1
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val mapOfPostulates = mutableMapOf<String, Int>()
    readVotes(scanner, mapOfPostulates)
    printVotes(mapOfPostulates)
}

fun printVotes(mapOfPostulates: MutableMap<String, Int>) {
    mapOfPostulates.forEach { println("${it.key}: ${it.value}") }
}

fun readVotes(scanner: Scanner, mapOfPostulates: MutableMap<String, Int>) {
    var inputName = scanner.nextLine()
    while (inputName.uppercase() != "END") {
        if (inputName in mapOfPostulates) mapOfPostulates[inputName] = mapOfPostulates[inputName]!! + 1
        else mapOfPostulates[inputName] = 1
        inputName = scanner.nextLine()
    }

}
