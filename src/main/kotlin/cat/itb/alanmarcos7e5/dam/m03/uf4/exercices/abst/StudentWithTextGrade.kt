package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.abst

/**
 * Volem enmagatzemar d'un estudiant, el seu nom, i la seva nota en format text.
 * Una nota pot ser suspès, aprovat, bé, notable o excel·lent.
 * Fes un programa que crei 2 estudiants amb dues notes diferents i printa'ls per pantalla
 */
enum class Grade{
    SUSPES, APROVAT, BE, NOTABLE, EXCELENT
}
class Student(val name: String, val textGrade: Grade){
    override fun toString(): String {
        return "name: $name / $textGrade"
    }
}

fun main() {
    val gradeStudent1 = Grade.APROVAT
    val student1 = Student("Mar", gradeStudent1)
    val student2 = Student("Lopez", Grade.EXCELENT)
    println(student1)
    println(student2)
}