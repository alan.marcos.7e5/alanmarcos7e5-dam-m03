package cat.itb.alanmarcos7e5.dam.m03.uf5.dadescovid.logic

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/**
 * Contains a list of Countries
 */
@Serializable
class DayData(@SerialName("Date") val date: String,
              @SerialName("Countries") val countryList: List<Country>) {
    override fun toString(): String {
        return this.countryList[0].newConfirmed.toString()
    }

    //Imprimeix els deu països amb més morts totals
    fun top10Deaths() = countryList.sortedByDescending { it.totalDeaths }.take(10)

    //Imprimeix els deu països amb més casos confirmats nous
    fun top10NewConfirmed() = countryList.sortedByDescending { it.newConfirmed }.take(10)

    //Top 10 EU
    fun ueData(): Map<String, Int> {
        val setOfUeCode = setOf<String>(
            "BE", "EL", "LT", "PT", "BG", "ES", "LU", "RO", "CZ", "FR", "HU", "SI",
            "DK", "HR", "MT", "SK", "DE", "IT", "NL", "FI", "EE", "CY", "AT", "SE", "IE", "LV", "PL"
        )
        var totalDeaths = 0
        var newConfirmed = 0
        countryList.forEach {
            if (it.code in setOfUeCode) {
                totalDeaths += it.totalDeaths
                newConfirmed += it.newConfirmed
            }
        }
        return mapOf(Pair("Total Deaths", totalDeaths), Pair("New Confirmed", newConfirmed))
    }

    fun top10PerCapita(listPopulation: List<Population>, listCodes: List<Codes>): List<Country> {
        putRates(listPopulation, listCodes)
        return countryList.sortedByDescending { it.rate }.take(10)
    }

    fun searchCountry(countryName: String): Country? {
        for (country in countryList) {
            if (country.name == countryName) return country
        }
        return null
    }

    private fun putRates(listPopulation: List<Population>, listCodes: List<Codes>, countryName: String? = null) {
        listPopulation.forEach { it.changeCodeToShort(listCodes) }
        countryList.forEach { it.getPopulation(listPopulation) }
    }

    fun searchRateByCountryName(listPopulation: List<Population>, listCodes: List<Codes>, countryName: String)
    : Double? {
        putRates(listPopulation, listCodes)
        val country = searchCountry(countryName) ?: return null
        return country.rate
    }


}