package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.intro.figures

class Piramid(color: String, val base: Int): Figure(color) {
    override fun paintText(){
        repeat(base){
            repeat(it+1){
                val piramidColor = prepareColor()
                print("${piramidColor}X")
            }
            println()
        }
        println()
    }
}