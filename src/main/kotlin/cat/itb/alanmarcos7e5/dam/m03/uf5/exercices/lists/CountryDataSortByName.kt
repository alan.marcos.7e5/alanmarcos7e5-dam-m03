package cat.itb.alanmarcos7e5.dam.m03.uf5.exercices.lists

import java.util.Scanner

/**
 * D'un païs ens en volem guardar la següent informació.
 *
 * nom, capital, superfície(km2), densitat (hab./km2)
 *
 * Imprimeix els països amb una densitat superior a 5hab/km2 i més de 1000km2
 * ordenats per ordre alfabètic segons la seva capital.
 *
 * L'usuari primer introduirà la quantitat de països a llegir.
 */
data class Country(var name: String, var capital: String, val area: Int, val density: Int){
    val population get() = area * density
}
data class Embassator(val name: String, val surname: String, val country: Country?)

fun main() {
    val scanner = Scanner(System.`in`)
    val size = scanner.nextLine().toInt()
    val listOfCountries: List<Country> = List(size){
            Country(scanner.nextLine(), scanner.nextLine(), scanner.nextLine().toInt(), scanner.nextLine().toInt())
    }
    listOfCountries.filter { it.density > 5 && it.area > 1000 }.sortedBy { it.capital }.forEach { println(it.name) }
}