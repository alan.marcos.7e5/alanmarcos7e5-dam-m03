package cat.itb.alanmarcos7e5.dam.m03.uf1.iterative

import java.util.*

//Volem printar les taules de múltiplicar.
//L'usuari introdueix un enter
//Printa per pantalla la taula de multiplicar del número introduit

fun main() {
    //leo datos
    val scanner = Scanner(System.`in`)
    val userNum = scanner.nextInt()
    //logica
    for (i in 0..9) {
        val product = userNum * i
        println("$i * $userNum = $product")
    }
}