package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * En una botiga volem convertir tot de preus sense a IVA al preu amb IVA.
 * Per afegir l'IVA a un preu hem de sumar-hi el 21% del seu valor.
 * L'usuari introduirà el preu de 10 artícles.
 * Imprimeix per pantalla el preu amb l'IVA afegit amb el següent format indicat a continuació.
 * El programa no pot imprimir res fins a que hagi llegit tots els valors.
 * Input
 * 8 4 45 2 48 12 48 55 15 1
 * Output
 * 8.0 IVA = 9.68
 * 4.0 IVA = 4.84
 * 45.0 IVA = 54.45
 * 2.0 IVA = 2.42
 * 48.0 IVA = 58.08
 * 12.0 IVA = 14.52
 * 48.0 IVA = 58.08
 * 55.0 IVA = 66.55
 * 15.0 IVA = 18.15
 * 1.0 IVA = 1.21
 */


fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val pricesList = readDoubleList(scanner)
    println(pricesList)
    val pricesWithIva = mutableListOf<Double>()
    for (price in pricesList) {
        pricesWithIva.add(price * 1.21)
    }
    println(pricesWithIva)
}