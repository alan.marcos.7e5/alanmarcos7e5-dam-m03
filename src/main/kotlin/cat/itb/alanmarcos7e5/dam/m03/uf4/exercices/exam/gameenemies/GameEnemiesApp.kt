package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.exam.gameenemies

fun main() {
    val zombieZog = Zombie("Zog",10,"AARRRrrgg")
    val zombieLili = Zombie("Lili",30,"GRAaaArg")
    val trollJum = Troll("Jum",12,5)
    val goblinTim = Goblin("Tim", 60)
    val enemies = listOf<Enemy>(zombieZog,zombieLili,trollJum,goblinTim)

    enemies[0].attack(5)
    enemies[0].attack(7)
    enemies[3].attack(7)
    enemies[1].attack(3)
    enemies[2].attack(4)
    enemies[2].attack(8)
    enemies[1].attack(4)
    enemies[0].attack(5)
    enemies[0].attack(1)
    enemies[2].attack(1)
    enemies[2].attack(1)
    enemies[2].attack(1)
    println(enemies)
}