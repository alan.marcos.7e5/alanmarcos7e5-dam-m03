package cat.itb.alanmarcos7e5.dam.m03.uf1.seleccio

import java.util.*

/**
 * L'usuari introdueix dos enters, la base i l'exponent.
 * Imprimeix el resultat de la potencia (sense usar la llibreria math).
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val userBase = scanner.nextInt().toDouble()
    val userPow = scanner.nextInt()
    var product = 1.0
    repeat(userPow){
        product *= userBase
        println(product)
    }

}