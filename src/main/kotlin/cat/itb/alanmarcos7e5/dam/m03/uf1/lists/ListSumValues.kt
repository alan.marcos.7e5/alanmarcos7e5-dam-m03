package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * L'usuari introduix una llista de valors tal i com s'indica al mètode readIntList.
 * Imprimeix per pantalla la suma d'aquests valors.
 * Input
 * 3 5 10 12
 * Output
 * 27
 */

fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    val userList = readIntList(scanner)
    println("Seleccione el algoritmo: 1 o 2")
    scanner.nextLine()
    val selMode = scanner.nextInt()
    if (selMode == 1) {
        val sumOfList = userList.sum()
        println(" La suma con función incluida es $sumOfList")
    } else {
        var suma = 0
        for (element in userList) {
            suma += element
        }
        println("La suma con iteración es $suma")
    }
}