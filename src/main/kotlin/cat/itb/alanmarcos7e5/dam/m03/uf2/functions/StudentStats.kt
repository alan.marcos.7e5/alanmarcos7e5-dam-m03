package cat.itb.alanmarcos7e5.dam.m03.uf2.functions

import java.util.*

/**
 * Després de fer un exàmen un professor vol veure l'estat general de la classe. Vol coneixer la nota mínima, la màxima i la mitjana dels seus alumnes.
 * Fes un petit programa que li solucioni la tasca (les notes de l'exàmen no tenen decimals).
 * Input
 * 5 3 4 7 8 -1
 * Output
 * Nota mínima:  3
 * Nota màxima:  8
 * Nota mitjana: 5.4
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val listGrades = readIntList(scanner)
    val minGrade = min(listGrades)
    val maxGrade = max(listGrades)
    val avgGrade = avg(listGrades)
    println("Nota mínima:  $minGrade\nNota màxima:  $maxGrade\nNota mitjana: $avgGrade")
}