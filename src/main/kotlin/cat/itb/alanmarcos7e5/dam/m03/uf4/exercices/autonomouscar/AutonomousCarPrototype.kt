package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.autonomouscar

/**
 * Volem que el nostre sistema funcioni per diferents models i marques de cotxes.
 * Així que en una reunió entre diferents empreses hem acordat una interfície comú:

fun isThereSomethingAt(direction: Direction) : Boolean
fun go(direction : Direction)
fun stop()

Direction pot tenir els valors FRONT, LEFT, RIGHT.
 */
fun main() {
    val autonomousCar = AutonomousCar()
    autonomousCar.doNextNSteps(2)

}