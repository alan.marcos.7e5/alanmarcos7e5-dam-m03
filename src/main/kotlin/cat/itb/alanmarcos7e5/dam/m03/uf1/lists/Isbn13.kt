package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.security.KeyStore
import java.util.*

/**
 * A partir de l'any 2006 els ISBN van passar a tenir 13 dígits i es va adaptar per a què fos compatible amb el sistema de codi de barres EAN-13.
 * Volem verificar si un codi ISBN és correcte. Mitjançant el dígit de control que és el dígit número 13. Es calcula de la manera següent:
 * Es multiplica el primer dígit dels 12 digits
 * per 1, el segon per 3, el tercer per 1, el quart per 3, i així
 * succesivament fins al número 12;
 * El dígito de control és el valor que s'ha d'afegir a la suma de tots aquests productes per fer-la divisible per 10. Si
 * el resultat de la suma ja fos múltiple de 10, el dígito de control seria 0.
 * input: 978-84-92493-70-8
 * output: Codi correcte
 */


fun main() {
    val scanner = Scanner(System.`in`)
    val myIsbn = mutableListOf<Int>()// inicializo lista vacía
    val myIsbnInput = scanner.next()

    //bucle para iterar sobre la secuencia de char ingresada por el usuario
    for (element in myIsbnInput)
        if (element.isDigit())
            myIsbn.add(element.toString().toInt())
    println(myIsbn)

    //bucle para trabajar sobre la lista
    var totalSum = 0
    for ((i,element) in myIsbn.withIndex()){
        if (i == 12) break
        totalSum += if (!isPair(i+1))
            (element * 1)
        else
            (element * 3)
    }

    //corroborando con digito de control
    val addLastDigit = totalSum + myIsbn[myIsbn.lastIndex]
    val residueTotalSum = addLastDigit % 10
    if (residueTotalSum == 0) println("Codi correcte")
    else println("Codi incorrecte")



}

fun isPair(number: Int): Boolean {

    return number % 2 == 0

}