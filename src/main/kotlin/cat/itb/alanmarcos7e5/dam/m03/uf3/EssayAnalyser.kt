package cat.itb.alanmarcos7e5.dam.m03.uf3

import java.nio.file.Path
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.exists
import kotlin.io.path.readLines
import kotlin.io.path.readText

/**
 * Volem fer un programa que, donat un fitxer de text ens indiqui quantes línies té i quantes paraules té.
 * Input
 * /home/user/somefile.txt
 * Output
 * Número de línies: 56
 * Número de paraules: 524
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val userInput = scanner.nextLine()
    val path = Path(userInput)
    if (path.exists()){
        println("Líneas: ${path.readLines().size}\n")
        val fileContain = path.readText()
        println("Texto: $fileContain\n")
        println("Palabras: ${countWords(fileContain)}")
    }
}

fun countWords(s: String): Int {
    return s.count { it == ' ' || it == '\n' }
}
