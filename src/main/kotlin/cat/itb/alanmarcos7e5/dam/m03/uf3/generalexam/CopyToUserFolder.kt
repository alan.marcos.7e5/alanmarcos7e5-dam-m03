package cat.itb.alanmarcos7e5.dam.m03.uf3.generalexam

import java.nio.file.Path
import java.util.*
import kotlin.io.path.*

/**
 * Volem fer un petit programa que fagi una copia d'un fitxer a una carpeta indicada per l'usuari.
L'usuari indirarà una ruta del fitxer a copiar, la ruta destí, el nom de la persona i el seu cognom.
Fes una copia del fitxer indicat a la carpeta rutadesti/cognom/nom
Si la ruta del fitxer a copiar és una carpeta, imprimeix "No es pot copiar una carpeta"

Exemple1
Input
/home/sjo/some_file.txt
/home/sjo/destinationFolder
Mar
Pi
Resultat
Ha copiat el fitxer some_file a la carpeta /home/sjo/destinationFolder/Pi/Mar

Output
Fitxer copiat a la carpeta /home/sjo/user_data/Pi/Mar
 */
fun main() {
    val scanner = Scanner(System.`in`)
    println("path to copy")
    val fileToCopyPath = Path(scanner.nextLine())
    val isFile = verifyFileToCopy(fileToCopyPath)
    if (isFile) {
        println("path destination")
        val destination = Path(scanner.nextLine())
        println("lastname:")
        val lastName = scanner.nextLine()
        println("name:")
        val name = scanner.nextLine()
        val destinationPath = createDestinationDirs(destination,lastName, name)
        val wasCopied = copyFile(fileToCopyPath, destinationPath)
        if (wasCopied) println("Fitxer copiat a la carpeta $destinationPath")
        else println("Not copied")
    }else println("No es pot copiar una carpeta")
}

fun copyFile(fileToCopy: Path, destinationPath: Path): Boolean {
    return if (fileToCopy.exists()){
        val fileDestinationPath = destinationPath.resolve("${fileToCopy.fileName}")
        fileToCopy.copyTo(fileDestinationPath)
        true
    }
    else false
}

fun verifyFileToCopy(fileToCopy: Path): Boolean {
    val file = fileToCopy.toFile()
    return file.isFile
}

fun createDestinationDirs(destination: Path, lastName: String, name: String): Path{
    val backUpPath = destination.resolve("$lastName/$name")
    if (backUpPath.notExists()) backUpPath.createDirectories()
    return backUpPath
}