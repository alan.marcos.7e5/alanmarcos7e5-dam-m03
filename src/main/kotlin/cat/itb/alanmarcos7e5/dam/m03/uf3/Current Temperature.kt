package cat.itb.alanmarcos7e5.dam.m03.uf3

import io.ktor.client.request.*
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
/**
 * La temperatura està en graus Kelvi
 * http://api.openweathermap.org/data/2.5/weather?lat=41.390205&lon=2.154007&appid=d662e754d0671e1384f22d2d9023795d
 * Output
 * Bon dia,
 * Avui fa 15.3º a la ciutat de Barcelona amb una humitat del 20%.
 */
@Serializable
data class MeteoBase(
    @SerialName("main")val temperatures: BaseTemperature,
    val name: String,
    @SerialName("weather")val listWeather: List<Weather>)

@Serializable
data class Weather(val icon: String)


@Serializable
data class BaseTemperature(@SerialName("temp")val temperature: Double, val humidity: Double) {
    val celsius get() = temperature - 273.15
}

suspend fun main(){
    val client = createJsonHttpClient(true)
    val meteoBase: MeteoBase = client.get(
        "http://api.openweathermap.org/data/2.5/weather?lat=41.390205&lon=2.154007&appid=d662e754d0671e1384f22d2d9023795d")
    putIcon(meteoBase)
    toOutput(meteoBase)
}

fun putIcon(meteoBase: MeteoBase) {
    for (weather in meteoBase.listWeather){
        val newIcon = weather.icon

    }
}

fun toOutput(meteoBase: MeteoBase){
    println("Avui fa ${meteoBase.temperatures.celsius} a la ciutat de ${meteoBase.name} " +
            "amb una humitat del ${meteoBase.temperatures.humidity}%.")
}