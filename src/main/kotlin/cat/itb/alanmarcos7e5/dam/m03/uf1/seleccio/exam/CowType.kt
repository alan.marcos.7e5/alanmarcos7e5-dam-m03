package cat.itb.alanmarcos7e5.dam.m03.uf1.seleccio.exam

import java.util.*

/**
 * Estem fent un programa per uns veterianris per gestionar vaques. Hem de classificar els animals segons si són vedell, vaca, toro o bou.

Un animal es considera vedell si és menor de 2 anys, en cas contrari és adult.
Dels adults, es considera vaca si és femella i toro o bou si és mascle.
La diferència entre toro i bou és que el bou està capat i el toro no.

Demana a l'usuari l'edat del animal, el sexe (1 = mascle, 2 = femella) i si està capat (1 = no capat, 2 = capat).
Imprimeix per pantalla la classe d'animal
 */

fun animal(age : Int, sex: Int, estado : Int) : String{
    return if (age < 2) "vedell"
    else{
        if (sex == 2) "vaca"
        else {
            if (estado == 1)
                "toro"
            else
                "bou"
        }
    }

}

fun main() {
    val scanner = Scanner(System.`in`)
    val animalAge = scanner.next().toInt()
    val animalSex = scanner.next().toInt()
    val animalEstado = scanner.next().toInt()
    println(animal(animalAge, animalSex, animalEstado))
}