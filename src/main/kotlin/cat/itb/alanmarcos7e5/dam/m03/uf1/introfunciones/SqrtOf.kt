package cat.itb.alanmarcos7e5.dam.m03.uf1.introfunciones

import java.util.*

//Demana un enter i printa'n l'arrel quadrada

fun main() {
    val scanner = Scanner(System.`in`)
    val inputInteger = scanner.nextInt()
    val inputSquareRoot: Double = Math.sqrt(inputInteger.toDouble())
    println(inputSquareRoot)
}