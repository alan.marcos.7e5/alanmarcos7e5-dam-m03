package cat.itb.alanmarcos7e5.dam.m03.uf1.adventOfCode

import java.util.*
import kotlin.io.path.Path

/**
 * Day 4
 */

fun main() {
    val scanner = Scanner(Path("data/aventofcode/inputDay4.txt"))
    var inputListDrawn = mutableListOf<String>()
    while (scanner.hasNext()){
        inputListDrawn = scanner.nextLine().split(',').toMutableList()
        break
    }
    //val listDrawnInt = inputListDrawn.map { it.toInt() }
    var inputBoard = mutableListOf<MutableList<MutableList<String>>>()
    var i = 0
    while (scanner.hasNext()) {
        inputBoard.add(i,addToBoard(scanner))
        i++
    }

    val listOfResume = mutableListOf<List<Int>>()
    for ((index,element) in inputBoard.withIndex()) {
        listOfResume.add(index, markingBoards(index,inputListDrawn, element))
    }

    val minimus = mutableListOf(0,Int.MAX_VALUE,0) //numero de inputboard,minimo momento en que gana, numero con el que gana
    for (element in listOfResume) {
        if (element[1] < minimus[1]) {
            minimus[0] = element[0]
            minimus[1] = element[1]
            minimus[2] = element[2]
        }
    }
    val countWinFirst = countingTheWinner(inputBoard[minimus[0]]) * minimus[2]
    println(countWinFirst)

    //For see the last winner
    val maximun = mutableListOf(0,Int.MIN_VALUE,0) //numero de inputboard,maximo momento en que gana, numero con el que gana
    for (element in listOfResume) {
        if (element[1] > maximun[1]) {
            maximun[0] = element[0]
            maximun[1] = element[1]
            maximun[2] = element[2]
        }
    }
    val countWinLast = countingTheWinner(inputBoard[maximun[0]]) * maximun[2]
    println("The last: $countWinLast")
}

fun addToBoard(scanner: Scanner) : MutableList<MutableList<String>>{
    scanner.nextLine()
    var i = 0
    var board = mutableListOf<MutableList<String>>()
    while (scanner.hasNext() && i<5) {
        var innerBoard = scanner.nextLine().split(' ').toMutableList()
        val provisionalInnerBoard = mutableListOf<String>()
        for (e in innerBoard) {
            if (e!="") provisionalInnerBoard += e
        }
        innerBoard = provisionalInnerBoard
        board += innerBoard
        i++
    }
    return board
}

fun markingBoards(index : Int ,drawnList : List<String>, boardList : MutableList<MutableList<String>>) : List<Int> {
    val verificationList = List(5){"M"}
    var indexOfDrawnWinner : Int = Int.MAX_VALUE
    var numberDrawnWinner = 0
    for ((i,number) in drawnList.withIndex()) {
        for (inner in boardList){
            for ((innerIndex,element) in inner.withIndex()){
                if (number == element) { //coincidio el elemento
                    inner[innerIndex] = "M"
                    val allColumn = mutableListOf<String>()
                    for (toBoardIndex in 0 until boardList.size) {
                        if (boardList[toBoardIndex][innerIndex] == "M") allColumn.add("M")
                        else break
                    }
                    if (inner == verificationList || allColumn == verificationList) {
                        indexOfDrawnWinner = i
                        return listOf(index, indexOfDrawnWinner, number.toInt())
                    }
                }
            }
        }
    }
    return listOf(index, indexOfDrawnWinner, numberDrawnWinner) //la board list , en que momento gana, numero con el que gana
}

fun countingTheWinner(boardList : MutableList<MutableList<String>>) : Int{
    var summary = 0
    for (inner in boardList) {
        for (element in inner) {
            if (element != "M") summary += element.toInt()
        }
    }
    return summary
}