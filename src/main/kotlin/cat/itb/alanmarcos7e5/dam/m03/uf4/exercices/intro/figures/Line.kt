package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.intro.figures

class Line(color: String, val base: Int): Figure(color) {
    override fun paintText() {
        repeat(base){
            val triangleColor = prepareColor()
            print("${triangleColor}X")
        }
        println()
    }
}