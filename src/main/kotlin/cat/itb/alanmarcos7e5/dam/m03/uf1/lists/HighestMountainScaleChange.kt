package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

/**
 * El govern britànic ens ha demanat que també vol accedir a les dades de l'exercici
 * anterior i que les necessitaria en peus i no metres.
 * Per convertir un metre a peus has de multiplicar els metres per 3.2808.
 * Fes la conversió i imprimeix la matriu per pantalla.
 */

fun main() {
    val mountain = HighestMountain(0,0,0.0)
    val result = highestMountain(mountain)
    val resultPeus = convertToPeus(mountain.maxim)
    println(resultPeus)
}

fun convertToPeus (meters : Double) : Double {
    return meters * 3.2808
}