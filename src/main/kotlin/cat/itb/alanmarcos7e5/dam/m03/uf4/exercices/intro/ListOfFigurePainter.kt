package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.intro

import cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.intro.figures.*
import cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.intro.figures.Rectangle

/**
 * Volem fer un programa que sigui capaç de pintar una llista de figures.
 */

fun main() {
    val rectangle = Rectangle(RED,4,3)
    val rectangle2 = Rectangle(GREEN,2,3)
    val piramid1 = Piramid(CYAN,3)
    val listOfFigures = listOf<Figure>(rectangle,rectangle2,piramid1)
    toOutput(listOfFigures)
}

fun toOutput(listFigures: List<Figure>) {
    for (figure in listFigures) figure.paintText()
}
