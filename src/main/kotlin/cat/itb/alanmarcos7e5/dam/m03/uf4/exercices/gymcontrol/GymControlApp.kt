package cat.itb.alanmarcos7e5.dam.m03.uf4.exercices.gymcontrol

import java.util.*

fun main() {
    val gymLector = GymControlManualReader()
    val usersList = mutableListOf<Int>()
    repeat(8) {
        val user = gymLector.nextId().toInt()
        checkUser(user, usersList)
    }
}

fun checkUser(user: Int, usersList: MutableList<Int>) {
    if (user in usersList) {
        usersList.remove(user)
        println("$user Sortida")
    } else {
        usersList += user
        println("$user Entrada")
    }
}

