import java.util.*

//L'usuari escriu un enter amb la seva edat i s'imprimeix true si l'edat està entre 10 i 20.

fun main() {
    val scanner = Scanner(System.`in`)
    val inputAge = scanner.nextInt()
    val inRange = inputAge in 10..20
    println(inRange)
}