package cat.itb.alanmarcos7e5.dam.m03.uf1.iterative

import java.util.*

/**
 * Calcula el factorial d'un valor
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val inputUser = scanner.nextInt()
    val output = calculateFactorial(inputUser)
    println(output)
}

fun calculateFactorial(inputNumber : Int) : Int{
    var i = inputNumber
    var factorial = 1
    while (i >= 1) {
        factorial *= i
        i--
    }
    return factorial
}