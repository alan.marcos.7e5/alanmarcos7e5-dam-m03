package cat.itb.alanmarcos7e5.dam.m03.uf3.EnergyPrice.logic

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File
import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.io.path.exists
import kotlin.io.path.readText
import kotlin.io.path.writeText

class EnergyDayPrices(val mapOfPrices: Map<String, HourRange>) {

    fun findByHour(hourKey: String): HourRange? {
        for (k in mapOfPrices.keys) {
            if (k == hourKey) return mapOfPrices[k]
        }
        return null
    }

    fun findExpensive(): HourRange? {
        var keyOfMax = ""
        var maxPrice = Double.MIN_VALUE
        for ((k,v) in mapOfPrices) if (v.price > maxPrice) {
            maxPrice = v.price
            keyOfMax = k
        }
        return mapOfPrices[keyOfMax]
    }

    fun findCheapest(): HourRange? {
        var keyOfMin = ""
        var minPrice = Double.MAX_VALUE
        for ((k,v) in mapOfPrices) if (v.price < minPrice) {
            minPrice = v.price
            keyOfMin = k
        }
        return mapOfPrices[keyOfMin]
    }

    fun allRanges(): List<HourRange> {
        val listOfRange = mutableListOf<HourRange>()
        for (k in mapOfPrices) listOfRange += k.value
        return listOfRange
    }

    fun creatingPath(): Path = Path(System.getProperty("user.home")).resolve("DayPrices.json")

    fun saveFile(filePath: Path) {
        val serializer = Json.encodeToString(mapOfPrices)
        filePath.writeText(serializer)
    }

    fun loadFromFile(filePath: Path): Map<String, HourRange> {
        val text = filePath.readText()
        return Json.decodeFromString<Map<String, HourRange>>(text)
    }

    fun deleteFile(filePath: Path): Boolean {
        var erased = false
        if (filePath.exists()) {
            filePath.toFile().delete()
            erased = true
        }
        return erased
    }

}

