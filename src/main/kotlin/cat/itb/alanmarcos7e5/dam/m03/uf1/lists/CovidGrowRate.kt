package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

import java.util.*

/**
 * El departament de salut ens ha demanat que calculem la taxa de infecció
 * que està tenint la Covid en la nostre regió sanitaria.
 * Donat un nombre de casos casos1casos1 en una setmana,
 * si la següent tenim un nombre de casos casos2casos2,
 * podem calcular la taxa d'infecció amb la fórmula
 * infecció = casos2/casos1
 * L'usuari introduirà un llistat de casos detectats cada setmana (usant el format del ListReader).
 * Imprimeix la taxa d'infecció detectada cada setmana.
 * El programa no pot imprimir res fins a que hagi llegit tots els valors.
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val casesPerWeek = readIntList(scanner)
    val infectPerWeek = mutableListOf<Double>()
    var infectionRate = 0.0
    for ((i,element) in casesPerWeek.withIndex()) {
        if (i < (casesPerWeek.lastIndex)) {
            infectionRate = casesPerWeek[i+1].toDouble() / element
            infectPerWeek.add(infectionRate)
            println("${casesPerWeek[i+1]} / $element = $infectionRate")
        } else break
    }
    println(infectPerWeek)
}