package cat.itb.alanmarcos7e5.dam.m03.uf1.lists

/**
 * Donada la següent matriu
 * val matrix = listOf(listOf(2,5,1,6),listOf(23,52,14,36),listOf(23,75,81,64))
 * Imrimeix la suma de tots els seus valors.
 */

fun main() {
    val matrix = listOf(listOf(2,5,1,6),listOf(23,52,14,36),listOf(23,75,81,64))
    print(matrixElementSumInt(matrix))
}

fun matrixElementSumInt(matrix: List<List<Int>>):Int{
    var sum =0
    for (row in matrix) {
        for (cell in row) {
            sum += cell
        }
    }
    return sum
}