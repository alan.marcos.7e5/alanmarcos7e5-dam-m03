package cat.itb.alanmarcos7e5.dam.m03.uf1.iterative

import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val userNum = scanner.nextInt()
    repeat(userNum){
        print(it+1)
    }
}