package cat.itb.alanmarcos7e5.dam.m03.uf2.functions

import java.util.*

/**
 * Fes un programa (en un fitxer nou anomenat IntListReaderSample) que llegeixi una llista d'enters i la imprimeixi per pantalla.
 * Input
 * 5 3 4 7 8 -1
 * Output
 * [5,3,4,7,8]
 */

fun main() {
    val scanner = Scanner(System.`in`)
    val listUser = readIntList(scanner)
    println(listUser)
}