package cat.itb.alanmarcos7e5.dam.m03.uf1.seleccio

import java.util.*

//L'usuari introdueix la lletra del tipus d'habitatge i número de m^3 d'aigua gastats.
//Printa per pantalla el preu total
//Pots veure com es calcules les tarífes aquí (format pdf)

fun main() {
    //Lectura de usuario
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Ingrese el tipo de vivienda: ")
    val userTipeResidence = scanner.nextLine()
    println("Ingrese la cantidad de metros cúbicos de agua gastados: ")
    val userWaterConsum = scanner.nextDouble()
    //Lógica de programa
    var outputMal : Boolean = false

    val fixQuote = when(userTipeResidence){
        "A"->2.46
        "B"->6.40
        "C"->7.25
        "D"->11.21
        "E"->12.10
        "F"->17.28
        "G"->28.01
        "H"->40.50
        "I"->61.31
        else-> {
            outputMal =true
            0.0 //fixQuote tomará este valor
        }
    }

    val userWaterConsumInt = userWaterConsum.toInt() //lo paso a Int porque no es necesario analizar valores con decimales
    val fixPrice = when(userWaterConsumInt){
        in 0..6 -> 0.5941
        in 7..9 -> 1.1883
        in 10..15 -> 1.7824
        in 16..18 -> 2.3764
        else -> {
            5.37 //fixPrice tomará este valor
        }

    }

    if (!outputMal){
        val waterBill = fixPrice * userWaterConsum + fixQuote
        println(waterBill)
    }else
        println("Has ingresado mal algun dato, no se puede realizar la operación")

}