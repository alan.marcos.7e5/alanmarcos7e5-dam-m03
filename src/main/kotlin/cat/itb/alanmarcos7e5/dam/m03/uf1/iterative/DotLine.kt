package cat.itb.alanmarcos7e5.dam.m03.uf1.iterative

import java.util.*

//Demana un enter a l'usuari.
//Imprimeix per pantalla tants punts com l'usuari hagi indicat


fun main() {
    val scanner = Scanner(System.`in`)
    val userIndication = scanner.nextInt()
    repeat(userIndication){
        print(".   $it")
    }

}