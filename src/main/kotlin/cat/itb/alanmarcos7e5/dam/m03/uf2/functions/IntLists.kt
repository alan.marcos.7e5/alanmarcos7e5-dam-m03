package cat.itb.alanmarcos7e5.dam.m03.uf2.functions

import java.util.*

/**
 * Defineix la funció readIntList que llegeixi una llista d'enters d'un scanner passat per paràmetre.
 * Els enters són introduits amb un espai de separació. -1 indica que ja no hi ha més enters a llegir.
 */

fun readIntList (scanner: Scanner):List<Int> {
    val intList = mutableListOf<Int>()
    var userInput = scanner.nextInt()
    while (userInput != -1) {
        intList += userInput
        userInput = scanner.nextInt()
    }
    return intList
}

/**
 * Defineix la funció min dins de la classe IntegerLists que donada una llista d'enters, retorni el valor mínim.
 */

fun min(list: List<Int>): Int{
    var min = list[0]
    for (e in list) {
        if (e < min) min = e
    }
    return min
}

/**
 * maximo de una lista de enteros
 */
fun max(list: List<Int>): Int{
    var max = list[0]
    for (e in list) {
        if (e > max) max = e
    }
    return max
}
/**
 * sumar items de una lista
 */
fun sum(list: List<Int>): Int {
    var sum = 0
    for (e in list) sum += e
    return sum
}

/**
 * promedio de una lista de enteros
 */
fun avg(list: List<Int>): Double {
    return sum(list).toDouble() / list.size
}

