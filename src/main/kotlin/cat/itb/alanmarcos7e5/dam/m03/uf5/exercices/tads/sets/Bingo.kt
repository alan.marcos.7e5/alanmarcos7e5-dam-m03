package cat.itb.alanmarcos7e5.dam.m03.uf5.exercices.tads.sets

import cat.itb.alanmarcos7e5.dam.m03.uf1.lists.readIntList
import java.util.Scanner

/**
Després de molts diumenges acompanyant a la teva avia al bingo de la residència decideixes
fer un petit programa que t'ajudi. El Bingo de la residència no es canta linia, només Bingo.

L'usuari primer itrodueix 10 números, que són els de la targeta.
Després introdueix els números que es van cantant.
Després que cada número cantat indica quants t'en queden.
Quan s'hagin cantat tots els números imprimeix BINGO i finialitza el programa

Input
1 2 3 4 5 6 7 8 9 10
1 3 50 66 2 4 5 6 84 98 7 8 9 10

Output
Em queden 9 números
Em queden 8 números
Em queden 8 números
Em queden 8 números
Em queden 7 números
Em queden 6 números
Em queden 5 números
Em queden 4 números
Em queden 4 números
Em queden 4 números
Em queden 3 números
Em queden 2 números
Em queden 1 números
BINGO
 */
fun main() {
    val scanner = Scanner(System.`in`)
    val setCard = readIntList(scanner).toMutableSet()
    println(setCard)
    val setSingedNumber = readIntList(scanner).toSet()
    println(setSingedNumber)
    verifyNumber(setCard, setSingedNumber)
}

fun verifyNumber(card: MutableSet<Int>, singedNumber: Set<Int>) {
    for (singedNum in singedNumber) {
        println("Em queden ${card.size} números")
        if (singedNum in card) {
            card.remove(singedNum)
            if (card.size == 0){
                println("BINGO!")
                break
            }
        }
    }
}
