package cat.itb.alanmarcos7e5.dam.m03.uf1.adventOfCode

import java.io.File
import java.util.*

fun main() {

    val pathName = "data/aventofcode/inputTesting.txt"
    val miArchivo = File(pathName)

    val lineas = miArchivo.readLines()
    lineas.forEach { println(it) }
}