package cat.itb.alanmarcos7e5.dam.m03.uf3.BicingStations.ui

import cat.itb.alanmarcos7e5.dam.m03.uf3.BicingStations.logic.LogicOfStation
import java.util.*

data class BicingStationsUi(val scanner: Scanner, val logicOfStation: LogicOfStation){
    fun findStation(){
        println("Input the id of station\n")
        val id = scanner.nextInt()
        println(logicOfStation.findStation(id))
    }
    //TODO (continue creating the functions according to the options)
}