package cat.itb.alanmarcos7e5.dam.m03.uf1.data.project

import java.time.Year
import java.util.*

val scanner = Scanner(System.`in`).useLocale(Locale.UK)
fun main() {
    welcome("TrainingAssistant") // change it as you need
    ageCalculator()
    imcCalculator()
    trainingSchedule()
}

fun welcome(assistantName: String) {
    println("Hello! My name is $assistantName\nPlease, tell me your name.\n")
    val inputName = scanner.nextLine()
    println("What a great name you have, $inputName!\"")
}
fun ageCalculator() {
    val currentYear = Year.now().value
    println("Please, tell me which year you were born.\n")
    val inputYear = scanner.nextInt()
    val userAgeMax = currentYear - inputYear
    val userAgeMin = userAgeMax - 1
    println("You are between $userAgeMin - $userAgeMax years old. That's a good age for practicing sport.")
}

fun imcCalculator() {
    println("Let's check some of your parameters.\nTell me your weight in kg   ")
    val inputWeight = scanner.nextDouble()
    println("Tell me your height in m   ")
    val inputHeight = scanner.nextDouble()
    val userImc = inputWeight / (inputHeight * inputHeight)
    println("Your IMC is $userImc")
    val userInsufficient = userImc < 18.5
    val userOver = 25.0 <= userImc && userImc <= 50.0
    val userObesity = userImc > 50
    val userNormal = !userInsufficient && !userOver && !userObesity
    println("Checking insufficient weight.... ${userInsufficient}\n" +
            "Checking normal weight.... ${userNormal}\n" +
            "Checking overweight.... ${userOver}\n" +
            "Checking obesity.... ${userObesity}")
}
fun trainingSchedule() {
    println("I'll tell you your training plan.\n" +
            "How many hours would you like to train?   ")
    val inputHours = scanner.nextInt()
    println("How many days can you train?   ")
    val inputDays = scanner.nextInt()
    val module = inputHours % inputDays // 9 % 3 = 0
    val daysB = inputDays- module
    val hoursA = (inputHours/ inputDays) + 1
    val hoursB = inputHours / inputDays
    println("Your routine sport could be: \n" +
            "${module} days ${hoursA} hours\n" +
            "${daysB} days ${hoursB} hours\n")
}